<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'telemarketing'], function () {
    //Ventas web
    Route::post('/ventas-web', 'Telemarketing\VentasWebController@store');
    Route::get('/ventas-web', 'Telemarketing\VentasWebController@index');
    Route::post('/get-ventas-web', 'Telemarketing\VentasWebController@getVentas');
    Route::get('/ventas-web/{id}', 'Telemarketing\VentasWebController@show');
    Route::post('/ventas-web/update-info-erronea', 'Telemarketing\VentasWebController@updateInfoErronea');
    Route::put('/ventas-web/{id}', 'Telemarketing\VentasWebController@updateClienteVenta');
    Route::put('/ventas-web/estatus-venta/{id}', 'Telemarketing\VentasWebController@updateVenta');
    Route::put('/ventas-web/update-comentario-venta/{id}', 'Telemarketing\VentasWebController@updateComentariosVenta');
    Route::post('/ventas-web/siguiente-asesor', 'Telemarketing\VentasWebController@siguienteAsesor');
    //Catálogo origenes
    Route::post('/catalogo-origenes', 'Telemarketing\CatalogoOrigenesController@store');
    Route::get('/catalogo-origenes', 'Telemarketing\CatalogoOrigenesController@index');
    //Guardar historial de comentario de ventas
    Route::post('/historial-comentarios-save', 'Telemarketing\HistorialComentariosVentasController@store');
    Route::post('/get-comentarios-ventas', 'Telemarketing\HistorialComentariosVentasController@getComentariosVentas');
    Route::post('/historial-comentarios-info-erronea', 'Telemarketing\HistorialComentariosVentasController@saveComentarioAndUpdate');
    //Guardar citas de ventas
    Route::post('/citas-ventas', 'Telemarketing\CitasWebController@store');
    Route::get('/citas-ventas-get-by-idventa/{id}', 'Telemarketing\CitasWebController@getCitaByIdVenta');
    Route::get('/citas-ventas/{id}', 'Telemarketing\CitasWebController@show');
    Route::post('/citas-ventas/horarios-ocupados', 'Telemarketing\CitasWebController@horariosOCupados');
    Route::post('/citas-ventas/existe-cita', 'Telemarketing\CitasWebController@existeCita');
    Route::post('/citas-ventas/save-cita', 'Telemarketing\CitasWebController@saveCita');
    Route::post('/citas-ventas/datos-citas', 'Telemarketing\CitasWebController@datosCita');
    Route::post('/citas-ventas/cambiar-estatus-cita', 'Telemarketing\CitasWebController@cambiar_status_cita');
    Route::put('/citas-ventas/{id}', 'Telemarketing\CitasWebController@update');
});
