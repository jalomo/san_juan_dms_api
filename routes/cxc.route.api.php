

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| MODULO AUTOS Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'catalogo-tipo-forma-pago'], function () {
    Route::get('/', 'CuentasPorCobrar\CatTipoFormaPagoController@index');
    Route::get('/{id}', 'CuentasPorCobrar\CatTipoFormaPagoController@show');
    Route::post('/', 'CuentasPorCobrar\CatTipoFormaPagoController@store');
    Route::put('/{id}', 'CuentasPorCobrar\CatTipoFormaPagoController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\CatTipoFormaPagoController@destroy');
});

Route::group(['prefix' => 'cuentas-por-cobrar'], function () {
    Route::get('/', 'CuentasPorCobrar\CuentasPorCobrarController@listado');
    Route::get('/getkardexpagos', 'CuentasPorCobrar\CuentasPorCobrarController@getKardexPagos');
    Route::get('/getCierreCaja', 'CuentasPorCobrar\CuentasPorCobrarController@getCierreCaja');
    Route::get('/verifica-pagos-morosos', 'CuentasPorCobrar\CuentasPorCobrarController@getVerificaCuentasMorosas');
    Route::get('/aviso-pago', 'CuentasPorCobrar\CuentasPorCobrarController@envioAvisoPago');
    Route::get('/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@show');
    Route::get('buscar-por-folio-id/{folio_id}', 'CuentasPorCobrar\CuentasPorCobrarController@showByFolioId');
    Route::post('/', 'CuentasPorCobrar\CuentasPorCobrarController@store');
    Route::post('/registrar-servicio', 'CuentasPorCobrar\CuentasPorCobrarController@registrarCxcServicio');
    Route::post('/set-complemento-cxc', 'CuentasPorCobrar\CuentasPorCobrarController@setCxcComplemento');
    Route::put('/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@update');
    Route::put('/updatestatus/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@updatestatus');
    Route::delete('/{id}', 'CuentasPorCobrar\CuentasPorCobrarController@destroy');
});

Route::group(['prefix' => 'abonos-por-cobrar'], function () {
    Route::get('abonos-by-orden-entrada', 'CuentasPorCobrar\AbonosController@showByIdOrdenEntrada');
    Route::get('listado-abonos-by-orden-entrada', 'CuentasPorCobrar\AbonosController@listadoAbonosByOrdenEntrada');
    Route::get('verifica-abonos-pendientes', 'CuentasPorCobrar\AbonosController@getVerificaAbonosPendientes');
    Route::get('/', 'CuentasPorCobrar\AbonosController@index');
    Route::get('/{id}', 'CuentasPorCobrar\AbonosController@show');
    Route::post('/', 'CuentasPorCobrar\AbonosController@store');
    Route::put('/{id}', 'CuentasPorCobrar\AbonosController@update');
    Route::put('/updateEstatus/{id}', 'CuentasPorCobrar\AbonosController@updateEstatus');
    Route::delete('/{id}', 'CuentasPorCobrar\AbonosController@destroy');
});

Route::group(['prefix' => 'solicitud-credito'], function () {
    Route::get('/', 'CuentasPorCobrar\SolicitudCreditoController@index');
    Route::get('/{id}', 'CuentasPorCobrar\SolicitudCreditoController@show');
    Route::post('/', 'CuentasPorCobrar\SolicitudCreditoController@store');
    Route::put('/{id}', 'CuentasPorCobrar\SolicitudCreditoController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\SolicitudCreditoController@destroy');
    Route::get('/descargar/{id}/{string}', 'CuentasPorCobrar\SolicitudCreditoController@downloadArchivosSolicitudCredito');
});

Route::group(['prefix' => 'solicitud/tipo-documentos'], function () {
    Route::get('/', 'CuentasPorCobrar\TipoDocumentosController@index');
    Route::get('/{id}', 'CuentasPorCobrar\TipoDocumentosController@show');
    Route::post('/', 'CuentasPorCobrar\TipoDocumentosController@store');
    Route::put('/{id}', 'CuentasPorCobrar\TipoDocumentosController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\TipoDocumentosController@destroy');
});

Route::group(['prefix' => 'catalogo-plazo-credito'], function () {
    Route::get('/', 'CuentasPorCobrar\CatPlazoCreditoController@index');
    Route::get('/{id}', 'CuentasPorCobrar\CatPlazoCreditoController@show');
    Route::post('/', 'CuentasPorCobrar\CatPlazoCreditoController@store');
    Route::put('/{id}', 'CuentasPorCobrar\CatPlazoCreditoController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\CatPlazoCreditoController@destroy');
});


Route::group(['prefix' => 'estatus-cuentas'], function () {
    Route::get('/', 'CuentasPorCobrar\CatEstatusCuentasController@index');
    Route::get('/{id}', 'CuentasPorCobrar\CatEstatusCuentasController@show');
    Route::post('/', 'CuentasPorCobrar\CatEstatusCuentasController@store');
    Route::put('/{id}', 'CuentasPorCobrar\CatEstatusCuentasController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\CatEstatusCuentasController@destroy');
});

Route::group(['prefix' => 'cuentas-morosas'], function () {
    Route::get('/', 'CuentasPorCobrar\CuentasMorosasController@index');
    Route::get('/buscar-filtro', 'CuentasPorCobrar\CuentasMorosasController@getBusqueda');
    Route::get('/{id}', 'CuentasPorCobrar\CuentasMorosasController@show');
    Route::post('/', 'CuentasPorCobrar\CuentasMorosasController@store');
    Route::put('/{id}', 'CuentasPorCobrar\CuentasMorosasController@update');
    Route::delete('/{id}', 'CuentasPorCobrar\CuentasMorosasController@destroy');
});

Route::group(['prefix' => 'detalle-asientos-cuentas'], function () {
    Route::get('/', 'CuentasPorCobrar\DetalleAbonoCuentasController@index');
    Route::get('/busqueda-filtros', 'CuentasPorCobrar\DetalleAbonoCuentasController@getFiltroFechas');
    Route::get('/{id}', 'CuentasPorCobrar\DetalleAbonoCuentasController@show');
    Route::get('buscar-cuenta-por-cobrar/{id}', 'CuentasPorCobrar\DetalleAbonoCuentasController@showByCuentaPorCobrar');
    Route::post('/', 'CuentasPorCobrar\DetalleAbonoCuentasController@store');
    Route::put('/{id}', 'CuentasPorCobrar\DetalleAbonoCuentasController@update');
    Route::put('/updateEstatus/{id}', 'CuentasPorCobrar\DetalleAbonoCuentasController@updateEstatus');
    Route::delete('/{id}', 'CuentasPorCobrar\DetalleAbonoCuentasController@destroy');
});

Route::resource('tipo-asiento', 'CuentasPorCobrar\TipoAsientoController');
