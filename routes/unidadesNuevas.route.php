<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::group(['prefix' => 'oasis/master/'], function () {
//     Route::post('/get-all', 'Oasis\OasisController@getAll');
// });

Route::resource('cat-combustible', 'Autos\UnidadesNuevas\CatCombustibleController');
Route::resource('cat-proveedores-un', 'Autos\UnidadesNuevas\ProveedoresUNController');
Route::resource('cat-lineas', 'Autos\UnidadesNuevas\CatLineasController');
Route::resource('precios-activos-un', 'Autos\UnidadesNuevas\PreciosActivosUNController');


Route::group(['prefix' => 'precios/unidades/'], function () {
    Route::post('/get-parameters', 'Autos\UnidadesNuevas\ListaPreciosUNController@getByParameters');
});
Route::resource('lista-precios-un', 'Autos\UnidadesNuevas\ListaPreciosUNController');

Route::group(['prefix' => 'unidades'], function () {
    Route::get('/resumen-contabilidad', 'Autos\UnidadesNuevas\RemisionesController@getTotalUnidadesnuevas');
    Route::post('/xml-remision', 'Autos\UnidadesNuevas\RemisionesController@uploadRemisionXml');
    Route::put('/update-remision/{id}', 'Autos\UnidadesNuevas\RemisionesController@updateRemision');
    Route::get('/get-all', 'Autos\UnidadesNuevas\RemisionesController@getUnidadesParameters');
    Route::get('/getbyparameters', 'Autos\UnidadesNuevas\RemisionesController@getByParametros');
    Route::post('/ventasinunidad', 'Autos\VentaAutosController@actualizarVentaSinUnidad');
});


Route::group(['prefix' => 'historial-recepcion-unidad'], function () {
    Route::get('/{id_unidad}', 'Autos\UnidadesNuevas\HistorialEstatusUNController@getHistorialByUnidad');
});

Route::group(['prefix' => 'agenda-salida-unidades'], function () {
    Route::post('/borrarsalida', 'Autos\AgendaSalidaController@borrarSalida');
});

Route::resource('agenda-salida-unidades', 'Autos\AgendaSalidaController');
Route::resource('unidades', 'Autos\UnidadesNuevas\RemisionesController');

Route::group(['prefix' => 'get-detalle-costos-remision'], function () {
    Route::get('/{id}', 'Autos\UnidadesNuevas\DetalleCostoRemisionController@getByRemision');
});

Route::resource('detalle-costos-remision', 'Autos\UnidadesNuevas\DetalleCostoRemisionController');

Route::group(['prefix' => 'get-detalle-remision'], function () {
    Route::get('/{id}', 'Autos\UnidadesNuevas\DetalleRemisionController@getByRemision');
});

Route::resource('detalle-remision', 'Autos\UnidadesNuevas\DetalleRemisionController');

Route::group(['prefix' => 'get-memo-compra'], function () {
    Route::get('/{id}', 'Autos\UnidadesNuevas\MemoCompraController@getByRemision');
});


Route::resource('memo-compra', 'Autos\UnidadesNuevas\MemoCompraController');
Route::resource('estatus-unidades-nuevas', 'Autos\UnidadesNuevas\CatStatusUNController');
Route::resource('catalogo-aduanas', 'Autos\UnidadesNuevas\CatAduanasController');

Route::group(['prefix' => 'get-facturacion-unidades-nuevas'], function () {
    Route::get('/{id}', 'Autos\UnidadesNuevas\FacturacionUnController@getByRemision');
});


Route::resource('facturacion-unidades-nuevas', 'Autos\UnidadesNuevas\FacturacionUnController');
Route::resource('estatus-recepcion-unidades-nuevas', 'Autos\UnidadesNuevas\CatStatusRecepcionUNController');
Route::resource('historial-estatus-unidades-nuevas', 'Autos\UnidadesNuevas\HistorialEstatusUNController');

Route::group(['prefix' => 'encuesta-satisfaccion'], function () {
    Route::get('/venta/{venta_id}', 'Autos\EncuestaSatisfaccionController@datosencuestaventa');
});
Route::resource('encuesta-satisfaccion', 'Autos\EncuestaSatisfaccionController');

Route::group(['prefix' => 'contrato-venta'], function () {
    Route::get('/venta/{venta_id}', 'Autos\ContratoVentaAutoController@getByIdVenta');
    Route::post('/createupdate', 'Autos\ContratoVentaAutoController@crearActualizarContrato');
    
});
Route::resource('contrato-venta', 'Autos\ContratoVentaAutoController');


