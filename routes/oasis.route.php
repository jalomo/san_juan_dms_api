<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'oasis/master/'], function () {
    Route::post('/get-all', 'Oasis\OasisController@getAll');
});
Route::resource('oasis', 'Oasis\OasisController');
