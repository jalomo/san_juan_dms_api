<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'notificaciones/master/'], function () {
    Route::post('/get-all', 'Notificaciones\NotificacionesController@getAll');
});

Route::resource('notificaciones-generales', 'Notificaciones\NotificacionesController');
