<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| MODULO NOMIA Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'catalogos/puestos'], function () {
    Route::get('/', 'Nomina\Catalogos\PuestosController@index');
    Route::get('/{id}', 'Nomina\Catalogos\PuestosController@show');
    Route::post('/', 'Nomina\Catalogos\PuestosController@store');
    Route::put('/{id}', 'Nomina\Catalogos\PuestosController@update');
    Route::delete('/{id}', 'Nomina\Catalogos\PuestosController@destroy');
});

Route::group(['prefix' => 'catalogos/departamentos'], function () {
    Route::get('/', 'Nomina\Catalogos\DepartamentosController@index');
    Route::get('/clave', 'Nomina\Catalogos\DepartamentosController@getClave');
    Route::get('/{id}', 'Nomina\Catalogos\DepartamentosController@show');
    Route::post('/', 'Nomina\Catalogos\DepartamentosController@store');
    Route::put('/{id}', 'Nomina\Catalogos\DepartamentosController@update');
    Route::delete('/{id}', 'Nomina\Catalogos\DepartamentosController@destroy');
});