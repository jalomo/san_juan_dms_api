<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| FINANZAS Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'catalogo-cuentas'], function () {
    Route::get('/', 'Contabilidad\CuentasController@index');
    Route::get('/{id}', 'Contabilidad\CuentasController@show');
    Route::post('/', 'Contabilidad\CuentasController@store');
    Route::put('/{id}', 'Contabilidad\CuentasController@update');
    Route::delete('/{id}', 'Contabilidad\CuentasController@destroy');
});