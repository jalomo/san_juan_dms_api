<?php

namespace App\Models\Soporte;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatRolesSoporteModel extends Modelo
{
    protected $table = 'cat_roles';
    const ID = "id";
    const ROL = "rol";
    protected $fillable = [
        self::ROL
    ];
}
