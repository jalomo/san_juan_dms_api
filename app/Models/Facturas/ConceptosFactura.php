<?php

namespace App\Models\Facturas;

use App\Models\Core\Modelo;

class ConceptosFactura extends Modelo
{
    protected $table = 'concepto_factura';
    const ID = "id";
    const CONCEPTO_CLAVE_PROD_SERV = 'concepto_claveprodserv';
    const CONCEPTO_NO_IDENTIFICACION = 'concepto_no_identificacion';
    const CONCEPTO_CANTIDAD = 'concepto_cantidad';
    const CONCEPTO_CLAVE_UNIDAD = 'concepto_clave_unidad';
    const CONCEPTO_DESCRIPCION = 'concepto_descripcion';
    const CONCEPTO_VALOR_UNITARIO = 'concept_valor_unitario';
    const CONCEPTO_IMPORTE = 'concepto_importe';
    const FACTURA_ID = "factura_id";

    protected $fillable = [
        self::CONCEPTO_CLAVE_PROD_SERV,
        self::CONCEPTO_NO_IDENTIFICACION,
        self::CONCEPTO_CANTIDAD,
        self::CONCEPTO_CLAVE_UNIDAD,
        self::CONCEPTO_DESCRIPCION,
        self::CONCEPTO_VALOR_UNITARIO,
        self::CONCEPTO_IMPORTE,
        self::FACTURA_ID
    ];
}
