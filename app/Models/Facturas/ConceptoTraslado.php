<?php

namespace App\Models\Facturas;

use App\Models\Core\Modelo;

class ConceptoTraslado extends Modelo
{
    protected $table = 'concepto_traslado';
    const ID = "id";
    const BASE = 'concepto_claveprodserv';
    const IMPUESTO = 'concepto_no_identificacion';
    const TIPO_FACTOR = 'concepto_cantidad';

    // protected $fillable = [
    //     self::CONCEPTO_CLAVE_PROD_SERV,
    //     self::CONCEPTO_NO_IDENTIFICACION,
    //     self::CONCEPTO_CANTIDAD,
    //     self::CONCEPTO_CLAVE_UNIDAD,
    //     self::CONCEPTO_DESCRIPCION,
    //     self::CONCEPTO_VALOR_UNITARIO,
    //     self::CONCEPTO_IMPORTE,
    //     self::FACTURA_ID
    // ];
}
