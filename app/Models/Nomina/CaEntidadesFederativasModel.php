<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaEntidadesFederativasModel extends ModeloNominas
{
    protected $table = 'ca_EntidadesFederativas';
     
    
    const ID = "id_EntidadFederativa";
    const Clave = "Clave";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Clave,
        self::Descripcion
    ];
}