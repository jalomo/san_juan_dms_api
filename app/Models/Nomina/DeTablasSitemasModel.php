<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class DeTablasSitemasModel extends ModeloNominas
{
    protected $table = 'de_TablasSitemas';
     
    
    const ID = "id";
    const id_TablaSistema = "id_TablaSistema";
    const Valor_1 = "Valor_1";
    const Valor_2 = "Valor_2";
    const Valor_3 = "Valor_3";
   
    protected $fillable = [
        self::id_TablaSistema,
        self::Valor_1,
        self::Valor_2,
        self::Valor_3
    ];
}