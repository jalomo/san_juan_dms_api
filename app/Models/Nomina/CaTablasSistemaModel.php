<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaTablasSistemaModel extends ModeloNominas
{
    protected $table = 'ca_TablasSistema';
     
    
    const ID = "id_TablaSistema";
    const Clave = "Clave";
    const Descripcion = "Descripcion";
    const id_TipoCatalogo = "id_TipoCatalogo";
   
    protected $fillable = [
        self::Clave,
        self::Descripcion,
        self::id_TipoCatalogo
    ];
}