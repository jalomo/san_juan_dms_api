<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;
use App\Models\Nomina\CaRiesgoPuestoModel;

class CaPuestosModel extends ModeloNominas
{
     
    const ID = "id_Puesto";
    const Clave = "Clave";
    const Descripcion = "Descripcion";
    const SalarioDiario = "SalarioDiario";
    const SalarioMaximo = "SalarioMaximo";
    const id_RiesgoPuesto = "id_RiesgoPuesto";

    protected $table = 'ca_Puestos';
    protected $primaryKey = self::ID;
   
    protected $fillable = [
        self::Clave,
        self::Descripcion,
        self::SalarioDiario,
        self::SalarioMaximo,
        self::id_RiesgoPuesto
    ];

    public function CaRiesgoPuesto()
    {
        return $this->belongsTo(CaRiesgoPuestoModel::class, CaRiesgoPuestoModel::ID, self::id_RiesgoPuesto);
    }
}