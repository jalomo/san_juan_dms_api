<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaTiposContratoModel extends ModeloNominas
{
    protected $table = 'ca_TiposContrato';
     
    
    const ID = "id_TipoContrato";
    const Clave = "Clave";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Clave,
        self::Descripcion
    ];
}