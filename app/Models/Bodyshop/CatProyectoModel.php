<?php

namespace App\Models\Bodyshop;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatProyectoModel extends Modelo
{
    protected $table = 'bodyshop_cat_proyecto';
    const ID = 'id';
    const PROYECTO = 'proyecto';

    protected $fillable = [
        self::PROYECTO
    ];
}
