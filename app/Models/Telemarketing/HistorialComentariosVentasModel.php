<?php

namespace App\Models\Telemarketing;

use App\Models\Core\Modelo;

class HistorialComentariosVentasModel extends Modelo
{
    protected $table = 'historial_comentarios_venta';
    const ID = 'id';
    const ID_VENTA_WEB = 'id_venta_web';
    const COMENTARIO = 'comentario';
    const ID_USUARIO = 'id_usuario';
    const FECHA_NOTIFICACION = 'fecha_notificacion';
    const ID_TIPO_COMENTARIO = 'id_tipo_comentario'; //1:comentario_venta 2:info_erronea 3:comentario_asesor_venta

    protected $fillable = [
        self::ID_VENTA_WEB,
        self::COMENTARIO,
        self::ID_USUARIO,
        self::FECHA_NOTIFICACION,
        self::ID_TIPO_COMENTARIO
    ];
}