<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;

class RolModel extends Modelo
{
    protected $table = 'roles';
    const ID = "id";
    const ROL = "rol";
    
    const ROL_ADMIN = 1;
    protected $fillable = [
        self::ROL
    ];
}
