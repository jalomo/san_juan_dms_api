<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;

class RolesSeccionesModel extends Modelo
{
    protected $table = 'det_roles_secciones';
    const ID = "id";
    const ROL_ID = "rol_id";
    const SECCION_ID = "seccion_id";
    
    protected $fillable = [
        self::SECCION_ID,
        self::ROL_ID
    ];
}
