<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class RolesVistasModel extends Modelo
{
    protected $table = 'det_rol_vista';
    const ID = "id";
    const ROL_ID = "rol_id";
    const VISTA_ID = "vista_id";
    
    protected $fillable = [
        self::ID,
        self::ROL_ID,
        self::VISTA_ID
    ];
}
