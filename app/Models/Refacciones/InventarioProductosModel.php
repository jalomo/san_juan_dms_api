<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class InventarioProductosModel extends Modelo
{
    protected $table = 'inventario_productos';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const CANTIDAD_INVENTARIO = "cantidad_inventario"; //cantidad ingresada
    const CANTIDAD_STOCK = "cantidad_stock"; //cantidad de stock
    
    const VALOR_UNITARIO = "valor_unitario";
    const VALOR_RESIDUO = "valor_residuo";
    const DIFERENCIA = "diferencia";
    const SOBRANTES = "sobrantes";
    const FALTANTES = "faltantes";
    const OBSERVACIONES = "observaciones";
    const USUARIO_ID = "usuario_id";
    const INVENTARIO_ID = 'inventario_id';

    protected $fillable = [
        self::PRODUCTO_ID,
        self::CANTIDAD_INVENTARIO,
        self::CANTIDAD_STOCK,
        self::VALOR_UNITARIO,
        self::VALOR_RESIDUO,
        self::DIFERENCIA,
        self::SOBRANTES,
        self::FALTANTES,
        self::OBSERVACIONES,
        self::USUARIO_ID,
        self::INVENTARIO_ID
    ];
}
