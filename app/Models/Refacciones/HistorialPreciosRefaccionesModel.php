<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class HistorialPreciosRefaccionesModel extends Modelo
{
    protected $table = 'historial_precio_refaccion';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const PRECIO = "precio";
    const PRECIO_PUBLICO = "precio_publico";

    protected $fillable = [
        self::PRODUCTO_ID,
        self::PRECIO,
        self::PRECIO_PUBLICO
    ];
}
