<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class Compras extends Modelo
{
    protected $table = 'compra';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const PROVEEDOR_ID = "proveedor_id";
    const FECHA_ENTRADA = "fecha_entrada";
    const ORDEN_ENTRADA = "orden_entrada";
    const NO_FACTURA = "no_factura";
    const NO_PEDIDO = "no_pedido";
    const ALMACEN_ID = "almacen_id";
    const OBSERVACIONES = "observaciones";
    const FOLIO = "folio";

    protected $fillable = [
        self::PRODUCTO_ID,
        self::PROVEEDOR_ID,
        self::FECHA_ENTRADA,
        self::ORDEN_ENTRADA,
        self::NO_FACTURA,
        self::NO_PEDIDO,
        self::ALMACEN_ID,
        self::OBSERVACIONES,
        self::FOLIO
    ];
}
