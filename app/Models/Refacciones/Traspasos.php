<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use App\Models\Refacciones\FoliosModel;

class Traspasos extends Modelo
{
    protected $table = 'traspasos';
    const ID = "id";
    const ALMACEN_ORIGEN_ID = "almacen_origen_id";
    const ALMACEN_DESTINO_ID = "almacen_destino_id";
    const ESTATUS = 'estatus';
    const FOLIO_ID = 'folio_id';


    const ESTATUS_FINALIZADO = 1;
    const ESTATUS_NO_FINALIZADO = 0;

    const REL_FOLIO = 'folios';
    const REL_TRASPASOS = 'detalle_traspasos';
    
    const REL_ALMACEN_DESTINO = 'almacen_destino';
    const REL_ALMACEN_ORIGEN = 'almacen_origen';

    const FECHA_INICIO = 'fecha_inicio';
    const FECHA_FIN = 'fecha_fin';
    
    protected $fillable = [
        self::ALMACEN_ORIGEN_ID,
        self::ALMACEN_DESTINO_ID,
        self::ESTATUS,
        self::FOLIO_ID,

    ];

    public function folios()
    {
        return $this->hasOne(FoliosModel::class, FoliosModel::ID, self::FOLIO_ID);
    }

    public function detalle_traspasos()
    {
        return $this->hasMany(TraspasoProductoAlmacenModel::class, TraspasoProductoAlmacenModel::TRASPASO_ID, self::ID);
    }

    public function almacen_origen()
    {
        return $this->hasOne(Almacenes::class, Almacenes::ID, self::ALMACEN_ORIGEN_ID);
    }

    public function almacen_destino()
    {
        return $this->hasOne(Almacenes::class, Almacenes::ID, self::ALMACEN_DESTINO_ID);
    }
}
