<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class InventarioModel extends Modelo
{
    protected $table = 'ma_inventario';
    const ID = "id";
    const ESTATUS_INVENTARIO_ID = "estatus_inventario_id";
    const USUARIO_REGISTRO = "usuario_registro";
    const USUARIO_ACTUALIZO = "usuario_actualizo";
    const JUSTIFICACION = "justificacion";

    protected $fillable = [
        self::ESTATUS_INVENTARIO_ID,
        self::USUARIO_REGISTRO,
        self::USUARIO_ACTUALIZO,
        self::JUSTIFICACION
    ];
}
