<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class permisoVentaModel extends Modelo
{
    protected $table = 'permisos_venta';
    const ID = "id";
    const USUARIO_ID = "usuario_id";
    const VENTA_ID = "venta_id";
    

    protected $fillable = [
        self::USUARIO_ID,
        self::VENTA_ID
    ];
}
