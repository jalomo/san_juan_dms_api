<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatalogoMarcasModel extends Modelo
{
    protected $table = 'catalogo_marcas';
    const ID = "id";
    const NOMBRE = "nombre";

    protected $fillable = [
        self::NOMBRE
    ];
}
