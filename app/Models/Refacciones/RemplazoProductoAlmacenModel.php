<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class RemplazoProductoAlmacenModel extends Modelo
{
    protected $table = 'remplazo_almacen';
    const ID = "id";
    const NOMBRE_PRODUCTO_ANTERIOR = "nombre_producto_anterior";
    const NOMBRE_PRODUCTO_ACTUAL = "nombre_producto_actual";
    const PRODUCTO_ID = "producto_id";
    const NO_IDENTIFICACION = 'no_identificacion';
    const NO_IDENTIFICACION_ANTERIOR = 'no_identificacion_anterior';

    protected $fillable = [
        self::PRODUCTO_ID,
        self::NOMBRE_PRODUCTO_ACTUAL,
        self::NOMBRE_PRODUCTO_ANTERIOR,
        self::NO_IDENTIFICACION,
        self::NO_IDENTIFICACION_ANTERIOR
    ];
}
