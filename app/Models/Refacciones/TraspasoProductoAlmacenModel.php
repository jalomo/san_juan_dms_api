<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class TraspasoProductoAlmacenModel extends Modelo
{
    protected $table = 'detalle_traspaso_producto_almacen';
    const ID = "id";
    const PRODUCTO_ID = 'producto_id';
    const ALMACEN_ORIGEN_ID = 'almacen_origen_id';
    const ALMACEN_DESTINO_ID = 'almacen_destino_id';
    const TRASPASO_ID = 'traspaso_id';
    const PRODUCTO_PRECIO = 'producto_precio';
    const TOTAL = 'total';
    const VALOR_UNITARIO = 'valor_unitario';
    const CANTIDAD = 'cantidad';

    const DESCONTADO_A_PRODUCTO = 'descontado_a_producto';

    const ACTIVO = 'activo';

    protected $fillable = [
        self::PRODUCTO_ID,
        self::ALMACEN_ORIGEN_ID,
        self::ALMACEN_DESTINO_ID,
        self::ACTIVO,
        self::TOTAL,
        self::TRASPASO_ID,
        self::CANTIDAD,
        self::DESCONTADO_A_PRODUCTO,
        self::VALOR_UNITARIO
    ];

    public function producto()
    {
        return $this->hasOne(ProductosModel::class, ProductosModel::ID, self::PRODUCTO_ID);
    }
}
