<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ReFacturaOrdenCompraModel extends Modelo
{
    protected $table = 're_factura_ordencompra';
    const ID = "id";
    const FACTURA_ID = "factura_id";
    const ORDEN_COMPRA_ID = "orden_compra_id";
    const ESTATUS_FACTURA_ID = "estatus_factura_id";
    const ACTIVO = "activo";
    const USER_ID = "user_id";
    
    protected $fillable = [
        self::FACTURA_ID,
        self::ORDEN_COMPRA_ID,
        self::ESTATUS_FACTURA_ID,
        self::ACTIVO,
        self::USER_ID,
    ];
}
