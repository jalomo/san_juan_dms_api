<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class UbicacionLLavesModel extends Modelo
{
    protected $table = 'catalogo_ubicacion_llaves';
    const ID = "id";
    const NOMBRE = "nombre";

    protected $fillable = [
        self::NOMBRE
    ];
}
