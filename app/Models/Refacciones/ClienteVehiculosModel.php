<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ClienteVehiculosModel extends Modelo
{
    protected $table = 'cat_vehiculos_cliente';
    const ID = "id";
    const UNIDAD_DESCRIPCION = "unidad_descripcion";
    const ULTIMO_KILOMETRAJE = "kilometraje";
    const MARCA_ID = "marca_id";
    const MODELO_ID = "modelo_id";
    const ANIO_ID = "anio_id";
    const COLOR_ID = "color_id";
    const CATALOGO_ID = "catalogo_id";
    const N_MOTOR = "n_motor";
    const MOTOR = "motor";
    const TRANSMISION = "transmision";
    const COMBUSTIBLE = "combustible";
    const VIN = "vin";
    const SERIE_CORTA = "serie_corta";
    const PLACAS = "placas";
    const NUMERO_CILINDROS = 'numero_cilindros';
    const CAPACIDAD = "capacidad";
    const NUMERO_PUERTAS = "numero_puertas";
    const NOTAS = "notas";
    const CLIENTE_ID = 'cliente_id';

    protected $fillable = [
        self::UNIDAD_DESCRIPCION,
        self::ULTIMO_KILOMETRAJE,
        self::MARCA_ID,
        self::MODELO_ID,
        self::ANIO_ID,
        self::COLOR_ID,
        self::CATALOGO_ID,
        self::NOTAS,
        self::N_MOTOR,
        self::MOTOR,
        self::TRANSMISION,
        self::COMBUSTIBLE,
        self::VIN,
        self::SERIE_CORTA,
        self::PLACAS,
        self::NUMERO_CILINDROS,
        self::CAPACIDAD,
        self::NUMERO_PUERTAS,
        self::CLIENTE_ID,
    ];
}