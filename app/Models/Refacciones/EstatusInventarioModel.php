<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class EstatusInventarioModel extends Modelo
{
    protected $table = 'estatus_inventario';
    const ID = "id";
    const NOMBRE = "nombre";

    const PROCESO = 1;
    const FINALIZADO = 2;
    const CANCELADO = 3;
    
    
    protected $fillable = [
        self::NOMBRE
    ];
}
