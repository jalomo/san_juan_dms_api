<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModeloNominas extends Model
{
    // use SoftDeletes;
    protected $connection = 'nominas';
    protected $softDelete = true;
    public $timestamps = false;

    const UPDATED_AT = 'updated_at';
    const CREATED_AT = 'created_at';
    const DELETED_AT = 'deleted_at';

    protected $dates = [self::DELETED_AT];

    protected $hidden = [
        self::CREATED_AT,
        self::UPDATED_AT,
        self::DELETED_AT
    ];

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    public static function connectionName()
    {

        return with(new static)->getConnectionName();
    }
}
