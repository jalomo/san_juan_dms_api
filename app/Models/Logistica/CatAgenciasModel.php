<?php

namespace App\Models\Logistica;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatAgenciasModel extends Modelo
{
    protected $table = 'cat_agencias';
    const ID = 'id';
    const AGENCIA = 'agencia';

    protected $fillable = [
        self::AGENCIA
    ];
}
