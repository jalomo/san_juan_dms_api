<?php

namespace App\Models\Logistica;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatAreaReparacionModel extends Modelo
{
    protected $table = 'cat_area_reparacion';
    const ID = 'id';
    const AREA_REPARACION = 'area_reparacion';

    protected $fillable = [
        self::AREA_REPARACION
    ];
}
