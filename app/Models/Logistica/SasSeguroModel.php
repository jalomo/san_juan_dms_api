<?php

namespace App\Models\Logistica;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class SasSeguroModel extends Modelo
{
    protected $table = 'sas_seguro_bodyshop';
    const ID = 'id';
    const RECLAMO = 'reclamo';
    const TIPO = 'tipo';
    const FECHA_ALTA = 'fecha_alta';
    const ID_UNIDAD = 'id_unidad';
    const ID_COLOR = 'id_color';
    const ID_ANIO = 'id_anio';
    const SERIE = 'serie';
    const DANIO = 'danio';
    const ID_ESTATUS = 'id_estatus';
    const FINALIZADO = 'finalizado';
    const ID_ASESOR_VENTA = 'id_asesor_venta';
    const FECHA_ENTREGA_ASESOR = 'fecha_entrega_asesor';
    const OBSERVACIONES = 'observaciones';
    const FOTOS = 'fotos';
    const DOCUMENTOS = 'documentos';
    const PRESUPUESTOS = 'presupuestos';
    const RECUPERADO = 'recuperado';
    const COBRADO = 'cobrado';
    


    protected $fillable = [
        self::RECLAMO,
        self::TIPO,
        self::FECHA_ALTA,
        self::ID_UNIDAD,
        self::ID_COLOR,
        self::ID_ANIO,
        self::SERIE,
        self::DANIO,
        self::ID_ESTATUS,
        self::FINALIZADO,
        self::ID_ASESOR_VENTA,
        self::FECHA_ENTREGA_ASESOR,
        self::OBSERVACIONES,
        self::FOTOS,
        self::DOCUMENTOS,
        self::PRESUPUESTOS,
        self::RECUPERADO,
        self::COBRADO
    ];
}
