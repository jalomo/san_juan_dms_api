<?php

namespace App\Models\Logistica;
use App\Models\Core\Modelo;

class CatEstatusAdmvoModel extends Modelo   
{
    protected $table = 'cat_estatus_administrativo';
    const ID = 'id';
    const ESTATUS = 'estatus';

    protected $fillable = [
        self::ESTATUS
    ];
}
