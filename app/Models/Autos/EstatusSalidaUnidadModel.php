<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class EstatusSalidaUnidadModel extends Modelo
{

    protected $table = 'estatus_salida_unidad';
    const ID = "id";
    const NOMBRE = "nombre";

    const ESTATUS_SALIDA_DISPONIBLE = 1;
    const ESTATUS_SALIDA_APARTADO = 2;
    const ESTATUS_SALIDA_VENDIDA = 3;

    protected $fillable = [
        self::NOMBRE
    ];
}
