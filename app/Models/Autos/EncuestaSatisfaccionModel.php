<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class EncuestaSatisfaccionModel extends Modelo
{
    protected $table = 'encuesta_satisfaccion';
    const ID = "id";
    const ID_VENTA_UNIDAD = "id_venta_unidad";
    const EXPERIENCIA_VENTAS = "experiencia_ventas";
    const ATENCION_ASESOR = "atencion_asesor";
    const EXPERIENCIA_FINANCIAMIENTO = "experiencia_financiamiento";
    const EXPERIENCIA_ENTREGA_VEHICULO = "experiencia_entrega_vehiculo";
    const SEGUIMIENTO_COMPROMISOS = "seguimiento_compromisos";
    const COMENTARIOS_MEJORAS = "comentarios_mejoras";

    const MALA = 1;
    const REGULAR = 2;
    const BUENA = 3;
    const MUY_BUENA = 4;
    const EXELENTE = 5;

    protected $fillable = [
        self::ID_VENTA_UNIDAD,
        self::EXPERIENCIA_VENTAS,
        self::ATENCION_ASESOR,
        self::EXPERIENCIA_FINANCIAMIENTO,
        self::EXPERIENCIA_ENTREGA_VEHICULO,
        self::SEGUIMIENTO_COMPROMISOS,
        self::COMENTARIOS_MEJORAS
    ];
}
