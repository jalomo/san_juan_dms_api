<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class SeguridadPasivaModel extends Modelo
{
    protected $table = 'seguridad_pasiva';
    const ID = "id";

    const CINTURON_SEGURIDAD = "cinturon_seguridad";
    const BOLSAS_AIRE = "bolsas_aire";
    const CHASIS_CARROCERIA = "chasis_carroceria";
    const CARROSERIA_HIDROFORMA = "carroseria_hidroforma";
    const CRISTALES = "cristales";
    const CABECERAS = "cabeceras";
    const SISTEMA_ALERTA = "sistema_alerta";
    const TECLADO_PUERTA = "teclado_puerta";
    const ALARMA_VOLUMETRICA = "alarma_volumetrica";
    const ALARMA_PERIMETRAL = "alarma_perimetral";
    const MYKEY = "mikey";

    const ID_VENTA_AUTO = "id_venta_auto";

    protected $fillable = [
        self::CINTURON_SEGURIDAD,
        self::BOLSAS_AIRE,
        self::CHASIS_CARROCERIA,
        self::CARROSERIA_HIDROFORMA,
        self::CRISTALES,
        self::CABECERAS,
        self::SISTEMA_ALERTA,
        self::TECLADO_PUERTA,
        self::ALARMA_VOLUMETRICA,
        self::ALARMA_PERIMETRAL,
        self::MYKEY,
        self::ID_VENTA_AUTO
    ];
}
