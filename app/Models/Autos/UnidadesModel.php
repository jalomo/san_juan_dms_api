<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;


class UnidadesModel extends Modelo
{
    protected $table = 'unidades';
    const ID = "id";
    const MARCA_ID = 'marca_id';
    const MODELO_ID = 'modelo_id';
    const ANIO_ID = 'anio_id';
    const COLOR_ID = 'color_id';
    const MOTOR = 'motor';
    const TRANSMISION = 'transmision';
    const COMBUSTIBLE = 'combustible';
    const VIN = 'vin';
    const PRECIO_COSTO = 'precio_costo';
    const PRECIO_VENTA = 'precio_venta';
    const NO_SERIE = "no_serie";

    // seminuevos
    const ID_ESTADO = "id_estado"; // nuevo - seminuevo
    const KILOMETRAJE = "kilometraje";
    const PLACAS = "placas";
    const NUMERO_CILINDROS = "numero_cilindros";
    const CAPACIDAD = "capacidad";
    const NUMERO_PUERTAS = "numero_puertas";
    const SERIE_CORTA = "serie_corta";
    const ID_UBICACION = "id_ubicacion";
    const ID_UBICACION_LLAVES = "id_ubicacion_llaves";
    const NUMERO_ECONOMICO = "numero_economico";
    const USUARIO_RECIBE = "usuario_recibe";
    const FECHA_RECEPCION = "fecha_recepcion";
    
    //recepcion unidades
    const UNIDAD_DESCRIPCION = "unidad_descripcion";
    const N_MOTOR = "n_motor";
    const N_PRODUCCION = "n_produccion";
    const N_REMISION = "n_remision";
    const N_FOLIO_REMISION = "n_folio_remision";
    const PDTO = "pdto";
    const FOLIO_PEDIDO = "folio_pedido";
    const VESTIDURA_ID = "vestidura_id";
    
    const ID_CUENTA = "id_cuenta";
    const REL_RECEPCION_COSTOS = 'recepcion_costos';

    const TIPO_NUEVO = 1;
    const TIPO_SEMI_NUEVO = 2;
    protected $fillable = [
        self::MARCA_ID,
        self::MODELO_ID,
        self::ANIO_ID,
        self::COLOR_ID,
        self::MOTOR,
        self::TRANSMISION,
        self::COMBUSTIBLE,
        self::VIN,
        self::PRECIO_COSTO,
        self::PRECIO_VENTA,
        self::ID_ESTADO,
        self::KILOMETRAJE,
        self::NUMERO_CILINDROS,
        self::CAPACIDAD,
        self::NUMERO_PUERTAS,
        self::SERIE_CORTA,
        self::ID_UBICACION,
        self::ID_UBICACION_LLAVES,
        self::NUMERO_ECONOMICO,
        self::USUARIO_RECIBE,
        self::NO_SERIE,
        self::PLACAS,
        self::FECHA_RECEPCION,
        self::N_MOTOR,
        self::N_PRODUCCION,
        self::N_REMISION,
        self::N_FOLIO_REMISION,
        self::PDTO,
        self::FOLIO_PEDIDO,
        self::VESTIDURA_ID,
        self::UNIDAD_DESCRIPCION,
        self::ID_CUENTA
    ];
}
