<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class TipoDocumentosVentaModel extends Modelo
{
    protected $table = 'tipo_documento_venta';
    const ID = "id";
    const NOMBRE = "nombre";

    protected $fillable = [
        self::NOMBRE
    ];
}
