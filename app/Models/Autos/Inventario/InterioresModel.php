<?php

namespace App\Models\Autos\Inventario;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class InterioresModel extends Modelo
{
    protected $table = 'inventario_interiores';
    const ID = "id";
    const LLAVERO = "llavero";
    const SEGURO_RINES = "seguro_rines";
    const INDICADORES_ACTIVADOS = "indicadores_activados";
    const INDICADORES = "indicadores";
    const ROCIADOR = "rociador";
    const CLAXON = "claxon";
    const LUCES_DEL = "luces_del";
    const LUCES_TRAS = "luces_tras";
    const LUCES_STOP = "luces_stop";
    const RADIO = "radio";
    const PANTALLAS = "pantallas";
    const AC = "ac";
    const ENCENDEDOR = "encendedor";
    const VIDRIOS = "vidrios";
    const ESPEJOS = "espejos";
    const SEGUROS_ELECTRICOS = "seguros_electricos";
    const DISCO_COMPACTO = "disco_compato";
    const ASIENTO_VESTIDURA = "asiento_vestidura";
    const TAPETES = "tapetes";
    const ID_INVENTARIO = "id_inventario";
    
    protected $fillable = [
        self::LLAVERO,
        self::SEGURO_RINES,
        self::INDICADORES_ACTIVADOS,
        self::INDICADORES,
        self::ROCIADOR,
        self::CLAXON,
        self::LUCES_DEL,
        self::LUCES_TRAS,
        self::LUCES_STOP,
        self::RADIO,
        self::PANTALLAS,
        self::AC,
        self::ENCENDEDOR,
        self::VIDRIOS,
        self::ESPEJOS,
        self::SEGUROS_ELECTRICOS,
        self::DISCO_COMPACTO,
        self::ASIENTO_VESTIDURA,
        self::ID_INVENTARIO,
        self::TAPETES
    ];
}
