<?php

namespace App\Models\Autos\Inventario;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class GasolinaModel extends Modelo
{
    protected $table = 'inventario_gasolina';
    const ID = "id";
    const NIVEL_GASOLINA = "nivel_gasolina";
    const ARTICULOS_PERSONALES = "articulos_personales";
    const CUALES = "cuales";
    const REPORTE_ALGO_MAS = "reporte_algo_mas";
    const ID_INVENTARIO = "id_inventario";
    
    protected $fillable = [
        self::NIVEL_GASOLINA,
        self::ARTICULOS_PERSONALES,
        self::CUALES,
        self::REPORTE_ALGO_MAS,
        self::ID_INVENTARIO
    ];
}
