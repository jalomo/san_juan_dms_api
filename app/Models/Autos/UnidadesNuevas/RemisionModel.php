<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;

class RemisionModel extends Modelo
{
    protected $table = 'remisiones';
    const ID = "id";
    const LINEA_ID = 'linea_id';
    const CAT = 'cat';
    const CLAVE_VEHICULAR = 'clave_vehicular';
    const UNIDAD_DESCRIPCION = 'unidad_descripcion';
    const ECONOMICO = 'economico';
    const UNIDAD_IMPORTADA = 'unidad_importada';
    const TIPO_AUTO = 'tipo_auto';
    const CLAVE_ISAN = 'clave_isan';
    const CTA_MENUDO = 'cta_menudeo';
    const CTA_FLOTILLA = 'cta_flotilla';
    const CTA_CONAUTO = 'cta_conauto';
    const CTA_INTERCAMBIO = 'cta_intercambio';
    const CTA_PLLENO = 'cta_plleno';
    const CTA_INVENTARIO = 'cta_inventario';
    const VTA_MENUDEO = 'venta_menudeo';
    const VTA_FLOTILLA = 'vta_flotilla';
    const VTA_CONAUTO = 'vta_conauto';
    const VTA_INTERCAMBIO = 'vta_intercambio';
    const VTA_PLLENO = 'venta_plleno';
    const USERID = 'user_id';
    const C_SUBTOTAL = 'c_subtotal';
    const C_TOTAL = 'c_total';
    const C_IVA = 'c_iva';
    const FECHA_PEDIMENTO = 'fecha_pedimento';
    const PEDIMENTO = 'pedimento';
    const FECHA_REMISION = 'fecha_remision';
    const SERIE = 'serie';
    const SERIE_CORTA = 'serie_corta';
    const MOTOR = 'motor';
    const INTERCAMBIO = 'intercambio';
    const PROVEEDOR_ID = 'proveedor_id';
    const LEYENDA_DCTO = 'leyenda_dcto';
    const ESTATUS_ID = 'estatus_id'; //estatus //vendida, apartada etc
    const UBICACION_ID = 'ubicacion_id';
    const XML_REMISION = 'xml_remision';
    const DIRECTORIO_FACTURAS = 'remisiones';
    //Recepción
    const UBICACION_LLAVES_ID = "ubicacion_llaves_id";
    const FECHA_RECEPCION = "fecha_recepcion";
    const COMENTARIO = 'comentario';
    const ULTIMO_SERVICIO = 'ultimo_servicio';
    const ID_STATUS_UNIDAD = 'id_status_unidad';

    const ESTATUS_VENTA_DISPONIBLE = 1;
    const ESTATUS_VENTA_APARTADA = 2;
    const ESTATUS_VENTA_VENDIDA = 3;

    protected $fillable = [
        self::LINEA_ID,
        self::UNIDAD_DESCRIPCION,
        self::CAT,
        self::CLAVE_VEHICULAR,
        self::ECONOMICO,
        self::UNIDAD_IMPORTADA,
        self::TIPO_AUTO,
        self::CLAVE_ISAN,
        self::CTA_MENUDO,
        self::CTA_FLOTILLA,
        self::CTA_CONAUTO,
        self::CTA_INTERCAMBIO,
        self::CTA_PLLENO,
        self::CTA_INVENTARIO,
        self::VTA_MENUDEO,
        self::VTA_FLOTILLA,
        self::VTA_CONAUTO,
        self::VTA_INTERCAMBIO,
        self::VTA_PLLENO,
        self::USERID,
        self::C_SUBTOTAL,
        self::C_TOTAL,
        self::C_IVA,
        self::FECHA_PEDIMENTO,
        self::PEDIMENTO,
        self::FECHA_REMISION,
        self::SERIE,
        self::SERIE_CORTA,
        self::MOTOR,
        self::INTERCAMBIO,
        self::PROVEEDOR_ID,
        self::ESTATUS_ID,
        self::UBICACION_ID,
        self::COMENTARIO,
        self::ULTIMO_SERVICIO,
        self::ID_STATUS_UNIDAD,
        self::UBICACION_LLAVES_ID,
        self::COMENTARIO,
        self::LEYENDA_DCTO
    ];
}
