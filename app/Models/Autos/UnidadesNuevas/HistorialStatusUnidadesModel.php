<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class HistorialStatusUnidadesModel extends Modelo
{
    protected $table = 'historial_estatus_unidades';
    const ID = "id";
    const ID_STATUS_ACTUAL = "id_status_actual";
    const ID_STATUS_NUEVO = "id_status_nuevo";
    const ID_USUARIO = "id_usuario";
    const ID_UNIDAD = "id_unidad";
    const ID_UBICACION = "id_ubicacion";
    const COMENTARIO = "comentario";
    const ID_UBICACION_LLAVES = "id_ubicacion_llaves";
    const IMAGEN = "imagen";
    const DIRECTORIO_IMAGEN = 'images_historial_unidades';
    protected $fillable = [
        self::ID_STATUS_ACTUAL,
        self::ID_STATUS_NUEVO,
        self::ID_USUARIO,
        self::ID_UNIDAD,
        self::ID_UBICACION,
        self::COMENTARIO,
        self::ID_UBICACION_LLAVES,
        self::IMAGEN
    ];


}
