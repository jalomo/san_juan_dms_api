<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class DetalleCostosRemisionModel extends Modelo
{
    protected $table = 'detalle_costos_remision';
    const ID = "id";
    const REMISIONID = "remision_id";
    const C_VALOR_UNIDAD = "c_valor_unidad";
    const C_EQUIPO_BASE = "c_equipo_base";
    const C_TOTAL_BASE = "c_total_base";
    const C_DEDUCCION_FORD = "c_deduccion_ford";
    const C_SEG_TRASLADO = "c_seg_traslado";
    const C_GASTOS_TRASLADO = "c_gastos_traslado";
    const C_IMP_IMPORT = "c_imp_import";
    const C_FLETES_EXT = "c_fletes_ext";
    const C_ISAN = "c_isan";
    const C_HOLDBACK = "c_holdback";
    const C_DONATIVO_CCF = "c_donativo_ccf";
    const C_PLAN_PISO = "c_plan_piso";
    

    protected $fillable = [
        self::REMISIONID,
        self::C_VALOR_UNIDAD,
        self::C_EQUIPO_BASE,
        self::C_TOTAL_BASE,
        self::C_DEDUCCION_FORD,
        self::C_SEG_TRASLADO,
        self::C_GASTOS_TRASLADO,
        self::C_IMP_IMPORT,
        self::C_FLETES_EXT,
        self::C_ISAN,
        self::C_HOLDBACK,
        self::C_DONATIVO_CCF,
        self::C_PLAN_PISO
    ];
}
