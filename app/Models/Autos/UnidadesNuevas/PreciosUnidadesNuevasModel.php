<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
class PreciosUnidadesNuevasModel extends Modelo
{
    protected $table = 'precios_activos_un';
    const ID = "id";
    const MODELO = 'modelo';
    const DESCRIPCION = 'descripcion';
    const FECHA_INICIO = 'fecha_inicio';
    const FECHA_FIN = 'fecha_fin';
    const ACTIVO = 'activo';

    protected $fillable = [
        self::MODELO,
        self::DESCRIPCION,
        self::FECHA_INICIO,
        self::FECHA_FIN,
        self::ACTIVO,
    ];
}
