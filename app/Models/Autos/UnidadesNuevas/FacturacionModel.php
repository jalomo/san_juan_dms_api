<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class FacturacionModel extends Modelo
{
    protected $table = 'facturacion_unidades_nuevas';
    const ID = 'id';
    const REMISION_ID = 'remision_id';
    const PROCEDENCIA = 'procedencia';
    const VENDEDOR = 'vendedor';
    const ADUANA_ID = 'aduana_id';
    const REPUVE = 'repuve';

    protected $fillable = [
        self::REMISION_ID,
        self::PROCEDENCIA,
        self::VENDEDOR,
        self::ADUANA_ID,
        self::REPUVE
    ];
}
