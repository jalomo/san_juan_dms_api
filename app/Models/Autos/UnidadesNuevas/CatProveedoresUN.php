<?php

namespace App\Models\Autos\UnidadesNuevas;

use App\Models\Core\Modelo;

class CatProveedoresUN extends Modelo
{
    protected $table = 'cat_proveedores_un';
    const ID = "id";
    const CLAVE = 'clave';
    const NOMBRE = 'nombre';

    protected $fillable = [
        self::CLAVE,
        self::NOMBRE,
    ];
}
