<?php

namespace App\Models\Autos\UnidadesNuevas;
use App\Models\Core\Modelo;
class CatStatusRecepcionUNModel extends Modelo
{
    protected $table = 'cat_estatus_recepcion_un';
    const ID = "id";
    const ESTATUS = 'estatus';

    protected $fillable = [
        self::ESTATUS
    ];
}
