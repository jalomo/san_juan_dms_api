<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class EstatusVentaAutosModel extends Modelo
{
    protected $table = 'estatus_venta_autos';
    const ID = "id";
    const NOMBRE = "nombre";

    const ESTATUS_PRE_PEDIDO = 1;
    const ESTATUS_CREDITO_APROBADO = 2;
    const ESTATUS_CREDITO_RECHAZADO = 3;
    const ESTATUS_AUTO_PENDIENTE = 4;
    const ESTATUS_ABONO_PAGADO = 5;
    const ESTATUS_ASESOR_NO_PROCESADO_ABONO  = 6;
    const ESTATUS_PAGO_PENDIENTE = 7;
    const ESTATUS_PAGO_LISTO_PENDIENTE_CAJA = 8;
    const ESTATUS_PAGO_TOTAL_PENDIENTE = 9;
    const ESTATUS_PAGO_TOTAL_LISTO = 10;
    const ESTATUS_DOCUMENTOS_FACTURA_PENDIENTE = 11;
    const ESTATUS_CONTRATOS_PENDIENTES = 12;
    const ESTATUS_CONTRATOS_LISTOS = 13;
    const ESTATUS_PASE_SALIDA_PENDIENTE = 14;
    const ESTATUS_PASE_SALIDA_LISTO = 15;
    const ESTATUS_UNIDAD_ENTREGADA = 16;
    const ESTATUS_PREVENTA_SIN_UNIDAD_INVENTARIO = 17;

    protected $fillable = [
        self::NOMBRE
    ];
}
