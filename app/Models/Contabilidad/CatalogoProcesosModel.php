<?php

namespace App\Models\Contabilidad;

use App\Models\Core\Modelo;

class CatalogoProcesosModel extends Modelo
{
    protected $table = 'catalogo_procesos';
    const ID = "id";
    const DESCRIPCION = "descripcion";
    
    
    const CAT_VENTAS = 1;
    const CAT_TRASPASO = 2;
    const CAT_COMPRAS = 3;
    const CAT_DEVOLUCION_PROVEEDOR = 4;
    const CAT_DEVOLUCION_VENTA = 5;
    const CAT_VENTA_AUTOS_NUEVOS = 6;
    const CAT_VENTA_AUTOS_SEMINUEVOS = 7;
    const CAT_MANO_DE_OBRA = 8;
    const CAT_BANORTE = 9;
    const OTROS_PROCESOS = 10;
    const SERVICIOS = 11;


    protected $fillable = [
        self::DESCRIPCION,
    ];
}
