<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatTipoSeguroModel extends Modelo
{
    protected $table = 'cat_tipo_seguros';
    const ID = 'id';
    const SEGURO = 'seguro';
    const VALOR = 'valor';

    protected $fillable = [
        self::ID,
        self::SEGURO,
        self::VALOR
    ];
}
