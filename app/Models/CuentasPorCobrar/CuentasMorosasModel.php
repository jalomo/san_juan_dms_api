<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class CuentasMorosasModel extends Modelo
{
    protected $table = 'ca_cuenta_morosas';
    const ID = "id";
    const CUENTA_POR_COBRAR_ID = "cuenta_por_cobrar_id";
    const ABONO_ID = "abono_id";
    const DIAS_MORATORIOS = 'dias_moratorios';
    const MONTO_MORATORIO = 'monto_moratorio';
    protected $dates = ['deleted_at']; //Registramos la nueva columna


    protected $fillable = [
        self::CUENTA_POR_COBRAR_ID,
        self::ABONO_ID,
        self::DIAS_MORATORIOS,
        self::MONTO_MORATORIO,
    ];
}
