<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class TipoAsientoModel extends Modelo
{
    protected $table = 'tipo_asiento';
    const ID = "id";
    const DESCRIPCION = "descripcion";

    protected $fillable = [
        self::ID,
        self::DESCRIPCION
    ];
}
