<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class SolicitudCreditoModel extends Modelo
{
    protected $table = 'solicitud_credito';
    
    const ID = "id";
    const CLIENTE_ID = "cliente_id";
    const BURO_CREDITO = "buro_credito";
    const REFERENCIA_NOMBRE = "referencia_nombre";
    const REFERENCIA_TELEFONO = "referencia_telefono";

    const REFERENCIA_NOMBRE_2 = "referencia_nombre_2";
    const REFERENCIA_TELEFONOO_2 = "referencia_telefono_2";
    
    protected $fillable = [
        self::CLIENTE_ID,
        self::BURO_CREDITO,
        self::REFERENCIA_NOMBRE,
        self::REFERENCIA_TELEFONO,
        self::REFERENCIA_NOMBRE_2,
        self::REFERENCIA_TELEFONOO_2
    ];

    public function documentos()
    {
        return $this->hasMany(DocumentosSolicitudModel::class, DocumentosSolicitudModel::ID_SOLICITUD, self::ID); 
      }
}
