<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class DetalleAsientoCuentasModel extends Modelo
{
    protected $table = 'detalle_asiento_cuentas';
    const ID = "id";
    const CUENTA_POR_COBRAR_ID = "cuenta_por_cobrar_id";
    const ABONO_ID = "abono_id";
    const TIPO_ASIENTO_ID = "tipo_asiento_id";
    const CUENTA_ID = "cuenta_id";
    const TOTAL_PAGO = "total_pago";
    const FECHA_PAGO = 'fecha_pago';
    const POLIZA_ID = 'poliza_id';
    const REFERENCIA = 'referencia';
    const ESTATUS_ID = 'estatus_id';

    const API_CONTABILIDAD = 'https://planificadorempresarial.com/contabilidad_dms/index.php/api/';

    protected $fillable = [
        self::CUENTA_POR_COBRAR_ID,
        self::ABONO_ID,
        self::TIPO_ASIENTO_ID,
        self::CUENTA_ID,
        self::TOTAL_PAGO,
        self::FECHA_PAGO,
        self::POLIZA_ID,
        self::REFERENCIA,
        self::ESTATUS_ID
    ];
}
