<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class EstatusAsientosModel extends Modelo
{
    protected $table = 'estatus_asientos';
    const ID = "id";
    const NOMBRE = "nombre";

    const ESTATUS_PENDIENTE = 1;
    const ESTATUS_ENVIADO = 2;
    const ESTATUS_CANCELADO = 3;

    protected $fillable = [
        self::NOMBRE
    ];
}
