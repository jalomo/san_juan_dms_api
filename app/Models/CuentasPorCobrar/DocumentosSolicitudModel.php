<?php

namespace App\Models\CuentasPorCobrar;

use App\Models\Core\Modelo;

class DocumentosSolicitudModel extends Modelo
{
    protected $table = 'documentos_solicitud';
    
    const ID = "id";
    const ID_SOLICITUD = "id_solicitud";
    const RUTA_ARCHIVO = "ruta_archivo";
    const NOMBRE_ARCHIVO = "nombre_archivo";
    const ID_TIPO_DOCUMENTO = "id_tipo_documento";
    
    const IDENTIFICACION = "identificacion";
    const ID_TIPO_IDENTIFICACION = 1;
    
    const RECIBO_NOMINA = "recibo_nomina";
    const ID_TIPO_RECIBO_NOMINA = 2;
    
    const COMPROBANTE_DOMICILIO = "comprobante_domicilio";
    const ID_TIPO_COMPROBANTE_DOMICILIO = 3;

    const SOLICITUD_CREDITO = "solicitud_credito";
    const ID_TIPO_SOLICITUD_CREDITO = 4;

    protected $fillable = [
        self::ID_SOLICITUD,
        self::RUTA_ARCHIVO,
        self::NOMBRE_ARCHIVO,
        self::ID_TIPO_DOCUMENTO
    ];
}
