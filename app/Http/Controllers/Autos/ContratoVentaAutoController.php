<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\ContratoVentaAutoModel;
use App\Servicios\Autos\ServicioContratoVentaAuto;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use Throwable;

class ContratoVentaAutoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioContratoVentaAuto();
    }

    public function getByIdVenta($id_venta)
    {
        try {
            
            $modelo = $this->servicio->getByIdVenta($id_venta);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function crearActualizarContrato(Request $request)
    {
        try {

            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->crearActualizarContrato(
                $request->get(ContratoVentaAutoModel::ID_VENTA_AUTO),
                $request->all()
            );
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
