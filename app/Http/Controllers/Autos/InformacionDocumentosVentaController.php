<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\InformacionDocumentosVentaModel;
use App\Servicios\Autos\ServicioInformacionDocumentosVenta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class InformacionDocumentosVentaController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioInformacionDocumentosVenta();
    }

    public function store(Request $request)
    {
        try {

            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $existe = $this->servicio->getByIdVentaUnidad($request->get(InformacionDocumentosVentaModel::ID_VENTA_UNIDAD));
            if (!empty($existe->id_informacion_documentos)) {
                $modelo = $this->servicio->massUpdateWhereId('id', $existe->id_informacion_documentos, $request->all());
                $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
                return Respuesta::json($modelo, 200, $mensaje);
            }
            $modelo = $this->servicio->crear($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByIdVentaUnidad($id_unidad)
    {
        return Respuesta::json($this->servicio->getByIdVentaUnidad($id_unidad), 200);
    }
}
