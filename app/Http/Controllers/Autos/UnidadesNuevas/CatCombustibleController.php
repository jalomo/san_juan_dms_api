<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioCatCombustible;
use Illuminate\Http\Request;

class CatCombustibleController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatCombustible();
    }
}
