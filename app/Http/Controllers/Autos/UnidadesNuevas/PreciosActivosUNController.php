<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioPreciosActivos;
class PreciosActivosUNController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioPreciosActivos();
    }
}
