<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioCatLineas;
use Illuminate\Http\Request;

class CatLineasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatLineas();
    }
}
