<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Core\CrudController;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Servicios\Autos\UnidadesNuevas\ServicioRemisiones;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use Illuminate\Http\Request;

class RemisionesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioRemisiones();
        $this->servicioArchivos = new ServicioManejoArchivos();
    }
    public function getUnidadesParameters(Request $request)
    {
        try {

            return ['data' => $this->servicio->getUnidades($request->all()), 200];
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function index()
    {
        return Respuesta::json($this->servicio->getunidadesNuevas([]), 200);
    }

    public function show($id)
    {
        $modelo = $this->servicio->getUnidadById($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function getTotalUnidadesnuevas()
    {
        return Respuesta::json($this->servicio->getTotalUnidadesnuevas(), 200);
    }

    public function uploadRemisionXml(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, [RemisionModel::XML_REMISION => 'required|mimes:xml']);
            $file = $request->file(RemisionModel::XML_REMISION);
            $newFileName = $this->servicioArchivos->setFileName($file);
            $directorio = $this->servicioArchivos->setDirectory(RemisionModel::DIRECTORIO_FACTURAS);
            $this->servicioArchivos->upload($file, $directorio, $newFileName);
            $path =  DIRECTORY_SEPARATOR . $directorio  . DIRECTORY_SEPARATOR . $newFileName;

            $xml_parameters = $this->servicio->handleDataXmlRemision($path);
            $mensaje = __(static::$I0011_FACTURA_SUBIDA, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($xml_parameters, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function updateRemision(Request $request, $id)
    {
        try {
            $reglas = [
                RemisionModel::FECHA_RECEPCION => 'required',
                RemisionModel::UBICACION_ID => 'nullable',
                RemisionModel::UBICACION_LLAVES_ID => 'nullable',
                RemisionModel::ESTATUS_ID => 'nullable',
                RemisionModel::COMENTARIO => 'nullable',
                RemisionModel::ULTIMO_SERVICIO => 'nullable'

            ];
            ParametrosHttpValidador::validar($request, $reglas);
            $remision = $this->servicio->updateRemision($request->all(), $id);
            $mensaje = __(static::$I0011_FACTURA_SUBIDA, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($remision, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function getByParametros(Request $request)
    {
        return Respuesta::json($this->servicio->getunidadesNuevas($request->all()), 200);
    }
}
