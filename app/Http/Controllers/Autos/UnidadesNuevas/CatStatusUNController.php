<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioEstatusUN;
use Illuminate\Http\Request;

class CatStatusUNController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioEstatusUN();
    }
}
