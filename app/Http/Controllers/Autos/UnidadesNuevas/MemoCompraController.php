<?php

namespace App\Http\Controllers\Autos\UnidadesNuevas;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\UnidadesNuevas\ServicioMemoCompra;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class MemoCompraController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioMemoCompra();
    }
    public function getByRemision($id)
    {
        $modelo = $this->servicio->getByRemision($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }
}
