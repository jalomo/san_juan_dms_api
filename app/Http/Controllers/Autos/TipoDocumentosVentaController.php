<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\ServicioTipoDocumentosVenta;
use Illuminate\Http\Request;

class TipoDocumentosVentaController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioTipoDocumentosVenta();
    }
}
