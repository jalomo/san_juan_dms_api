<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\ServicioEquipoOpcional;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class EquipoOpcionalController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioEquipoOpcional();
    }

    //Muestra por id de preventa - carrito de equipo opcional
    public function show($preventa_id)
    {
        $modelo = $this->servicio->getByPreVentaId($preventa_id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->store($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getVentaAutosEquipoOpcional()
    {
        return Respuesta::json($this->servicio->getVentaConEquipoOpcional(), 200);
    }
}
