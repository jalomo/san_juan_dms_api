<?php

namespace App\Http\Controllers\Financiamientos;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Financiamientos\ServicioCatComisiones;
use Illuminate\Http\Request;

class catComisionesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatComisiones();
    }
}
