<?php

namespace App\Http\Controllers\Usuarios;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Usuarios\Menu\ServicioUsuariosModulos;
use App\Servicios\Usuarios\ServicioUsuarios;
use App\Servicios\Usuarios\Log\ServicioLogSessiones;
use App\Models\Usuarios\Log\LogSessionesModel;
use App\Models\Usuarios\User;
use App\Servicios\Refacciones\ServicioCurl;

class UsuariosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioUsuarios();
        $this->servicioUsuariosModulos = new ServicioUsuariosModulos();
        $this->servicioLogSessiones = new ServicioLogSessiones();
        $this->servicioCurl = new ServicioCurl();
    }

    public function index()
    {
        return Respuesta::json($this->servicio->getAll(), 200);
    }

    public function login(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasLogin());
            $credenciales = request([User::USUARIO, User::PASSWORD, LogSessionesModel::DIRECCION_IP]);
            $usuarioModel = $this->servicio->validarCredenciales($credenciales);
            return Respuesta::json($usuarioModel, 200, __(static::$I0002_LOGIN_SUCCESSFUL));
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function registro(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $this->servicioCurl->curlPost(
                env('URL_SERVICIOS') . 'usuarios/index',
                [
                    User::NOMBRE => $request->get(User::NOMBRE),
                    User::USUARIO => $request->get(User::USUARIO),
                    User::PASSWORD => $request->get(User::PASSWORD),
                    User::EMAIL => $request->get(User::EMAIL),
                    User::TELEFONO => $request->get(User::TELEFONO),
                ],
                false
            );

            $registro = $this->crearUsuario($request);
            return Respuesta::json($registro, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function logout(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicioLogSessiones->getReglasGuardar());
            return Respuesta::json($this->servicioLogSessiones->crear($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function crearUsuario(Request $request)
    {
        $usuario = new User([
            User::NOMBRE => $request[User::NOMBRE],
            User::APELLIDO_PATERNO => $request[User::APELLIDO_PATERNO],
            User::APELLIDO_MATERNO => $request[User::APELLIDO_MATERNO],
            User::TELEFONO => $request[User::TELEFONO],
            User::NOMBRE => $request[User::NOMBRE],
            User::EMAIL => $request[User::EMAIL],
            User::USUARIO => $request[User::USUARIO],
            User::ROL_ID => $request[User::ROL_ID],
            User::RFC => $request[User::RFC],
            User::ESTATUS_ID => 1,
            User::PASSWORD => $request[User::PASSWORD] //bcrypt($request[User::PASSWORD])
        ]);

        return $this->servicio->guardarModelo($usuario);
    }

    public function storeUsuariosModulos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicioUsuariosModulos->reglasStoreUsuariosModulos());
            return Respuesta::json($this->servicioUsuariosModulos->storeUsuariosModulos($request->all()), 201);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function update(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
            $this->servicioCurl->curlPost(
                env('URL_SERVICIOS') . 'usuarios/index/' . $id,
                $request->all(),
                false
            );
            return Respuesta::json($this->servicio->update($request, $id));
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function usuariosModulos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicioUsuariosModulos->getReglasGetModulosByUsuarios());
            return Respuesta::json($this->servicioUsuariosModulos->usuariosAndModulos($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function getUsuariosByParametros(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->getUserByRoles($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
