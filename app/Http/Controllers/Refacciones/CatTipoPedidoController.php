<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioCatTipoPedido;
use Illuminate\Http\Request;

class CatTipoPedidoController extends CrudController
{
     public function __construct()
    {
        $this->servicio = new ServicioCatTipoPedido();
    }
}
