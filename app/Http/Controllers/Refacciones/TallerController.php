<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioTalleres;
use Illuminate\Http\Request;

class TallerController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioTalleres();
    }
}
