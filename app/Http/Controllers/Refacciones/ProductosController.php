<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use App\Servicios\Facturas\ServicioFacturacion;
use App\Servicios\Refacciones\ServicioProductos;
use App\Servicios\Refacciones\ServicioVentaProducto;
use App\Servicios\Refacciones\ServicioListaProductosOrdenCompra;
use App\Models\Facturas\Factura;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Http\Request;
use Throwable;

class ProductosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioProductos();
        $this->servicioFacturas = new ServicioFacturacion();
        $this->servicioArchivos = new ServicioManejoArchivos();
        $this->servicioVentaProducto = new ServicioVentaProducto();
        $this->servicioProductoOrdenCompra = new ServicioListaProductosOrdenCompra();
    }

    public function storeproductos(Request $request)
    {
        try {
            $producto = $this->servicio->existeProducto($request);
            if (count($producto) > 0) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
                $modelo = $this->servicio->updateproductos($request, $producto[0]->id);
                $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
                return Respuesta::json($modelo, 200, $mensaje);
            } else {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
                $modelo = $this->servicio->storeproductos($request);
                $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
                return Respuesta::json($modelo, 201, $mensaje);
            }
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function uploadFacturaProductos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicioFacturas->getReglasUploadxml());
            $file = $request->file(Factura::ARCHIVO_FACTURA);
            $newFileName = $this->servicioFacturas->setFileName($file);
            $directorio = $this->servicioFacturas->setDirectory(ServicioFacturacion::DIRECTORIO_FACTURAS);
            $this->servicioArchivos->upload($file, $directorio, $newFileName);
            $path =  DIRECTORY_SEPARATOR . $directorio  . $newFileName;
            $factura = $this->servicioFacturas->handleDataXmlFactura($path);
            $this->servicio->setFacturaId($factura);
            $modelo = $this->servicio->guardarDataFacturaProducto(['xml_path' => $path]);
            return Respuesta::json($modelo, 200);
        } catch (Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getProductoById($id)
    {
        try {
            $modelo = $this->servicio->getproductodataByid($id);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getAllProductos(Request $request)
    {
        try {
            $productos = $this->servicio->getAllProductos($request);
            return Respuesta::json($productos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getAllStockProducto(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->stockProductos($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getStockByProductoId(int $producto_id)
    {
        try {
            return Respuesta::json($this->servicio->stockByProductoId($producto_id), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getCantidadActualByProducto(Request $request)
    {
        try {
            return Respuesta::json($this->servicio->getCantidadActualByProducto($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function listadoStockProductos(Request $request)
    {
        try {

            return ['data' => $this->servicio->stockProductos($request->all())];
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getByNumeroPiezaDescripcion(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaPiezas());
            $modelo = $this->servicio->searchProductos($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function descontarProductos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasDescontarAumentar());
            $modelo = $this->servicio->descontarProductos($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function aumentarProductos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasDescontarAumentar());
            $modelo = $this->servicio->aumentarProductos($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function validarCantidadProducto(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasValidarCantidad());
            $modelo = $this->servicio->validarRestarCantidadProducto($request->all());
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateProductoUbicacion(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdateUbicacionProducto());
            $producto_ubicacion = $this->servicio->customUpdate($request, $id, $this->servicio->getReglasUpdateUbicacionProducto());
            return Respuesta::json($producto_ubicacion);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateProductsPrecio(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, [ProductosModel::ARCHIVO_UNIVERSO_PARTES => 'required']);
            $file = $request->file(ProductosModel::ARCHIVO_UNIVERSO_PARTES);
            $newFileName = $this->servicioArchivos->setFileName($file);
            $directorio = $this->servicioArchivos->setDirectory(ProductosModel::RUTA_ARCHIVO_UNIVERSO_PARTES);

            $this->servicioArchivos->upload($file, $directorio, $newFileName);
            $path =  DIRECTORY_SEPARATOR . $directorio  . DIRECTORY_SEPARATOR . $newFileName;

            $real_path = realpath('../storage/app/' . $path);
            $data_updated = null;
            $no_actualizados = 0;
            $actualizados = 0;
            if (file_exists($real_path)) {
                $read_file = fopen($real_path, 'r');
                while (!feof($read_file)) {
                    $buffer = fgets($read_file, 4096);
                    $prefijo = trim(substr($buffer, 1, 8));
                    $basico  = trim(substr($buffer, 9, 6));
                    $sufijo  = trim(substr($buffer, 15, 8));
                    $no_parte = "$prefijo$basico$sufijo";
                    $nuevo_precio  = floatval(trim(substr($buffer, 69, 13)));
                    $nuevo_precio_publico  = floatval(trim(substr($buffer, 173, 13)));
                    
                    $response_update = $this->servicio->validatePrecioProducto($prefijo, $basico, $sufijo, $no_parte, $nuevo_precio, $nuevo_precio_publico);
                    if ($response_update) {
                        $actualizados += 1;
                        $data_updated['actualizados'] = $actualizados;
                    } else {
                        $no_actualizados += 1;
                        $data_updated['no_actualizados'] = $no_actualizados;
                    }
                    // }
                    /* ------*/
                }
                fclose($read_file);
                $this->servicioArchivos->deleteFile($path); //Se borra para no llenar el disco
                return Respuesta::json($data_updated, 200);
            } else {
                return Respuesta::json($data_updated, 404);
            }
        } catch (Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function handleLine($linea)
    {
        $explode_linea = explode("    ", $linea);
        $no_parte = trim($explode_linea[1]);
        $nuevo_precio = preg_match('/[0-9]{1,5}(\.[0-9]{1,2})/', $linea, $m) ? floatval($m[0]) : 0;
        $this->servicio->validatePrecioProducto($no_parte, $nuevo_precio);
    }
}
