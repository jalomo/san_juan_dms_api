<?php

namespace App\Http\Controllers\Refacciones\Kardex;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\Kardex\ServicioTipoMovimientos;
use Illuminate\Http\Request;

class TipoMovimientosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioTipoMovimientos();
    }
}
