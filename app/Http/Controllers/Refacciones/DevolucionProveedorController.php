<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioOrdenCompra;
use App\Servicios\Refacciones\ServicioDevolucionProveedor;
use App\Servicios\Refacciones\ServicioFolios;
use App\Servicios\Refacciones\ServicioReOrdenCompraEstatus;
use App\Servicios\CuentasPorPagar\ServicioCuentaPorPagar;
use Illuminate\Http\Request;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Refacciones\DevolucionVentasModel;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use Illuminate\Support\Facades\DB;
class DevolucionProveedorController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDevolucionProveedor();
        $this->servicioReOrdenCompraEstatus = new ServicioReOrdenCompraEstatus();
        $this->servicioFolios = new ServicioFolios();
        $this->servicioOrdenCompra = new ServicioOrdenCompra();
        $this->servicioCuentaPorPagar = new ServicioCuentaPorPagar();

    }
    public function store(Request $request)
	{
        DB::beginTransaction();

		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $params = $request->all();
            // Verificar si existen ventas de los productos de la orden compra
            $this->servicio->verificaVentasOrdenCompra($params['orden_compra_id']);
            $orden_compra = $this->servicioOrdenCompra->getById($params['orden_compra_id']);
            $folio_orden_compra = $orden_compra ? $orden_compra->folio_id : null;
            $folio = $this->servicioFolios->generarFolio(CatalogoProcesosModel::CAT_DEVOLUCION_PROVEEDOR);
            $this->servicioCuentaPorPagar->changeEstatusByFolio(EstatusCuentaModel::ESTATUS_CANCELADO, $folio_orden_compra);

            $params[DevolucionVentasModel::FOLIO_ID] = $folio->id;

            $devolucion = $this->servicio->crear($params);
            //historico detalle orden compra devolucion
			if (true) {
                $descontarAlmacen = $this->servicio->descontarAlmacenPorOrdenCompra($params['orden_compra_id']);
                if(!$descontarAlmacen){
                    DB::rollback();
                }
                $reEstatusCompras = [
                    'orden_compra_id' => $params['orden_compra_id'],
                    'estatus_compra_id' =>EstatusCompra::ESTATUS_DEVOLUCION
                ];
				$this->servicioReOrdenCompraEstatus->storeReOrdenCompraEstatus($reEstatusCompras);
			} else {
                //Si algo sale mal se restaura
                DB::rollback();
            }
			$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            DB::commit();
            //proceso para actualizar inventario de productos de la orden compra

            $this->servicio->actualizarInventarioPorOrdenCompra($params['orden_compra_id']);

            //Se devuelve la respuesta satisfactoria de la orden compra
            return Respuesta::json($devolucion, 200, $mensaje);
		} catch (\Throwable $e) {
            //dd($e);
			DB::rollback();
			return Respuesta::error($e);
		}
	}
    public function showByOrdenCompraId($orden_compra_id)
    {
        try {
            $result = $this->servicio->getByOrdenCompraId($orden_compra_id);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getDevolucionProveedorByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function totalesSearchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaTraspaso());
            $result = $this->servicio->getTotalesDevolucionProveedorByFechas($request);
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

}
