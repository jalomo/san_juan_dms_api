<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioCatTipoPago;


class CatTipoPagoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatTipoPago();
    }
}
