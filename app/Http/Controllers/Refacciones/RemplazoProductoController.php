<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioProductos;
use App\Servicios\Refacciones\ServicioRemplazoProducto;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Http\Request;

class RemplazoProductoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioRemplazoProducto();
        $this->servicioProducto = new ServicioProductos();
    }

    public function actualizarProductoAlmacen(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request,$this->servicio->getReglasGuardar());
            $data = [
                ProductosModel::DESCRIPCION => $request->nombre_producto_actual,
                ProductosModel::NO_IDENTIFICACION_FACTURA => $request->no_identificacion
            ];
            
            $this->servicioProducto->updateProductoAlmacen($request->producto_id, $data);
            $modelo = $this->servicio->store($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
