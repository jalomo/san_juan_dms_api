<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\Utilidades\Utilidades;
use App\Servicios\Refacciones\ServicioEnvioPedidoDetalle;
use Illuminate\Http\Request;

class EnvioPedidoDetalleController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioEnvioPedidoDetalle();
        $this->servicioUtilidades = new Utilidades();
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            
            $dia_letra = $this->servicioUtilidades->fechaCastellano($request->get('dia_envio'));
            $request->merge(['dia_envio' => $dia_letra]);
            $modelo = $this->servicio->store($request);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
