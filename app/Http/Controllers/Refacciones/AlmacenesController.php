<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioAlmacenes;
use Illuminate\Http\Request;

class AlmacenesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioAlmacenes();
    }
}
