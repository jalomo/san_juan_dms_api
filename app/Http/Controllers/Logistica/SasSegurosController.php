<?php

namespace App\Http\Controllers\logistica;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Logistica\ServicioSasSeguros;
use Illuminate\Http\Request;

class SasSegurosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioSasSeguros();
    }
    public function getByParametros(Request $request){
        return Respuesta::json($this->servicio->getSasSeguros($request->all()), 200);
    }
}
