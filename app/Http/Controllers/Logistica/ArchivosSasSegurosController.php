<?php

namespace App\Http\Controllers\Logistica;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use App\Servicios\Logistica\ServicioArchivosSasSeguros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArchivosSasSegurosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioArchivosSasSeguros();
        $this->servicioArchivos = new ServicioManejoArchivos();
    }
    public function uploadFile(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpload());
            $file = $request->file('archivo');
            $newFileName = $this->servicioArchivos->setFileName($file);
            $directorio = $this->servicioArchivos->setDirectory('sas_seguros');
            $file_uploaded = $this->servicioArchivos->upload($file, $directorio, $newFileName);
            //Insertar
            $this->servicio->saveFile(['archivo'=>$file_uploaded,'seguro_id'=>$request->get('seguro_id')]);
            return Respuesta::json(['archivo'=>$file_uploaded], 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    
    public function getFiles($seguro_id){
        
        return Respuesta::json($this->servicio->getFilesBySeguro($seguro_id), 200);
    }
}
