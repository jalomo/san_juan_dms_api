<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioAbono;
use App\Servicios\CuentasPorCobrar\ServicioDetalleAbonoCuentas;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Mail\EmailAbonoRealizado;
use App\Models\CuentasPorCobrar\AbonosModel;
use Illuminate\Support\Facades\Mail;

class AbonosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioAbono();
        $this->servicioDetalleAbonoCuentas = new ServicioDetalleAbonoCuentas();
    }

    public function showByIdOrdenEntrada(Request $request)
    {
        try {

            $modelo = $this->servicio->getAbonosPorPagar($request->toArray());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function listadoAbonosByOrdenEntrada(Request $request)
    {
        try {
            $modelo = $this->servicio->getAbonosPorPagar($request->toArray());
            return Respuesta::json(['data' => $modelo], empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getVerificaAbonosPendientes(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasVerificaAbonosPendientes());
            $modelo = $this->servicio->getVerificaAbonosPendientes($request->toArray());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            if ($request->tipo_proceso_id != 10) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
            } else {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdateMasCuentas());
            }
            
            
            $request->merge(['abono_id' =>  $id]);
            $movimientos = json_encode($this->servicio->set_movimiento($request->toArray()));
            if ($movimientos) {
                $modelo = $this->servicio->actualiza_abonos($request, $id);
            }
            
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            //Mail::to($modelo->correo_electronico)->send(new EmailAbonoRealizado($modelo));
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateEstatus(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasEstatus());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->changeEstatus($request, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
