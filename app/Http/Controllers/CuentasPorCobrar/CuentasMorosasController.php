<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioCuentasMorosas;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class CuentasMorosasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCuentasMorosas();
    }

    public function getBusqueda(Request $request)
    {
        try {
            // ParametrosHttpValidador::validar($request, $this->servicio->getReglasFiltrosFechas());
            $result = $this->servicio->getBusqueda($request);
 
            return Respuesta::json($result, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
