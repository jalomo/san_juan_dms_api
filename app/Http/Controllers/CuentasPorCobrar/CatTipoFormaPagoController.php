<?php

namespace App\Http\Controllers\CuentasPorCobrar;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioTipoFormaPago;


class CatTipoFormaPagoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioTipoFormaPago();
    }
}
