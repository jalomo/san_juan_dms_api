<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioDetalleAbonoCuentas;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Models\CuentasPorCobrar\DetalleAsientoCuentasModel;
use Illuminate\Http\Request;


class DetalleAbonoCuentasController extends CrudController
{
	public function __construct()
	{
		$this->servicio = new ServicioDetalleAbonoCuentas();
	}


	public function showByCuentaPorCobrar($id)
	{
		$modelo = $this->servicio->showByCuentaPorCobrar($id);
		return Respuesta::json(['data' => $modelo], empty($modelo) ? 404 : 200);
	}

	public function store(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
			$poliza = $this->servicio->set_poliza();
			//if ($poliza && isset($poliza->id_id)) {
				//$request->merge([DetalleAsientoCuentasModel::POLIZA_ID =>  $poliza->id_id]);
				$modelo = $this->servicio->store($request);
				// if ($modelo) {
				// 	$this->servicio->curl_asiento_api($request->toArray());
				$mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
				// }
			//}

			return Respuesta::json($modelo, 201, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function updateEstatus(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasEstatus());
            $mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
            $modelo = $this->servicio->changeEstatus($request, $id);
            return Respuesta::json($modelo, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

	public function getFiltroFechas(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasFiltrosFechas());
			$result = $this->servicio->getFiltroFechas($request);
			return Respuesta::json($result, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}
}
