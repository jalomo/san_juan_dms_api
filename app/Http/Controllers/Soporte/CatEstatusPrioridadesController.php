<?php

namespace App\Http\Controllers\Soporte;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Soporte\ServicioCatPrioridades;
use Illuminate\Http\Request;

class CatEstatusPrioridadesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatPrioridades();
    }
}
