<?php

namespace App\Http\Controllers\Soporte;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Soporte\ServicioCatRoles;
use Illuminate\Http\Request;

class CatRolesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatRoles();
    }
}
