<?php

namespace App\Http\Controllers\Contabilidad;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Contabilidad\PolizasContabilidad;
use Illuminate\Http\Request;

class PolizasContabilidadController extends CrudController
{
    public function __construct() {
        $this->servicio = new PolizasContabilidad();
    }
}
