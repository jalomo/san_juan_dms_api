<?php

namespace App\Servicios\Telemarketing;

use App\Servicios\Core\ServicioDB;
use App\Models\Telemarketing\CatalogoOrigenesModel;
use App\Models\Telemarketing\HistorialComentariosVentasModel;

class ServicioHistorialComentariosVentas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'historial comentarios ventas';
        $this->modelo = new HistorialComentariosVentasModel();
    }

    public function getReglasGuardar()
    {
        return [
            HistorialComentariosVentasModel:: ID_VENTA_WEB => 'required',
            HistorialComentariosVentasModel:: COMENTARIO => 'required',
            HistorialComentariosVentasModel:: ID_USUARIO => 'required',
            HistorialComentariosVentasModel:: FECHA_NOTIFICACION => 'nullable|date',
            HistorialComentariosVentasModel:: ID_TIPO_COMENTARIO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            HistorialComentariosVentasModel:: ID_VENTA_WEB => 'required',
            HistorialComentariosVentasModel:: COMENTARIO => 'required',
            HistorialComentariosVentasModel:: ID_USUARIO => 'required',
            HistorialComentariosVentasModel:: FECHA_NOTIFICACION => 'nullable|date',
            HistorialComentariosVentasModel:: ID_TIPO_COMENTARIO => 'required'
        ];
    }
    public function buscarComentariosPorParametros($parametros){
        return $this->modelo->select(HistorialComentariosVentasModel::getTableName().'.*')
                            ->where(HistorialComentariosVentasModel::ID_TIPO_COMENTARIO,$parametros['id_tipo_comentario'])
                            ->where(HistorialComentariosVentasModel::ID_VENTA_WEB,$parametros['id_venta_web'])
                            ->orderBy(HistorialComentariosVentasModel::CREATED_AT,'desc')
                            ->get();
    }
}

