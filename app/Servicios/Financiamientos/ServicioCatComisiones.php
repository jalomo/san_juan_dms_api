<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\CatComisionModel;

class ServicioCatComisiones extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo comisiones financieras';
        $this->modelo = new CatComisionModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatComisionModel::COMISION => 'required',
            CatComisionModel::VALOR => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatComisionModel::COMISION => 'required',
            CatComisionModel::VALOR => 'required'
        ];
    }
}


