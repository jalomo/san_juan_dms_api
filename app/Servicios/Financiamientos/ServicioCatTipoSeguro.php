<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\CatComisionModel;
use App\Models\Financiamientos\CatTipoSeguroModel;

class ServicioCatTipoSeguro extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo tipo seguro';
        $this->modelo = new CatTipoSeguroModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatTipoSeguroModel::SEGURO => 'required',
            CatComisionModel::VALOR => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatTipoSeguroModel::SEGURO => 'required',
            CatComisionModel::VALOR => 'required'
        ];
    }
}


