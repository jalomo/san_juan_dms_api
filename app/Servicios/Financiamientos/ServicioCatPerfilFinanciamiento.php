<?php
namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\CatPerfilFinanciamientoModel;
class ServicioCatPerfilFinanciamiento extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo perfiles financiamientos';
        $this->modelo = new CatPerfilFinanciamientoModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatPerfilFinanciamientoModel::PERFIL => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatPerfilFinanciamientoModel::PERFIL => 'required'
        ];
    }
}


