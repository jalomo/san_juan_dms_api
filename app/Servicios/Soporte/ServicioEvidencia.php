<?php

namespace App\Servicios\Soporte;

use App\Models\Soporte\EvidenciaTicketModel;
use App\Models\Soporte\HistorialTicketsModel;
use App\Models\Soporte\TicketsModel;
use App\Models\Usuarios\User;
use App\Servicios\Core\ServicioDB;

class ServicioEvidencia extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'evidencia tickets';
        $this->modelo = new EvidenciaTicketModel();
        $this->ServicioTelefonosSoporte = new ServicioTelefonosSoporte();
        $this->ServicioCatRoles = new ServicioCatRoles();
    }

    public function getReglasGuardar()
    {
        return [
            EvidenciaTicketModel::EVIDENCIA => 'required',
            EvidenciaTicketModel::COMENTARIO => 'required',
            EvidenciaTicketModel::USER_ID => 'required',
            EvidenciaTicketModel::TICKET_ID => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            EvidenciaTicketModel::EVIDENCIA => 'required',
            EvidenciaTicketModel::COMENTARIO => 'required',
            EvidenciaTicketModel::USER_ID => 'required',
            EvidenciaTicketModel::TICKET_ID => 'required'
        ];
    }
    public function handleSaveEvidencia($path, $data)
    {

        $rol = $this->ServicioCatRoles->getById($data[TicketsModel::ROL_ID]);
        $ticket =  $this->crear([
            EvidenciaTicketModel::TICKET_ID => $data[EvidenciaTicketModel::TICKET_ID],
            EvidenciaTicketModel::USER_ID => $data[EvidenciaTicketModel::USER_ID],
            EvidenciaTicketModel::COMENTARIO => $data[EvidenciaTicketModel::COMENTARIO],
            EvidenciaTicketModel::EVIDENCIA => $path
        ]);

        //Enviar notificación
        $CONST_AGENCIA = 'FORD Mylsa San Juan del Río';
        $telefonos = $this->ServicioTelefonosSoporte->buscarTodos();
        $folio_ticket = $data[TicketsModel::FOLIO];
        $mensaje = "El usuario " . $data[HistorialTicketsModel::USUARIO] . " ha anexado evidencia en la agencia " . $CONST_AGENCIA . " con # de folio: $folio_ticket, fecha de creación: " . date('Y-m-d H:i:s') . ' Comentario: ' . $data[EvidenciaTicketModel::COMENTARIO] . ', rol: ' . $rol->rol;
        foreach ($telefonos as $t => $data) {
            $this->notificacion($data->telefono, $mensaje);
        }
        return $ticket;
    }
    public function notificacion($celular_sms = '', $mensaje_sms = '')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            "celular=" . $celular_sms . "&mensaje=" . $mensaje_sms . "&sucursal=M2139"
        );
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
    }
    public function getEvidenciaTicket($ticket_id)
    {
        return $this->modelo->select(
            EvidenciaTicketModel::getTableName() . '.' .EvidenciaTicketModel::ID,
            EvidenciaTicketModel::getTableName() . '.' .EvidenciaTicketModel::TICKET_ID,
            EvidenciaTicketModel::getTableName() . '.' .EvidenciaTicketModel::COMENTARIO,
            EvidenciaTicketModel::getTableName() . '.' .EvidenciaTicketModel::EVIDENCIA,
            User::getTableName() . '.' . User::NOMBRE . ' AS nombre_usuario',
            User::getTableName() . '.' . User::APELLIDO_PATERNO . ' AS ap_usuario',
            User::getTableName() . '.' . User::APELLIDO_MATERNO . ' AS am_usuario',
            User::getTableName() . '.' . User::EMAIL . ' AS email_usuario',
            User::getTableName() . '.' . User::TELEFONO . ' AS telefono_usuario'
        )
        ->join(
            User::getTableName(),
            User::getTableName() . '.' . User::ID,
            '=',
            EvidenciaTicketModel::getTableName() . '.' . EvidenciaTicketModel::USER_ID
        )
        ->where(
            EvidenciaTicketModel::TICKET_ID,
            $ticket_id
        )->get();
    }
}
