<?php

namespace App\Servicios\Soporte;

use App\Models\Soporte\CatStatusPrioridadesModel;
use App\Servicios\Core\ServicioDB;

class ServicioCatPrioridades extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo prioridades';
        $this->modelo = new CatStatusPrioridadesModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatStatusPrioridadesModel::PRIORIDAD => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatStatusPrioridadesModel::PRIORIDAD => 'required'
        ];
    }
}
