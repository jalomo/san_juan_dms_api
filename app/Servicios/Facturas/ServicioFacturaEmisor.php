<?php

namespace App\Servicios\Facturas;

use App\Servicios\Core\ServicioDB;
use App\Models\Facturas\EmisorFactura;

class ServicioFacturaEmisor extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new EmisorFactura();
    }

    public function guardarEmisor($id, array $data)
    {
        return $this->createOrUpdate(
            [EmisorFactura::FACTURA_ID => $id],
            $data
        );
    }
}
