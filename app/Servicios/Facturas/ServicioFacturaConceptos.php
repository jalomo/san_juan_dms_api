<?php

namespace App\Servicios\Facturas;

use App\Servicios\Core\ServicioDB;
use App\Models\Facturas\ConceptosFactura;

class ServicioFacturaConceptos extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new ConceptosFactura(); 
    }

    public function crearConcepto($factura_id, array $data)
    {
        return $this->createOrUpdate(
            [
                ConceptosFactura::FACTURA_ID => $factura_id,
                ConceptosFactura::CONCEPTO_DESCRIPCION => $data[ConceptosFactura::CONCEPTO_DESCRIPCION]
            ],
            $data
        );
    }
}
