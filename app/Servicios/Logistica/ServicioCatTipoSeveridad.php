<?php
namespace App\Servicios\Logistica;
use App\Servicios\Core\ServicioDB;
use App\Models\Logistica\CatTipoSeveridadModel;

class ServicioCatTipoSeveridad extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo tipo severidad';
        $this->modelo = new CatTipoSeveridadModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatTipoSeveridadModel::TIPO_SEVERIDAD => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatTipoSeveridadModel::TIPO_SEVERIDAD => 'required'
        ];
    }
}


