<?php

namespace App\Servicios\Seminuevos;

use App\Servicios\Core\ServicioDB;
use App\Models\Seminuevos\DocumentacionModel;

class ServicioDocumentacion extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'documentacion';
        $this->modelo = new DocumentacionModel();
    }

    public function getReglasGuardar()
    {
        return [
            DocumentacionModel::USUARIO_RECIBE => 'required',
            DocumentacionModel::FECHA_RECEPCION => 'required|date',
            DocumentacionModel::NUMERO_ECONOMICO => 'required',
            DocumentacionModel::ID_UNIDAD => 'required',
            DocumentacionModel::NUMERO_CLIENTE => 'required',
            DocumentacionModel::REGIMEN_FISCAL => 'required',
            DocumentacionModel::NOMBRE => 'required',
            DocumentacionModel::APELLIDO_PATERNO => 'required',
            DocumentacionModel::APELLIDO_MATERNO => 'required',
            DocumentacionModel::NOMBRE_EMPRESA => 'required',
            DocumentacionModel::COPIA_FACTURA_ORIGEN => 'nullable',
            DocumentacionModel::REFACTURACION_MYLSA => 'nullable',
            DocumentacionModel::FACTURA_ORIGINAL_ENDOSADA => 'nullable',
            DocumentacionModel::TENENCIAS_PAGADAS => 'nullable',
            DocumentacionModel::COPIA_TARJETA_CIRCULACION => 'nullable',
            DocumentacionModel::CERTIFICADO_VERIFICACION_VIGENTE => 'nullable',
            DocumentacionModel::MANUAL_PROPIETARIO => 'nullable',
            DocumentacionModel::POLIZA_GARANTIA => 'nullable',
            DocumentacionModel::CANCELACION_POLIZA_VIGENTE => 'nullable',
            DocumentacionModel::DUPLICADO_LLAVES => 'nullable',
            DocumentacionModel::CODIGO_RADIO => 'nullable',
            DocumentacionModel::BRILLO_SEGURIDAD => 'required',
            DocumentacionModel::CONTROL => 'required',
            DocumentacionModel::AUDIFONOS => 'required',
            DocumentacionModel::INE_FRONTAL => 'nullable',
            DocumentacionModel::INE_TRASERA => 'nullable',
            DocumentacionModel::CURP => 'nullable',
            DocumentacionModel::RFC_SAT => 'nullable',
            DocumentacionModel::COMPROBANTE_DOMICILIO => 'nullable',
            DocumentacionModel::CONSTANCIA_SITUACION_FISCAL => 'nullable',
            DocumentacionModel::ACTA_CONSTITUTIVA => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DocumentacionModel::USUARIO_RECIBE => 'required',
            DocumentacionModel::FECHA_RECEPCION => 'required|date',
            DocumentacionModel::ID_UNIDAD => 'nullable',
            DocumentacionModel::NUMERO_ECONOMICO => 'required',
            DocumentacionModel::NUMERO_CLIENTE => 'required',
            DocumentacionModel::REGIMEN_FISCAL => 'required',
            DocumentacionModel::NOMBRE => 'required',
            DocumentacionModel::APELLIDO_PATERNO => 'required',
            DocumentacionModel::APELLIDO_MATERNO => 'required',
            DocumentacionModel::NOMBRE_EMPRESA => 'required',
            DocumentacionModel::COPIA_FACTURA_ORIGEN => 'nullable',
            DocumentacionModel::REFACTURACION_MYLSA => 'nullable',
            DocumentacionModel::FACTURA_ORIGINAL_ENDOSADA => 'nullable',
            DocumentacionModel::TENENCIAS_PAGADAS => 'nullable',
            DocumentacionModel::COPIA_TARJETA_CIRCULACION => 'nullable',
            DocumentacionModel::CERTIFICADO_VERIFICACION_VIGENTE => 'nullable',
            DocumentacionModel::MANUAL_PROPIETARIO => 'nullable',
            DocumentacionModel::POLIZA_GARANTIA => 'nullable',
            DocumentacionModel::CANCELACION_POLIZA_VIGENTE => 'nullable',
            DocumentacionModel::DUPLICADO_LLAVES => 'nullable',
            DocumentacionModel::CODIGO_RADIO => 'nullable',
            DocumentacionModel::BRILLO_SEGURIDAD => 'required',
            DocumentacionModel::CONTROL => 'required',
            DocumentacionModel::AUDIFONOS => 'required',
            DocumentacionModel::INE_FRONTAL => 'nullable',
            DocumentacionModel::INE_TRASERA => 'nullable',
            DocumentacionModel::CURP => 'nullable',
            DocumentacionModel::RFC_SAT => 'nullable',
            DocumentacionModel::COMPROBANTE_DOMICILIO => 'nullable',
            DocumentacionModel::CONSTANCIA_SITUACION_FISCAL => 'nullable',
            DocumentacionModel::ACTA_CONSTITUTIVA => 'nullable'
        ];
    }
}
