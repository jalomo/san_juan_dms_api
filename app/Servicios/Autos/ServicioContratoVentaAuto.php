<?php

namespace App\Servicios\Autos;

use App\Models\Autos\ContratoVentaAutoModel;
use App\Models\Autos\UnidadesNuevas\CatLineas;
use App\Models\Autos\UnidadesNuevas\DetalleCostosRemisionModel;
use App\Models\Autos\UnidadesNuevas\DetalleRemisionModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\ClientesModel;
use App\Servicios\Core\ServicioDB;

class ServicioContratoVentaAuto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'contrato';
        $this->modelo = new ContratoVentaAutoModel();
        $this->modeloVentaAuto = new VentasAutosModel();
    }

    public function getReglasGuardar()
    {
        return [
            ContratoVentaAutoModel::FIRMA_CLIENTE => 'nullable',
            ContratoVentaAutoModel::FIRMA_GERENTE_CREDITO => 'nullable',
            ContratoVentaAutoModel::FIRMA_GERENTE_VENTAS => 'nullable',
            ContratoVentaAutoModel::ID_VENTA_AUTO => 'required'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            ContratoVentaAutoModel::FIRMA_CLIENTE => 'nullable',
            ContratoVentaAutoModel::FIRMA_GERENTE_CREDITO => 'nullable',
            ContratoVentaAutoModel::FIRMA_GERENTE_VENTAS => 'nullable',
            ContratoVentaAutoModel::ID_VENTA_AUTO => 'nullable'
        ];
    }

    public function crearActualizarContrato($id_venta_auto, array $data)
    {
        $existe = $this->modelo->where(ContratoVentaAutoModel::ID_VENTA_AUTO, '=', $id_venta_auto)->first();
        if (!$existe) {
            return $this->crear($data);
        } else {
            return $this->massUpdateWhereId(ContratoVentaAutoModel::ID_VENTA_AUTO, $id_venta_auto, $data);
        }
    }

    public function getByIdVenta($id_venta = '')
    {
        $tabla_contrato = ContratoVentaAutoModel::getTableName();
        $tabla_venta_auto = VentasAutosModel::getTableName();
        $tabla_unidades = RemisionModel::getTableName();
        $tabla_detalleRemision = DetalleRemisionModel::getTableName();
        $tabla_cat_colores = CatalogoColoresModel::getTableName();
        $tabla_detalle_costo_remision = DetalleCostosRemisionModel::getTableName();
        $tabla_lineas = CatLineas::getTableName();
        $tabla_cliente = ClientesModel::getTableName();
        $tabla_cxc = CuentasPorCobrarModel::getTableName();
        $tabla_plazo_credito = PlazoCreditoModel::getTableName();

        $query = $this->modeloVentaAuto
            ->select(
                // $tabla_contrato . '.*',
                // $tabla_contrato . '.*',
                $tabla_contrato . '.' . ContratoVentaAutoModel::ID,
                $tabla_contrato . '.' . ContratoVentaAutoModel::ID_VENTA_AUTO,
                $tabla_contrato . '.' . ContratoVentaAutoModel::FIRMA_CLIENTE,
                $tabla_contrato . '.' . ContratoVentaAutoModel::FIRMA_GERENTE_CREDITO,
                $tabla_contrato . '.' . ContratoVentaAutoModel::FIRMA_GERENTE_VENTAS,
                $tabla_contrato . '.' . ContratoVentaAutoModel::CREATED_AT .' as fecha_contrato',
                $tabla_cliente . '.' . ClientesModel::ID . ' as id_cliente',
                $tabla_cliente . '.' . ClientesModel::NOMBRE . ' as nombre_cliente',
                $tabla_cliente . '.' . ClientesModel::APELLIDO_PATERNO . ' as cliente_apellido_paterno',
                $tabla_cliente . '.' . ClientesModel::APELLIDO_MATERNO . ' as cliente_apellido_materno',
                $tabla_cliente . '.' . ClientesModel::RFC . ' as cliente_rfc',
                $tabla_cliente . '.' . ClientesModel::CORREO_ELECTRONICO . ' as cliente_correo',
                $tabla_cliente . '.' . ClientesModel::CODIGO_POSTAL,
                $tabla_cliente . '.' . ClientesModel::DIRECCION,
                $tabla_cliente . '.' . ClientesModel::COLONIA,
                $tabla_cliente . '.' . ClientesModel::CODIGO_POSTAL,
                $tabla_cliente . '.' . ClientesModel::NUMERO_EXT,
                $tabla_cliente . '.' . ClientesModel::ESTADO,
                $tabla_cliente . '.' . ClientesModel::TELEFONO . ' as cliente_telefono',

                $tabla_unidades . '.' . RemisionModel::ID . ' as id_remision',
                $tabla_unidades . '.' . RemisionModel::ECONOMICO, //validar si se quita
                $tabla_unidades . '.' . RemisionModel::CLAVE_VEHICULAR,
                $tabla_unidades . '.' . RemisionModel::SERIE,
                $tabla_unidades . '.' . RemisionModel::SERIE_CORTA,
                $tabla_lineas . '.' . CatLineas::MODELO . ' as anio_modelo',
                $tabla_lineas . '.' . CatLineas::LINEA,
                $tabla_lineas . '.' . CatLineas::DESCRIPCION . ' as descripcion_linea',
                'color_interior' . '.' . CatalogoColoresModel::NOMBRE . ' as color_interior',
                'color_exterior' . '.' . CatalogoColoresModel::NOMBRE . ' as color_exterior',
                $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::C_VALOR_UNIDAD . ' as precio_costo',
                $tabla_unidades . '.' . RemisionModel::C_TOTAL . ' as precio_venta',
                $tabla_venta_auto . '.' . VentasAutosModel::TIPO_VENTA,
                $tabla_venta_auto . '.' . VentasAutosModel::ID . ' as id_venta_auto',
                $tabla_venta_auto . '.' . VentasAutosModel::DESCUENTO,
                $tabla_venta_auto . '.' . VentasAutosModel::IVA,
                $tabla_venta_auto . '.' . VentasAutosModel::NO_ECONOMICO,
                $tabla_venta_auto . '.' . VentasAutosModel::IVA,
                // $tabla_venta_auto . '.' . VentasAutosModel::,
                $tabla_venta_auto . '.' . VentasAutosModel::CREATED_AT .' as fecha_venta',
                $tabla_cxc . '.' . CuentasPorCobrarModel::TOTAL,
                $tabla_cxc . '.' . CuentasPorCobrarModel::IMPORTE,
                $tabla_cxc . '.' . CuentasPorCobrarModel::ENGANCHE,
                $tabla_cxc . '.' . CuentasPorCobrarModel::TASA_INTERES,
                $tabla_cxc . '.' . CuentasPorCobrarModel::INTERESES,
                $tabla_cxc . '.' . CuentasPorCobrarModel::PLAZO_CREDITO_ID,
                $tabla_plazo_credito . '.' . PlazoCreditoModel::NOMBRE . ' as plazo_credito',
                $tabla_detalleRemision . '.' . DetalleRemisionModel::PUERTAS,
                $tabla_detalleRemision . '.' . DetalleRemisionModel::CILINDROS,
                $tabla_detalleRemision . '.' . DetalleRemisionModel::CAPACIDAD,
                $tabla_detalleRemision . '.' . DetalleRemisionModel::TRANSMISION
            )
            ->leftJoin(
                $tabla_contrato,
                $tabla_contrato . '.' . ContratoVentaAutoModel::ID_VENTA_AUTO,
                '=',
                $tabla_venta_auto . '.' . VentasAutosModel::ID
            )
            ->join(
                $tabla_unidades,
                $tabla_unidades . '.' . RemisionModel::ID,
                '=',
                $tabla_venta_auto . '.' . VentasAutosModel::ID_UNIDAD
            )
            ->join(
                $tabla_detalleRemision,
                $tabla_detalleRemision . '.' . DetalleRemisionModel::ID,
                '=',
                $tabla_unidades . '.' . RemisionModel::ID
            )
            ->join(
                $tabla_detalle_costo_remision,
                $tabla_detalle_costo_remision . '.' . DetalleCostosRemisionModel::REMISIONID,
                '=',
                $tabla_unidades . '.' . RemisionModel::ID
            )
            ->join(
                $tabla_cliente,
                $tabla_cliente . '.' . ClientesModel::ID,
                '=',
                $tabla_venta_auto . '.' . VentasAutosModel::ID_CLIENTE
            )
            ->join(
                $tabla_cxc,
                $tabla_cxc . '.' . CuentasPorCobrarModel::FOLIO_ID,
                '=',
                $tabla_venta_auto . '.' . VentasAutosModel::ID_FOLIO
            )
            ->join(
                $tabla_cat_colores . ' as color_interior',
                'color_interior.' . CatalogoColoresModel::ID,
                '=',
                $tabla_detalleRemision . '.' . DetalleRemisionModel::COLORINTID
            )
            ->join(
                $tabla_cat_colores . ' as color_exterior',
                'color_exterior.' . CatalogoColoresModel::ID,
                '=',
                $tabla_detalleRemision . '.' . DetalleRemisionModel::COLORINTID
            )
            ->join(
                $tabla_plazo_credito,
                $tabla_plazo_credito . '.' . PlazoCreditoModel::ID,
                '=',
                $tabla_cxc . '.' . CuentasPorCobrarModel::PLAZO_CREDITO_ID
            )
            ->join(
                $tabla_lineas,
                $tabla_lineas . '.' . CatLineas::ID,
                '=',
                $tabla_unidades . '.' . RemisionModel::LINEA_ID
            );

        if ($id_venta !== '') {
            $query->where($tabla_venta_auto . '.' . VentasAutosModel::ID, $id_venta);
        }

        return $query->first();
    }
}
