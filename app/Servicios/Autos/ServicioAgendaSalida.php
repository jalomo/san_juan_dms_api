<?php

namespace App\Servicios\Autos;

use App\Models\Autos\AgendaSalidaModel;
use App\Models\Autos\VentasAutosModel;
use App\Servicios\Core\ServicioDB;


class ServicioAgendaSalida extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'Agenda';
        $this->modelo = new AgendaSalidaModel();
    }

    public function getReglasGuardar()
    {
        return [
            AgendaSalidaModel::TITULO => 'required',
            AgendaSalidaModel::DATETIME_INICIO => 'required',
            // AgendaSalidaModel::ID_VENTA_AUTO => 'required|unique:' . AgendaSalidaModel::getTableName() . ',' . AgendaSalidaModel::getTableName() . '.' . VentasAutosModel::ID_VENTA_AUTO . '|exists:' . VentasAutosModel::getTableName() . ',id',
            AgendaSalidaModel::ID_VENTA_AUTO => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            AgendaSalidaModel::TITULO => 'required',
            AgendaSalidaModel::DATETIME_INICIO => 'required',
            AgendaSalidaModel::ID_VENTA_AUTO => 'required|exists:' . VentasAutosModel::getTableName() . ',id',
        ];
    }

    public function getEventoPorHora($parametros)
    {
        $fecha_agenda =  $this->modelo
            ->where(
                AgendaSalidaModel::DATETIME_INICIO,
                '=',
                $parametros[AgendaSalidaModel::DATETIME_INICIO]
            )
            ->first();

        if (!empty($fecha_agenda)) {
            return $fecha_agenda->delete();
        }
    }
}
