<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\CatStatusRecepcionUNModel;
use App\Servicios\Core\ServicioDB;

class ServicioCatStatusRecepcionUN extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo estatus recepcion unidades nuevas';
        $this->modelo = new CatStatusRecepcionUNModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatStatusRecepcionUNModel::ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatStatusRecepcionUNModel::ESTATUS => 'required'
        ];
    }
}
