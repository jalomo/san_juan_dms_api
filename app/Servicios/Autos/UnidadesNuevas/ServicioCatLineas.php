<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\CatLineas;
use App\Servicios\Core\ServicioDB;

class ServicioCatLineas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo líneas';
        $this->modelo = new CatLineas();
    }

    public function getReglasGuardar()
    {
        return [
            CatLineas::MODELO => 'required',
            CatLineas::DESCRIPCION => 'required',
            CatLineas::LINEA => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatLineas::MODELO => 'required',
            CatLineas::DESCRIPCION => 'required',
            CatLineas::LINEA => 'required',
        ];
    }
}
