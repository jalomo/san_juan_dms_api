<?php

namespace App\Servicios\Autos\UnidadesNuevas;
use App\Models\Autos\UnidadesNuevas\CatEstatusUNModel;
use App\Servicios\Core\ServicioDB;

class ServicioEstatusUN extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'estatus unidades nuevas';
        $this->modelo = new CatEstatusUNModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatEstatusUNModel::ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatEstatusUNModel::ESTATUS => 'required'
        ];
    }
}


