<?php

namespace App\Servicios\Autos\UnidadesNuevas;

use App\Models\Autos\UnidadesNuevas\PreciosUnidadesNuevasModel;
use App\Servicios\Core\ServicioDB;

class ServicioPreciosActivos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo líneas';
        $this->modelo = new PreciosUnidadesNuevasModel();
    }

    public function getReglasGuardar()
    {
        return [
            PreciosUnidadesNuevasModel::MODELO => 'required',
            PreciosUnidadesNuevasModel::DESCRIPCION => 'required',
            PreciosUnidadesNuevasModel::FECHA_INICIO => 'required',
            PreciosUnidadesNuevasModel::FECHA_FIN => 'required',
            PreciosUnidadesNuevasModel::ACTIVO => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            PreciosUnidadesNuevasModel::MODELO => 'required',
            PreciosUnidadesNuevasModel::DESCRIPCION => 'required',
            PreciosUnidadesNuevasModel::FECHA_INICIO => 'required',
            PreciosUnidadesNuevasModel::FECHA_FIN => 'required',
            PreciosUnidadesNuevasModel::ACTIVO => 'required',
        ];
    }
}
