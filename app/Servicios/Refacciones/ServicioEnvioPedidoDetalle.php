<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\EnvioPedidoDetalleModel;
use App\Models\Refacciones\MaProductoPedidoModel;
use App\Servicios\Core\ServicioDB;

class ServicioEnvioPedidoDetalle extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'envio pedido';
        $this->modelo = new EnvioPedidoDetalleModel();
    }

    public function getReglasGuardar()
    {
        return [
            EnvioPedidoDetalleModel::VIA_ENVIO => 'required',
            EnvioPedidoDetalleModel::DIA_ENVIO => 'required',
            EnvioPedidoDetalleModel::RUTA => 'required',
            EnvioPedidoDetalleModel::LINEAS => 'required',
            EnvioPedidoDetalleModel::TOTAL_PIEZAS => 'required',
            EnvioPedidoDetalleModel::PESO_TOTAL => 'required',
            EnvioPedidoDetalleModel::NUMERO_REMISION => 'required',
            EnvioPedidoDetalleModel::PEDIDO_ID => 'required|exists:' . MaProductoPedidoModel::getTableName() . ',id|unique:' . $this->modelo->getTable() . ',pedido_id'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            EnvioPedidoDetalleModel::VIA_ENVIO => 'nullable',
            EnvioPedidoDetalleModel::DIA_ENVIO => 'nullable',
            EnvioPedidoDetalleModel::RUTA => 'nullable',
            EnvioPedidoDetalleModel::LINEAS => 'nullable',
            EnvioPedidoDetalleModel::TOTAL_PIEZAS => 'nullable',
            EnvioPedidoDetalleModel::PESO_TOTAL => 'nullable',
            EnvioPedidoDetalleModel::NUMERO_REMISION => 'nullable',
            EnvioPedidoDetalleModel::PEDIDO_ID => 'required|exists:' . MaProductoPedidoModel::getTableName() . ',id'
        ];
    }
}
