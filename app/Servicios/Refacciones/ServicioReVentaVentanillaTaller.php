<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ReVentasVentanillaTallerModel;

class ServicioReVentaVentanillaTaller extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventanilla taller';
        $this->modelo = new ReVentasVentanillaTallerModel();
    }

    public function getReglasGuardar()
    {
        return [
            ReVentasVentanillaTallerModel::VENTA_ID => 'required|numeric|exists:ventas,id',
            ReVentasVentanillaTallerModel::NUMERO_ORDEN => 'required|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ReVentasVentanillaTallerModel::VENTA_ID => 'required|numeric|exists:ventas,id',
            ReVentasVentanillaTallerModel::NUMERO_ORDEN => 'required|numeric'
        ];
    }
}
