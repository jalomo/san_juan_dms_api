<?php

namespace App\Servicios\Refacciones;


use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\RemplazoProductoAlmacenModel;
use Symfony\Component\HttpFoundation\Request;

class ServicioRemplazoProducto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'remplazo_producto';
        $this->modelo = new RemplazoProductoAlmacenModel();
    }

    public function getReglasGuardar()
    {
        return [
            RemplazoProductoAlmacenModel::PRODUCTO_ID => 'required',
            RemplazoProductoAlmacenModel::NOMBRE_PRODUCTO_ANTERIOR => 'required',
            RemplazoProductoAlmacenModel::NOMBRE_PRODUCTO_ACTUAL => 'required',
            RemplazoProductoAlmacenModel::NO_IDENTIFICACION => 'required',
            RemplazoProductoAlmacenModel::NO_IDENTIFICACION_ANTERIOR => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            RemplazoProductoAlmacenModel::PRODUCTO_ID => 'required',
            RemplazoProductoAlmacenModel::NOMBRE_PRODUCTO_ANTERIOR => 'required',
            RemplazoProductoAlmacenModel::NOMBRE_PRODUCTO_ACTUAL => 'required',
            RemplazoProductoAlmacenModel::NO_IDENTIFICACION => 'required',
            RemplazoProductoAlmacenModel::NO_IDENTIFICACION_ANTERIOR => 'required',
        ];
    }

    public function actualizarProductoAlmacen(Request $request, $id)
    {
        return $this->update($request, $id);
    }

    public function getbyidproducto($id)
    {
        return $this->modelo->where(RemplazoProductoAlmacenModel::PRODUCTO_ID, $id)->get();
    }
}
