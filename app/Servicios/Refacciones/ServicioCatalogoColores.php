<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\CatalogoColoresModel;
use App\Servicios\Core\ServicioDB;

class ServicioCatalogoColores extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'colores';
        $this->modelo = new CatalogoColoresModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoColoresModel::NOMBRE => 'required',
            CatalogoColoresModel::CLAVE => 'required'
        ];
    }
    
    public function getReglasUpdate()
    {
        return [
            CatalogoColoresModel::NOMBRE => 'required',
            CatalogoColoresModel::CLAVE => 'required'
        ];
    }

    public function getReglasBusquedaColor()
    {
        return [
            CatalogoColoresModel::NOMBRE => 'required'
        ];
    }

    public function searchIdColor($request)
    {
        $data = $this->modelo->where(CatalogoColoresModel::NOMBRE,$request->get(CatalogoColoresModel::NOMBRE))
            ->select(CatalogoColoresModel::ID)->limit(1)->get();
        
        return $data;
    }
}
