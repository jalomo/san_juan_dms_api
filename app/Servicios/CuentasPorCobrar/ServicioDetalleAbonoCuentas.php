<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\DetalleAsientoCuentasModel;
use App\Models\CuentasPorCobrar\TipoAsientoModel;
use App\Models\CuentasPorCobrar\EstatusAsientosModel;
use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Servicios\Refacciones\ServicioCurl;

class ServicioDetalleAbonoCuentas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'Detalle Abonos Cuentas';
        $this->modelo = new DetalleAsientoCuentasModel();
        $this->servicioCurl = new ServicioCurl();
    }

    public function getReglasGuardar()
    {
        return [
            DetalleAsientoCuentasModel::CUENTA_POR_COBRAR_ID => 'required|numeric|exists:cuentas_por_cobrar,id',
            DetalleAsientoCuentasModel::ABONO_ID => 'required|numeric|exists:abonos_por_cobrar,id',
            DetalleAsientoCuentasModel::TIPO_ASIENTO_ID => 'required|numeric|exists:tipo_asiento,id',
            DetalleAsientoCuentasModel::CUENTA_ID => 'required|numeric|exists:catalogo_cuentas,id',
            DetalleAsientoCuentasModel::FECHA_PAGO => 'required|date',
            DetalleAsientoCuentasModel::TOTAL_PAGO => 'required|numeric',
            DetalleAsientoCuentasModel::POLIZA_ID => 'nullable',
            DetalleAsientoCuentasModel::REFERENCIA => 'nullable',
            DetalleAsientoCuentasModel::ESTATUS_ID => 'nullable|numeric|exists:estatus_asientos,id'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            DetalleAsientoCuentasModel::CUENTA_POR_COBRAR_ID => 'required|numeric|exists:cuentas_por_cobrar,id',
            DetalleAsientoCuentasModel::ABONO_ID => 'required|numeric|exists:abonos_por_cobrar,id',
            DetalleAsientoCuentasModel::TIPO_ASIENTO_ID => 'required|numeric|exists:tipo_asiento,id',
            DetalleAsientoCuentasModel::CUENTA_ID => 'required|numeric|exists:catalogo_cuentas,id',
            DetalleAsientoCuentasModel::FECHA_PAGO => 'required|date',
            DetalleAsientoCuentasModel::TOTAL_PAGO => 'required|numeric',
            DetalleAsientoCuentasModel::POLIZA_ID => 'required',
            DetalleAsientoCuentasModel::REFERENCIA => 'required',
            DetalleAsientoCuentasModel::ESTATUS_ID => 'nullable|numeric|exists:estatus_asientos,id'
        ];
    }

    public function getReglasEstatus()
    {
        return [
            DetalleAsientoCuentasModel::ESTATUS_ID => 'required|exists:estatus_asientos,id',
        ];
    }

    public function getReglasFiltrosFechas()
    {
        return [
            'fecha_inicio' => 'required|date',
            'fecha_fin' => 'required|date',
            'estatus_id' => 'nullable|numeric|exists:estatus_asientos,id'
        ];
    }

    public function showByCuentaPorCobrar($cuenta_por_cobrar_id)
    {
        return $this->modelo
            ->leftJoin(TipoAsientoModel::getTableName(), TipoAsientoModel::getTableName() . '.' . TipoAsientoModel::ID, '=', DetalleAsientoCuentasModel::getTableName() . '.' . DetalleAsientoCuentasModel::TIPO_ASIENTO_ID)
            ->leftJoin(CatalogoCuentasModel::getTableName(), CatalogoCuentasModel::getTableName() . '.' . CatalogoCuentasModel::ID, '=', DetalleAsientoCuentasModel::getTableName() . '.' . DetalleAsientoCuentasModel::CUENTA_ID)
            ->where(DetalleAsientoCuentasModel::getTableName() . '.' . DetalleAsientoCuentasModel::CUENTA_POR_COBRAR_ID, $cuenta_por_cobrar_id)
            ->select(
                DetalleAsientoCuentasModel::getTableName() . '.' . '*',
                CatalogoCuentasModel::getTableName() . '.' . CatalogoCuentasModel::NO_CUENTA,
                CatalogoCuentasModel::getTableName() . '.' . CatalogoCuentasModel::NOMBRE_CUENTA,
                TipoAsientoModel::getTableName() . '.' . TipoAsientoModel::DESCRIPCION . ' as tipo_asiento'
            )
            ->get();
    }

    public function getFiltroFechas($request)
    {
        $fecha_inicio = $request->get('fecha_inicio');
        $fecha_fin = $request->get('fecha_fin');

        $query = $this->modelo
            ->leftJoin(TipoAsientoModel::getTableName(), TipoAsientoModel::getTableName() . '.' . TipoAsientoModel::ID, '=', DetalleAsientoCuentasModel::getTableName() . '.' . DetalleAsientoCuentasModel::TIPO_ASIENTO_ID)
            ->leftJoin(CatalogoCuentasModel::getTableName(), CatalogoCuentasModel::getTableName() . '.' . CatalogoCuentasModel::ID, '=', DetalleAsientoCuentasModel::getTableName() . '.' . DetalleAsientoCuentasModel::CUENTA_ID)
            ->leftJoin(EstatusAsientosModel::getTableName(), EstatusAsientosModel::getTableName() . '.' . EstatusAsientosModel::ID, '=', DetalleAsientoCuentasModel::getTableName() . '.' . DetalleAsientoCuentasModel::ESTATUS_ID)
            ->select(
                DetalleAsientoCuentasModel::getTableName() . '.' . '*',
                CatalogoCuentasModel::getTableName() . '.' . CatalogoCuentasModel::NO_CUENTA,
                CatalogoCuentasModel::getTableName() . '.' . CatalogoCuentasModel::NOMBRE_CUENTA,
                TipoAsientoModel::getTableName() . '.' . TipoAsientoModel::DESCRIPCION . ' as tipo_asiento',
                EstatusAsientosModel::getTableName() . '.' . EstatusAsientosModel::NOMBRE . ' as estatus'
            );
        if ($fecha_inicio && $fecha_fin) {
            $query->whereBetween(DetalleAsientoCuentasModel::getTableName() . '.' . DetalleAsientoCuentasModel::FECHA_PAGO, [$fecha_inicio . ' 00:00:00', $fecha_fin . ' 23:59:59']);
        }
        if ($request->get(DetalleAsientoCuentasModel::ESTATUS_ID)) {
            $query->where(DetalleAsientoCuentasModel::ESTATUS_ID, $request->get(DetalleAsientoCuentasModel::ESTATUS_ID));
        }
        return $query->get();
    }

    public function changeEstatus($request, $id)
    {
        $modelo = $this->modelo->find($id);
        $modelo->estatus_id = $request->get(DetalleAsientoCuentasModel::ESTATUS_ID);

        if ($request->get(DetalleAsientoCuentasModel::ESTATUS_ID) == EstatusAsientosModel::ESTATUS_ENVIADO) {
            $poliza = $this->set_poliza();
            if ($poliza && isset($poliza->id_id)) {
                $modelo->poliza_id = $poliza->id_id;
                $this->curl_asiento_api($modelo);
                $update = $modelo->save();
            }
        } else {
            $update = $modelo->save();
        }

        return $update;
    }


    public function set_poliza()
    {
        $request = $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'crear_poliza', [
            'fecha' => date('Y-m-d'),
            'poliza' => 'San_Juan_' . date('Y-m-d'),
            'concepto' => 'DmsSanJuan',
            'tipo' => 1,
            'id_departamento' => 1,
        ], false);
        return json_decode($request);
    }

    public function curl_asiento_api($data)
    {
        return $this->servicioCurl->curlPost(env('URL_CONTABILIDAD') . 'crear_asiento', [
            'id_id_poliza' => $data['poliza_id'],
            'nombre' => 'San Juan DMS: ' . date('Y-m-d'),
            'referencia' => isset($data['referencia']) ? $data['referencia'] : '',
            'concepto' => isset($data['referencia']) ? $data['referencia'] : '',
            'total' => isset($data['total_pago']) ? $data['total_pago'] : '',
            'cuenta' => isset($data['cuenta_id']) ? $data['cuenta_id'] : '',
            'tipo' => isset($data['tipo_asiento_id']) ? $data['tipo_asiento_id'] : '',
            'fecha' => isset($data['fecha_pago']) ? $data['fecha_pago'] : ''
        ], false);
    }
}
