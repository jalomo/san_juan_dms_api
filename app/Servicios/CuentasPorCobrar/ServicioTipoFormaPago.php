<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;

class ServicioTipoFormaPago extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'tipo forma pago';
        $this->modelo = new TipoFormaPagoModel();
    }

    public function getReglasGuardar()
    {
        return [
            TipoFormaPagoModel::DESCRIPCION => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',descripcion',

        ];
    }
    public function getReglasUpdate()
    {
        return [
            TipoFormaPagoModel::DESCRIPCION => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',descripcion',
        ];
    }
}
