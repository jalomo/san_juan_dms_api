<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Servicios\Refacciones\ServicioFolios;
use App\Models\Contabilidad\CatalogoProcesosModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use Illuminate\Support\Facades\DB;


class ServicioCuentaPorCobrar extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cuentas por cobrar';
        $this->modelo = new CuentasPorCobrarModel();
        $this->servicioFolio = new ServicioFolios();
        $this->servicio_abonos = new ServicioAbono();
        $this->modelo_plazo_credito = new PlazoCreditoModel();
    }

    public function getReglasGuardar()
    {
        return [
            CuentasPorCobrarModel::FOLIO_ID => 'nullable|exists:folios,id',
            CuentasPorCobrarModel::CLIENTE_ID => 'required|exists:clientes,id',
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => 'required|exists:estatus_cuenta,id',
            CuentasPorCobrarModel::CONCEPTO => 'required|string',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric|exists:tipo_forma_pago,id',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'required|numeric|exists:cat_tipo_pago,id',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric|exists:plazos_credito,id',
            CuentasPorCobrarModel::TOTAL => 'nullable|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'nullable|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorCobrarModel::COMENTARIOS => 'nullable|string',
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => 'nullable|numeric|exists:usuarios,id',
            CuentasPorCobrarModel::FECHA => 'required|date',
            VentasRealizadasModel::VENTA_TOTAL  => 'required',
            CuentasPorCobrarModel::TIPO_PROCESO => 'nullable|numeric|exists:catalogo_cuentas,id',
        ];
    }

    public function getReglasUpdate()
    {
        return [
            CuentasPorCobrarModel::FOLIO_ID => 'nullable|exists:folios,id',
            CuentasPorCobrarModel::CLIENTE_ID => 'nullable|exists:clientes,id',
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => 'required|exists:estatus_cuenta,id',
            CuentasPorCobrarModel::CONCEPTO => 'nullable|string',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'nullable|numeric|exists:tipo_forma_pago,id',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric|exists:cat_tipo_pago,id',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'nullable|numeric|exists:plazos_credito,id',
            CuentasPorCobrarModel::TOTAL => 'nullable|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'nullable|numeric',
            CuentasPorCobrarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorCobrarModel::COMENTARIOS => 'nullable|string',
            CuentasPorCobrarModel::USUARIO_GESTOR_ID => 'nullable|numeric|exists:usuarios,id',
            CuentasPorCobrarModel::FECHA => 'nullable|date',
            VentasRealizadasModel::VENTA_TOTAL  => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_PROCESO => 'nullable|numeric|exists:catalogo_cuentas,id',
        ];
    }


    public function getAll($params)
    {
        $consulta =  DB::table('cuentas_por_cobrar as cxc')
            ->join('folios', 'cxc.folio_id', '=', 'folios.id')
            ->join('clientes', 'cxc.cliente_id', '=', 'clientes.id')
            ->join('plazos_credito', 'cxc.plazo_credito_id', '=', 'plazos_credito.id')
            ->join('tipo_forma_pago', 'cxc.tipo_forma_pago_id', '=', 'tipo_forma_pago.id')
            ->join('estatus_cuenta', 'cxc.estatus_cuenta_id', '=', 'estatus_cuenta.id')
            ->leftJoin('abonos_por_cobrar', 'cxc.id', '=', 'abonos_por_cobrar.cuenta_por_cobrar_id')
            ->leftJoin('cat_tipo_abono', 'abonos_por_cobrar.tipo_abono_id', '=', 'cat_tipo_abono.id')
            ->select(
                'cxc.id',
                'cxc.concepto',
                'cxc.importe',
                'cxc.total',
                'cxc.fecha',
                'cxc.tipo_forma_pago_id',
                'folios.folio',
                'folios.tipo_proceso_id',
                'folios.id as folio_id',
                'clientes.numero_cliente as numero_cliente',
                'clientes.nombre as nombre_cliente',
                'plazos_credito.nombre as plazo_credito',
                'plazos_credito.cantidad_mes',
                'tipo_forma_pago.descripcion as tipo_forma_pago',
                'estatus_cuenta.nombre as estatus_cuenta',
                'estatus_cuenta.id as estatus_cuenta_id',
                DB::raw('sum(abonos_por_cobrar.total_pago) as saldo_acomulado')
            );
        if (isset($params['cliente_id']) && $params['cliente_id']) {
            $consulta->where('cxc.cliente_id', $params['cliente_id']);
        }
        if (isset($params['folio_id']) && $params['folio_id']) {
            $folio = $params['folio_id'];
            $consulta->whereRaw('LOWER(folios.folio) LIKE (?) ', ["%{$folio}%"]);
        }
        if (isset($params['tipo_forma_pago_id']) && $params['tipo_forma_pago_id']) {
            $consulta->where('cxc.tipo_forma_pago_id', $params['tipo_forma_pago_id']);
        }
        if (isset($params['estatus_cuenta_id']) && $params['estatus_cuenta_id']) {
            $consulta->where('cxc.estatus_cuenta_id', $params['estatus_cuenta_id']);
        }
        $consulta->groupBy(
            'cxc.id',
            'cxc.concepto',
            'cxc.importe',
            'cxc.total',
            'cxc.fecha',
            'cxc.tipo_forma_pago_id',
            'folios.folio',
            'folios.tipo_proceso_id',
            'folios.id',
            'numero_cliente',
            'nombre_cliente',
            'plazo_credito',
            'plazos_credito.cantidad_mes',
            'tipo_forma_pago',
            'estatus_cuenta.nombre',
            'estatus_cuenta.id'
        );
        return $consulta->get();
    }

    public function getByIdCuenta($id)
    {
        return DB::table('cuentas_por_cobrar as cxc')
            ->join('folios', 'cxc.folio_id', '=', 'folios.id')
            ->join('clientes', 'cxc.cliente_id', '=', 'clientes.id')
            ->join('plazos_credito', 'cxc.plazo_credito_id', '=', 'plazos_credito.id')
            ->join('tipo_forma_pago', 'cxc.tipo_forma_pago_id', '=', 'tipo_forma_pago.id')
            ->join('estatus_cuenta', 'cxc.estatus_cuenta_id', '=', 'estatus_cuenta.id')
            ->select(
                'cxc.id',
                'cxc.concepto',
                'cxc.importe',
                'cxc.intereses',
                'cxc.total',
                'cxc.comentarios',
                'cxc.enganche',
                'cxc.tipo_forma_pago_id',
                'cxc.tipo_pago_id',
                'cxc.tasa_interes',
                'cxc.estatus_cuenta_id',
                'cxc.fecha',
                'cxc.created_at',
                'folios.folio',
                'folios.id as folio_id',
                'folios.tipo_proceso_id',
                'clientes.nombre as nombre_cliente',
                'clientes.id AS cliente_id',
                'plazos_credito.nombre as plazo_credito',
                'plazos_credito.cantidad_mes',
                'tipo_forma_pago.descripcion as tipo_forma_pago',
                'estatus_cuenta.nombre as estatus_cuenta'
            )->where('cxc.id', $id)->first();
    }

    public function getByFolioId($folio_id)
    {
        return $this->modelo->where(CuentasPorCobrarModel::FOLIO_ID, $folio_id)->first();
    }

    public function getKardexPagos($params)
    {
        $consulta =  DB::table('cuentas_por_cobrar as cxc')
            ->join('folios', 'cxc.folio_id', '=', 'folios.id')
            ->join('clientes', 'cxc.cliente_id', '=', 'clientes.id')
            ->join('plazos_credito', 'cxc.plazo_credito_id', '=', 'plazos_credito.id')
            ->join('tipo_forma_pago', 'cxc.tipo_forma_pago_id', '=', 'tipo_forma_pago.id')
            ->join('estatus_cuenta', 'cxc.estatus_cuenta_id', '=', 'estatus_cuenta.id')
            ->join('abonos_por_cobrar', 'cxc.id', '=', 'abonos_por_cobrar.cuenta_por_cobrar_id')
            ->join('cat_estatus_abono', 'abonos_por_cobrar.estatus_abono_id', '=', 'cat_estatus_abono.id')
            ->join('cat_tipo_abono', 'abonos_por_cobrar.tipo_abono_id', '=', 'cat_tipo_abono.id')
            ->leftJoin('usuarios', 'cxc.usuario_gestor_id', '=', 'usuarios.id')
            ->select(
                'cxc.id',
                'cxc.concepto',
                'cxc.total',
                'cxc.tipo_forma_pago_id',
                'folios.folio',
                'clientes.numero_cliente as numero_cliente',
                'clientes.nombre as nombre_cliente',
                'plazos_credito.nombre as plazo_credito',
                'plazos_credito.cantidad_mes',
                'tipo_forma_pago.descripcion as tipo_forma_pago',
                'abonos_por_cobrar.total_abono',
                'abonos_por_cobrar.fecha_vencimiento',
                'abonos_por_cobrar.fecha_pago',
                'abonos_por_cobrar.total_pago',
                'abonos_por_cobrar.dias_moratorios',
                'cat_tipo_abono.nombre as tipo_pago',
                'cat_estatus_abono.nombre as estatus_abono',
                'cat_estatus_abono.id as estatus_abono_id',
                'usuarios.nombre as gestor_nombre',
                'usuarios.apellido_paterno as gestor_apellido_paterno'
            );
        if (isset($params['cliente_id']) && $params['cliente_id']) {
            $consulta->where('cxc.cliente_id', $params['cliente_id']);
        }
        if (isset($params['folio']) && $params['folio']) {
            $folio = $params['folio'];

            $consulta->where('folios.folio', 'iLIKE', '%' . $folio . '%');
        }
        if (isset($params['tipo_forma_pago_id']) && $params['tipo_forma_pago_id']) {
            $consulta->where('cxc.tipo_forma_pago_id', $params['tipo_forma_pago_id']);
        }
        if (isset($params['estatus_abono_id']) && $params['estatus_abono_id']) {
            $consulta->where('abonos_por_cobrar.estatus_abono_id', $params['estatus_abono_id']);
        }
        if (isset($params['fecha_inicio']) && !isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_vencimiento', '>=', $params['fecha_inicio']);
        }
        if (!isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_vencimiento', '<=', $params['fecha_fin']);
        }
        if (isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_vencimiento', '>=', $params['fecha_inicio']);
            $consulta->where('abonos_por_cobrar.fecha_vencimiento', '<=', $params['fecha_fin']);
        }
        if (isset($params['usuario_gestor_id']) && $params['usuario_gestor_id']) {
            $consulta->where('cxc.usuario_gestor_id', $params['usuario_gestor_id']);
        }
        $consulta->whereIn('cxc.estatus_cuenta_id', [1, 3]);
        $consulta->where('abonos_por_cobrar.tipo_abono_id', 2);

        $consulta->orderBy('abonos_por_cobrar.id', 'asc');

        return $consulta->get();
        //dd( DB::getQueryLog());
    }

    public function getCierreCaja($params)
    {
        $consulta =  DB::table('cuentas_por_cobrar as cxc')
            ->leftJoin('abonos_por_cobrar', 'cxc.id', '=', 'abonos_por_cobrar.cuenta_por_cobrar_id')
            ->leftJoin('cat_tipo_pago', 'abonos_por_cobrar.tipo_pago_id', '=', 'cat_tipo_pago.id')
            ->leftJoin('catalogo_caja', 'abonos_por_cobrar.caja_id', '=', 'catalogo_caja.id')
            ->select(
                DB::raw('sum(abonos_por_cobrar.total_pago) as total'),
                'cat_tipo_pago.id',
                'cat_tipo_pago.nombre as tipo_pago'
            );

        if (isset($params['estatus_abono_id']) && $params['estatus_abono_id']) {
            $consulta->where('abonos_por_cobrar.estatus_abono_id', $params['estatus_abono_id']);
        }
        if (isset($params['fecha_inicio']) && !isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '=', $params['fecha_inicio']);
        }
        if (!isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '=', $params['fecha_fin']);
        }
        if (isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '>=', $params['fecha_inicio']);
            $consulta->where('abonos_por_cobrar.fecha_pago', '<=', $params['fecha_fin']);
        }
        if (isset($params['caja_id']) && $params['caja_id']) {
            $consulta->where('abonos_por_cobrar.caja_id', $params['caja_id']);
        }

        $consulta->whereIn('cxc.estatus_cuenta_id', [1, 2, 3]);
        $consulta->groupBy(
            'cat_tipo_pago.id',
            'tipo_pago'
        );
        return $consulta->get();
    }

    public function getDetalleCaja($params)
    {
        $consulta =  DB::table('cuentas_por_cobrar as cxc')
            ->join('abonos_por_cobrar', 'cxc.id', '=', 'abonos_por_cobrar.cuenta_por_cobrar_id')
            ->join('cat_tipo_pago', 'abonos_por_cobrar.tipo_pago_id', '=', 'cat_tipo_pago.id')
            ->select(
                DB::raw('sum(abonos_por_cobrar.total_pago) as total'),
                'cat_tipo_pago.nombre as tipo_pago'
            );

        if (isset($params['estatus_abono_id']) && $params['estatus_abono_id']) {
            $consulta->where('abonos_por_cobrar.estatus_abono_id', $params['estatus_abono_id']);
        }
        if (isset($params['fecha_inicio']) && !isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '=', $params['fecha_inicio']);
        }
        if (!isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '=', $params['fecha_fin']);
        }
        if (isset($params['fecha_inicio']) && isset($params['fecha_fin'])) {
            $consulta->where('abonos_por_cobrar.fecha_pago', '>=', $params['fecha_inicio']);
            $consulta->where('abonos_por_cobrar.fecha_pago', '<=', $params['fecha_fin']);
        }

        $consulta->whereIn('cxc.estatus_cuenta_id', [1, 2, 3]);
        $consulta->groupBy(
            'tipo_pago'
        );
        return $consulta->get();
    }


    public function changeEstatusByFolio($estatus_cuenta_id, $folio_id)
    {
        $modelo = $this->modelo->where(CuentasPorCobrarModel::FOLIO_ID, $folio_id)->first();
        $modelo->estatus_cuenta_id = $estatus_cuenta_id;
        return $modelo->save();
    }

    public function getCuentasEnProcesoPago()
    {
        return $this->modelo->whereIn(CuentasPorCobrarModel::ESTATUS_CUENTA_ID, [EstatusCuentaModel::ESTATUS_ATRASADO, EstatusCuentaModel::ESTATUS_PROCESO])->get();
    }

    public function procesarCuentasPorCobrar($parametros)
    {
        $importe = $parametros[VentasRealizadasModel::VENTA_TOTAL];
        if ($parametros['tipo_forma_pago_id'] == 2) {
            // $tasa_interes = $parametros[CuentasPorCobrarModel::TASA_INTERES];
            // $intereses = $tasa_interes * $importe / 100;
            // $total = $importe + $intereses;
            $tasa_interes = $parametros[CuentasPorCobrarModel::TASA_INTERES];
            $plazo_credito = $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID];
            $enganche = $parametros[CuentasPorCobrarModel::ENGANCHE];
            $totalsinenganche = $importe - $enganche;
            $cantidad_abonos = $this->modelo_plazo_credito->find($plazo_credito)->cantidad_mes;
            $abono_mensual = ($totalsinenganche / $cantidad_abonos);
            $interes_mensual = ($abono_mensual * $tasa_interes) / 100;
            $intereses = $interes_mensual * $cantidad_abonos;
            $total = $importe + $intereses;
        } else {
            $total = $parametros[VentasRealizadasModel::VENTA_TOTAL];
            $intereses = 0;
        }
        // validar si existe el folio antes de crearlo, checar esta funcionalidad para meterlo en el controlador donde se hacer la orden de servicio

        if (!isset($parametros['folio_id'])) {
            $data_folio = $this->servicioFolio->generarFolio(CatalogoProcesosModel::CAT_MANO_DE_OBRA);
            $folio = $data_folio ? $data_folio->id : '';
        } else {
            $folio = $parametros['folio_id'];
        }

        $params = [
            CuentasPorCobrarModel::FOLIO_ID => $folio,
            CuentasPorCobrarModel::CLIENTE_ID => $parametros[CuentasPorCobrarModel::CLIENTE_ID],
            CuentasPorCobrarModel::CONCEPTO => $parametros[CuentasPorCobrarModel::CONCEPTO],
            CuentasPorCobrarModel::IMPORTE => $importe,
            CuentasPorCobrarModel::TOTAL => $total,
            CuentasPorCobrarModel::FECHA => isset($parametros[CuentasPorCobrarModel::FECHA]) ?  $parametros[CuentasPorCobrarModel::FECHA] : date('Y-m-d'),
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID],
            CuentasPorCobrarModel::TIPO_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_PAGO_ID],
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID],
            CuentasPorCobrarModel::ENGANCHE => isset($parametros[CuentasPorCobrarModel::ENGANCHE]) ? $parametros[CuentasPorCobrarModel::ENGANCHE] : 0,
            CuentasPorCobrarModel::TASA_INTERES => isset($parametros[CuentasPorCobrarModel::TASA_INTERES]) ? $parametros[CuentasPorCobrarModel::TASA_INTERES] : 0,
            CuentasPorCobrarModel::INTERESES => $intereses,
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID =>  isset($parametros[CuentasPorCobrarModel::ESTATUS_CUENTA_ID]) ? $parametros[CuentasPorCobrarModel::ESTATUS_CUENTA_ID] : EstatusCuentaModel::ESTATUS_POR_EMPEZAR, 
        ];
        if ($parametros['tipo_forma_pago_id'] == 2) {
            $data = $this->massUpdateWhereId(CuentasPorCobrarModel::FOLIO_ID, $folio, $params);
        } else {
            $data = $this->crear($params);
        }
        $abonos = $this->servicio_abonos->crearAbonosByCuentaId($data->id);

        if (!$abonos) {
            return false;
        }
        return $data;
    }
}
