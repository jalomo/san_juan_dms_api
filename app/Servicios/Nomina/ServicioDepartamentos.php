<?php

namespace App\Servicios\Nomina;

use App\Servicios\Core\ServicioDB;
use App\Models\Nomina\CaDepartamentoModel;

class ServicioDepartamentos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'departamentos';
        $this->modelo = new CaDepartamentoModel();
    }

    public function getReglasGuardar()
    {
        return [
            CaDepartamentoModel::Descripcion => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CaDepartamentoModel::Descripcion => 'required',
        ];
    }

    public function getClave()
    {
        $clave =  $this->modelo->max(CaDepartamentoModel::Clave);
        return $clave != null && $clave == 0 ? 1 : $clave + 1;
    }
}
