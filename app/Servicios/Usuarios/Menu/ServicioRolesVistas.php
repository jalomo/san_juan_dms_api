<?php

namespace App\Servicios\Usuarios\Menu;

use App\Models\Usuarios\Menu\ModulosModel;
use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\RolesVistasModel;
use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuVistasModel;
use App\Models\Usuarios\RolModel;
use App\Models\Usuarios\User;
use Illuminate\Support\Facades\DB;

class ServicioRolesVistas extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new RolesVistasModel();
        $this->userModel = new User();
        $this->rolModel = new RolModel();
        $this->RolesVistasModel = new RolesVistasModel();
        $this->modulosModel = new ModulosModel();
        $this->menuSeccionesModel = new MenuSeccionesModel();
        $this->menuSubmenuModel = new MenuSubmenuModel();
        $this->menuVistasModel = new MenuVistasModel();

        $this->recurso = 'roles_vistas';
    }

    public function getReglasGuardar()
    {
        return [
            RolesVistasModel::ROL_ID => 'required|exists:roles,id',
            RolesVistasModel::VISTA_ID => 'required|exists:menu_vistas,id'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            RolesVistasModel::ROL_ID => 'required|exists:roles,id',
            RolesVistasModel::VISTA_ID => 'required|exists:menu_vistas,id'
        ];
    }

    public function getReglasMenuVistaRoles()
    {
        return [
            RolesVistasModel::ROL_ID => 'required|exists:roles,id',
            RolesVistasModel::VISTA_ID => 'nullable|array'
        ];
    }

    public function getReglasVistaByRol()
    {
        return [
            RolesVistasModel::ROL_ID => 'required|exists:roles,id'
        ];
    }

    public function getVistasRoles($parametros)
    {
        $tabla_user = $this->userModel->getTable();
        $tabla_rol = $this->rolModel->getTable();
        $tabla_menuRolesVistas = $this->RolesVistasModel->getTable();
        $tabla_modulos = $this->modulosModel->getTable();
        $tabla_secciones = $this->menuSeccionesModel->getTable();
        $tabla_submenu = $this->menuSubmenuModel->getTable();
        $tabla_menuVistas = $this->menuVistasModel->getTable();

        $query = $this->modelo->select(
            $tabla_modulos . '.' . ModulosModel::NOMBRE . ' as nombre_modulo',
            $tabla_secciones . '.' . MenuSeccionesModel::NOMBRE . ' as nombre_seccion',
            $tabla_submenu . '.' . MenuSubmenuModel::NOMBRE,
            $tabla_menuVistas . '.' . MenuVistasModel::NOMBRE . ' as nombre_vista',
            $tabla_menuVistas . '.' . MenuVistasModel::ES_EXTERNA,
            $tabla_menuVistas . '.' . MenuVistasModel::LINK,
            $tabla_menuVistas . '.' . MenuVistasModel::CONTROLADOR,
            $tabla_menuVistas . '.' . MenuVistasModel::MODULO
        )->from($tabla_user);
        $query->join($tabla_rol, $tabla_rol . '.' . RolModel::ID, '=', $tabla_user . '.' . User::ROL_ID);
        $query->join($tabla_menuRolesVistas, $tabla_menuRolesVistas . '.' . RolesVistasModel::ROL_ID, '=', $tabla_rol . '.' . RolModel::ID);
        $query->join($tabla_menuVistas, $tabla_menuVistas . '.' . MenuVistasModel::ID, '=', $tabla_menuRolesVistas . '.' . RolesVistasModel::VISTA_ID);
        $query->where($tabla_user . '.' . User::ID, $parametros['id']);
        return $query->get();
    }

    //TODO: revisar consultas
    //TODO: duplicar con usuarios
    public function vistasByRoles($parametros)
    {
        $query = $this->menuVistasModel->select(
            'menu_vistas.nombre',
            'roles.rol',
            'roles.id as rol_id',
            'menu_vistas.id as vista_id'
        );
        $query->join('det_rol_vista', 'det_rol_vista.vista_id', '=', 'menu_vistas.id');
        $query->join('roles', 'det_rol_vista.rol_id', '=', 'roles.id');

        if (isset($parametros[RolesVistasModel::ROL_ID])) {
            $query->where(RolesVistasModel::ROL_ID, '=', $parametros[RolesVistasModel::ROL_ID]);
        }

        return  $query->dd();
    }

    //TODO: duplicar con usuarios
    public function returnArrayIdsVistaRoles($parametros)
    {
        $data = $this->vistasByRoles($parametros);
        $array_menu = [];
        foreach ($data as $key => $vista) {
            array_push($array_menu, $vista->vista_id);
        }

        return $array_menu;
    }

    public function storeVistasRoles($parametros)
    {
        $vistas = $parametros[RolesVistasModel::VISTA_ID];
        $this->modelo
            ->where(RolesVistasModel::ROL_ID, '=',  $parametros[RolesVistasModel::ROL_ID])
            ->delete();

        if (isset($vistas)) {
            foreach ($vistas as $key => $item) {
                $this->crear([
                    RolesVistasModel::ROL_ID => $parametros[RolesVistasModel::ROL_ID],
                    RolesVistasModel::VISTA_ID => $item
                ]);
            }
        }
    }
}
