<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\MenuUsuariosVistasModel;
use App\Models\Usuarios\MenuVistasModel;

class ServicioMenuUsuariosVistas extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new MenuUsuariosVistasModel();
        $this->menuVistasModel = new MenuVistasModel();
        $this->recurso = 'usuarios_vistas';
    }

    public function getReglasGuardar()
    {
        return [
            MenuUsuariosVistasModel::USUARIO_ID => 'required|exists:usuarios,id',
            MenuUsuariosVistasModel::VISTA_ID => 'required|exists:menu_vistas,id'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            MenuUsuariosVistasModel::USUARIO_ID => 'required|exists:usuarios,id',
            MenuUsuariosVistasModel::VISTA_ID => 'required|exists:menu_vistas,id'
        ];
    }

    public function getReglasMenuVistaUsuarios()
    {
        return [
            MenuUsuariosVistasModel::USUARIO_ID => 'required|exists:usuarios,id',
            MenuUsuariosVistasModel::VISTA_ID => 'required|array',
            MenuUsuariosVistasModel::SECCION_ID => 'required',
            MenuUsuariosVistasModel::SUBMENU_ID => 'required',
        ];
    }

    public function getReglasVistaByUsuario()
    {
        return [
            MenuUsuariosVistasModel::USUARIO_ID => 'required|exists:usuarios,id'
        ];
    }

    //TODO: revisar consultas
    //TODO: duplicar con usuarios
    public function vistasByUserId($parametros)
    {
        $query = $this->menuVistasModel->select(
            'menu_vistas.nombre',
            'usuarios.nombre',
            'usuarios.id as usuario_id',
            'menu_vistas.id as vista_id'
        );
        $query->join('menu_usuario_vista', 'menu_usuario_vista.vista_id', '=', 'menu_vistas.id');
        $query->join('usuarios', 'menu_usuario_vista.usuario_id', '=', 'usuarios.id');

        if (isset($parametros[MenuUsuariosVistasModel::USUARIO_ID])) {
            $query->where(MenuUsuariosVistasModel::USUARIO_ID, '=', $parametros[MenuUsuariosVistasModel::USUARIO_ID]);
        }

        return  $query->get();
    }

    //TODO: duplicar con usuarios
    public function returnArrayIdsVistaUsuarios($parametros)
    {
        $data = $this->vistasByUserId($parametros);
        $array_menu = [];
        foreach ($data as $key => $vista) {
            array_push($array_menu, $vista->vista_id);
        }

        return $array_menu;
    }

    public function storeVistasUsuarios($parametros)
    {
        $vistas = $parametros[MenuUsuariosVistasModel::VISTA_ID];
        $this->modelo
            ->where(MenuUsuariosVistasModel::USUARIO_ID, '=',  $parametros[MenuUsuariosVistasModel::USUARIO_ID])
            ->where(MenuUsuariosVistasModel::SECCION_ID, '=',  $parametros[MenuUsuariosVistasModel::SECCION_ID])
            ->where(MenuUsuariosVistasModel::SUBMENU_ID, '=',  $parametros[MenuUsuariosVistasModel::SUBMENU_ID])
            ->delete();

        if(isset($vistas)){
            foreach ($vistas as $key => $item) {
                $this->crear([
                    MenuUsuariosVistasModel::USUARIO_ID => $parametros[MenuUsuariosVistasModel::USUARIO_ID],
                    MenuUsuariosVistasModel::VISTA_ID => $item,
                    MenuUsuariosVistasModel::SUBMENU_ID => $parametros[MenuUsuariosVistasModel::SUBMENU_ID],
                    MenuUsuariosVistasModel::SECCION_ID => $parametros[MenuUsuariosVistasModel::SECCION_ID]
                ]);
            }
        }
    }
}
