<?php

namespace App\Servicios\Bodyshop;

use App\Models\Autos\CatModelosModel;
use App\Models\Autos\UnidadesModel;
use App\Models\logistica\CatModosModel;
use App\Models\Oasis\CatEstatusOasisModel;
use App\Models\Oasis\OasisModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\CatalogoUbicacionModel;
use App\Models\Refacciones\UbicacionLLavesModel;
use App\Servicios\Core\ServicioDB;

class ServiciosOasis extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'oasis';
        $this->modelo = new OasisModel();
    }

    public function getReglasGuardar()
    {
        return [
            OasisModel::FECHA => 'required',
            OasisModel::VIN => 'required',
            OasisModel::SERIE_CORTA => 'required',
            OasisModel::ID_UNIDAD => 'required',
            OasisModel::ID_COLOR => 'required',
            OasisModel::ID_ANIO => 'required',
            OasisModel::QR => 'nullable',
            OasisModel::ID_STATUS => 'required',
            OasisModel::ID_USUARIO => 'nullable',
            OasisModel::ID_UBICACION => 'required',
            OasisModel::NO_ECONOMICO => 'required',
            OasisModel::ID_UBICACION_LLAVES => 'required',

            OasisModel::DESCUENTO_MAXIMO => 'nullable',
            OasisModel::TENENCIA => 'nullable',
            OasisModel::SEGURO_COB_AMPLIA => 'nullable',
            OasisModel::PLAN_GANE => 'nullable',
            OasisModel::ORDEN_REPORTE => 'nullable',
            OasisModel::UNIDAD_IMPORTADA => 'nullable',
            OasisModel::TIPO_AUTO => 'nullable',
            OasisModel::COSTO_VALOR_UNIDAD => 'nullable',
            OasisModel::VENTA_VALOR_UNIDAD => 'nullable',
            OasisModel::COSTO_EQUIPO_BASE => 'nullable',
            OasisModel::VENTA_EQUIPO_BASE => 'nullable',
            OasisModel::COSTO_TOTAL_BASE => 'nullable',
            OasisModel::VENTA_TOTAL_BASE => 'nullable',
            OasisModel::COSTO_OTROS_GASTOS => 'nullable',
            OasisModel::VENTA_OTROS_GASTOS => 'nullable',
            OasisModel::COSTO_DEDUCCIONES_FORD => 'nullable',
            OasisModel::VENTA_DEDUCCIONES_FORD => 'nullable',
            OasisModel::COSTO_SEGUROS_TRASLADOS => 'nullable',
            OasisModel::VENTA_SEGUROS_TRASLADOS => 'nullable',
            OasisModel::COSTO_GASTOS_TRASLADOS => 'nullable',
            OasisModel::VENTA_GASTOS_TRASLADOS => 'nullable',
            OasisModel::COSTO_GASTOS_ACOND => 'nullable',
            OasisModel::VENTA_GASTOS_ACOND => 'nullable',
            OasisModel::COSTO_IMP_IMP => 'nullable',
            OasisModel::VENTA_IMP_IMP => 'nullable',
            OasisModel::COSTO_FLETES_EXT => 'nullable',
            OasisModel::VENTA_FLETES_EXT => 'nullable',
            OasisModel::COSTO_ISAN => 'nullable',
            OasisModel::VENTA_ISAN => 'nullable',
            OasisModel::COSTO_SUBTOTAL => 'nullable',
            OasisModel::VENTA_SUBTOTAL => 'nullable',
            OasisModel::COSTO_IVA => 'nullable',
            OasisModel::VENTA_IVA => 'nullable',
            OasisModel::CLAVE_ISAN => 'nullable',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            OasisModel::FECHA => 'required',
            OasisModel::VIN => 'required',
            OasisModel::SERIE_CORTA => 'required',
            OasisModel::ID_UNIDAD => 'required',
            OasisModel::ID_COLOR => 'required',
            OasisModel::ID_ANIO => 'required',
            OasisModel::QR => 'nullable',
            OasisModel::ID_STATUS => 'required',
            OasisModel::ID_USUARIO => 'nullable',
            OasisModel::ID_UBICACION => 'required',
            OasisModel::NO_ECONOMICO => 'required',
            OasisModel::ID_UBICACION_LLAVES => 'required',

            OasisModel::DESCUENTO_MAXIMO => 'nullable',
            OasisModel::TENENCIA => 'nullable',
            OasisModel::SEGURO_COB_AMPLIA => 'nullable',
            OasisModel::PLAN_GANE => 'nullable',
            OasisModel::ORDEN_REPORTE => 'nullable',
            OasisModel::UNIDAD_IMPORTADA => 'nullable',
            OasisModel::TIPO_AUTO => 'nullable',
            OasisModel::COSTO_VALOR_UNIDAD => 'nullable',
            OasisModel::VENTA_VALOR_UNIDAD => 'nullable',
            OasisModel::COSTO_EQUIPO_BASE => 'nullable',
            OasisModel::VENTA_EQUIPO_BASE => 'nullable',
            OasisModel::COSTO_TOTAL_BASE => 'nullable',
            OasisModel::VENTA_TOTAL_BASE => 'nullable',
            OasisModel::COSTO_OTROS_GASTOS => 'nullable',
            OasisModel::VENTA_OTROS_GASTOS => 'nullable',
            OasisModel::COSTO_DEDUCCIONES_FORD => 'nullable',
            OasisModel::VENTA_DEDUCCIONES_FORD => 'nullable',
            OasisModel::COSTO_SEGUROS_TRASLADOS => 'nullable',
            OasisModel::VENTA_SEGUROS_TRASLADOS => 'nullable',
            OasisModel::COSTO_GASTOS_TRASLADOS => 'nullable',
            OasisModel::VENTA_GASTOS_TRASLADOS => 'nullable',
            OasisModel::COSTO_GASTOS_ACOND => 'nullable',
            OasisModel::VENTA_GASTOS_ACOND => 'nullable',
            OasisModel::COSTO_IMP_IMP => 'nullable',
            OasisModel::VENTA_IMP_IMP => 'nullable',
            OasisModel::COSTO_FLETES_EXT => 'nullable',
            OasisModel::VENTA_FLETES_EXT => 'nullable',
            OasisModel::COSTO_ISAN => 'nullable',
            OasisModel::VENTA_ISAN => 'nullable',
            OasisModel::COSTO_SUBTOTAL => 'nullable',
            OasisModel::VENTA_SUBTOTAL => 'nullable',
            OasisModel::COSTO_IVA => 'nullable',
            OasisModel::VENTA_IVA => 'nullable',
            OasisModel::CLAVE_ISAN => 'nullable',
        ];
    }
    public function getAll($parametros)
    {
        $query = $this->modelo
            ->select(
                OasisModel::getTableName() . '.*',
                CatalogoColoresModel::getTableName() . '.' . CatalogoColoresModel::NOMBRE . ' as color',
                CatModelosModel::getTableName() . '.' . CatModelosModel::NOMBRE . ' as modelo',
                CatalogoUbicacionModel::getTableName() . '.' . CatalogoUbicacionModel::NOMBRE . ' as ubicacion',
                UbicacionLLavesModel::getTableName() . '.' . UbicacionLLavesModel::NOMBRE . ' as ubicacion_llaves',
                CatEstatusOasisModel::getTableName() . '.' . CatEstatusOasisModel::ESTATUS . ' as estatus',

            )
            ->join(
                CatalogoColoresModel::getTableName(),
                CatalogoColoresModel::getTableName() . '.' . CatalogoColoresModel::ID,
                '=',
                OasisModel::getTableName() . '.' . OasisModel::ID_COLOR
            )
            ->join(
                CatModelosModel::getTableName(),
                CatModelosModel::getTableName() . '.' . CatModelosModel::ID,
                '=',
                OasisModel::getTableName() . '.' . OasisModel::ID_UNIDAD
            )
            ->join(
                CatalogoUbicacionModel::getTableName(),
                CatalogoUbicacionModel::getTableName() . '.' . CatalogoUbicacionModel::ID,
                '=',
                OasisModel::getTableName() . '.' . OasisModel::ID_UBICACION
            )
            ->join(
                UbicacionLLavesModel::getTableName(),
                UbicacionLLavesModel::getTableName() . '.' . UbicacionLLavesModel::ID,
                '=',
                OasisModel::getTableName() . '.' . OasisModel::ID_UBICACION_LLAVES
            )
            ->join(
                CatEstatusOasisModel::getTableName(),
                CatEstatusOasisModel::getTableName() . '.' . CatEstatusOasisModel::ID,
                '=',
                OasisModel::getTableName() . '.' . OasisModel::ID_STATUS
            );
        return $query->get();
    }
}
