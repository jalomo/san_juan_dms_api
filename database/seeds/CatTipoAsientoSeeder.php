<?php

use App\Models\CuentasPorCobrar\TipoAsientoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatTipoAsientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                TipoAsientoModel::DESCRIPCION => 'Abono'
            ],
            [
                TipoAsientoModel::DESCRIPCION => 'Cargo'
            ]
        );

        foreach ($data as $items) {
            DB::table(TipoAsientoModel::getTableName())->insert($items);
        }
    }
}
