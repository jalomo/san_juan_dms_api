<?php

use App\Models\Refacciones\TipoPedidoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatTipoPedidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [

                TipoPedidoModel::NOMBRE => 'BACK ORDER',
                TipoPedidoModel::CLAVE => 'BACKOR'
            ],
            [
                TipoPedidoModel::NOMBRE => 'COMPLEMENTARIO',
                TipoPedidoModel::CLAVE => 'COMPLE'
            ],
            [
                TipoPedidoModel::NOMBRE => 'DIRECT',
                TipoPedidoModel::CLAVE => 'DIRECTO'
            ],
            [
                TipoPedidoModel::NOMBRE => 'EMERGENCIA',
                TipoPedidoModel::CLAVE => 'EMERGE'
            ],
            [
                TipoPedidoModel::NOMBRE => 'UNIDADES INMOVIL',
                TipoPedidoModel::CLAVE => 'UNINMO'
            ],
            [
                TipoPedidoModel::NOMBRE => 'CONTADO',
                TipoPedidoModel::CLAVE => 'CONTAD'
            ],
            [
                TipoPedidoModel::NOMBRE => 'STOCK',
                TipoPedidoModel::CLAVE => 'STOCK'
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(TipoPedidoModel::getTableName())
                ->insert($items);
        }
    }
}
