<?php

use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusCuentaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                EstatusCuentaModel::ID => 1,
                EstatusCuentaModel::NOMBRE => 'VENTA NUEVA'
            ],
            [
                EstatusCuentaModel::ID => 2,
                EstatusCuentaModel::NOMBRE => 'PAGO LIQUIDADO'
            ],
            [
                EstatusCuentaModel::ID => 3,
                EstatusCuentaModel::NOMBRE => 'PAGO ATRASADO'
            ],
            [
                EstatusCuentaModel::ID => 4,
                EstatusCuentaModel::NOMBRE => 'CANCELADO'
            ],
            [
                EstatusCuentaModel::ID => 5,
                EstatusCuentaModel::NOMBRE => 'PAGO EN PROCESO'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(EstatusCuentaModel::getTableName())->insert($items);
        }
    }
}
