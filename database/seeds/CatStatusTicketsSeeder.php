<?php

use App\Models\Soporte\CatStatusTickets;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatStatusTicketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatStatusTickets::ID => 1,
                CatStatusTickets::ESTATUS => 'ABIERTO'
            ],
            [
                CatStatusTickets::ID => 2,
                CatStatusTickets::ESTATUS => 'PROCESO DE SOLUCIÓN'
            ],
           
        ];

        foreach ($data as $value) {
            DB::table(CatStatusTickets::getTableName())->insert($value);
        }
    }
}
