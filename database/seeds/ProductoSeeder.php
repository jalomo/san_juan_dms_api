<?php

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\StockProductosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        $filename = 'csv/inventario.csv';//'csv/inventario_refacc.csv';
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            while (($data = fgetcsv($file)) !== false) {

                $clave = trim($data[0]);
                $prefijo = trim($data[1]);
                $basico = trim($data[2]);
                $sufijo = trim($data[3]);
                $descripcion = trim($data[4]);
                $precio = trim($data[5]);
                $no_identificacion = "$prefijo$basico$sufijo";

                ProductosModel::create([
                    ProductosModel::NO_IDENTIFICACION_FACTURA =>  $no_identificacion,
                    ProductosModel::BASICO =>  $basico,
                    ProductosModel::PREFIJO =>  $prefijo,
                    ProductosModel::SUFIJO =>  $sufijo,
                    ProductosModel::CLAVE_UNIDAD_FACTURA => "",
                    ProductosModel::CLAVE_PROD_SERV_FACTURA => "",
                    ProductosModel::DESCRIPCION => $descripcion,
                    ProductosModel::UNIDAD => "PIEZA",
                    ProductosModel::CANTIDAD => 0,
                    ProductosModel::VALOR_UNITARIO => $precio,
                    ProductosModel::PRECIO_FACTURA => $precio,
                    ProductosModel::TALLER_ID => 1,
                    ProductosModel::ALMACEN_ID => 1,
                    ProductosModel::UBICACION_PRODUCTO_ID => 1
                ]);
            }
        }
    }
}
