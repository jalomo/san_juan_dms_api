<?php

use App\Models\Autos\UnidadesNuevas\CatEstatusUNModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatStausUNSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatEstatusUNModel::ID => 1,
                CatEstatusUNModel::ESTATUS => 'CDM'
            ],
           
        ];

        foreach ($data as $value) {
            DB::table(CatEstatusUNModel::getTableName())->insert($value);
        }
    }
}
