<?php

use App\Models\Financiamientos\CatTipoSeguroModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tipoSeguroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatTipoSeguroModel::ID => 1,
                CatTipoSeguroModel::SEGURO => 'Anual',
                CatTipoSeguroModel::VALOR => '0'
            ],
            [
                CatTipoSeguroModel::ID => 2,
                CatTipoSeguroModel::SEGURO => 'Financiado',
                CatTipoSeguroModel::VALOR => '0'
            ],
            [
                CatTipoSeguroModel::ID => 3,
                CatTipoSeguroModel::SEGURO => 'Anual contado',
                CatTipoSeguroModel::VALOR => '0'
            ],
           
        ];

        foreach ($data as $value) {
            DB::table(CatTipoSeguroModel::getTableName())->insert($value);
        }
    }
}
