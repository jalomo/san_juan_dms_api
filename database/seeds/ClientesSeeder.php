<?php

use App\Models\Refacciones\ClaveClienteModel;
use App\Models\Refacciones\ClientesModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $filename = 'csv/clientes.csv';  
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            while (($data = fgetcsv($file)) !== false) {
                $numero_cliente = trim($data[1]);
                $tipo_registro = trim($data[2]);
                $nombre = trim($data[3]);

                $apellido_paterno = trim($data[4]);
                $apellido_materno = trim($data[5]);
                $regimen_fiscal = trim($data[6]);
                $nombre_empresa = trim($data[7]);
                $rfc = trim($data[8]);
                $direccion = trim($data[9]);
                $numero_int = trim($data[10]);
                $numero_ext = trim($data[11]);
                $colonia = trim($data[12]);

                $municipio = trim($data[13]);
                $estado = trim($data[14]);
                $pais = trim($data[15]);
                $codigo_postal = trim($data[16]);
                $telefono = trim($data[17]);
                $telefono_secundario = trim($data[18]);
                $telefono_oficina = trim($data[19]);
                $flotillero = trim($data[20]);
                $correo = trim($data[21]);
                $correo_secundario = trim($data[22]);
                $fecha_nacimiento = trim($data[23]);
                $cfdi_id = trim($data[24]);
                $metodo_pago = trim($data[25]);
                $forma_pago = trim($data[26]);
                $saldo = trim($data[27]);
                $limite_credito = trim($data[28]);
                $notas = trim($data[29]);
                $es_cliente = trim($data[30]);

                ClientesModel::create([
                    ClientesModel::NUMERO_CLIENTE => $numero_cliente,
                    ClientesModel::TIPO_REGISTRO => $tipo_registro,
                    ClientesModel::NOMBRE => $nombre,
                    ClientesModel::APELLIDO_MATERNO => $apellido_materno,
                    ClientesModel::APELLIDO_PATERNO => $apellido_paterno,
                    ClientesModel::REGIMEN_FISCAL => $regimen_fiscal,
                    ClientesModel::NOMBRE_EMPRESA => $nombre_empresa,
                    ClientesModel::RFC => $rfc,
                    ClientesModel::DIRECCION => $direccion,
                    ClientesModel::NUMERO_INT => $numero_int,
                    ClientesModel::NUMERO_EXT => $numero_ext,
                    ClientesModel::COLONIA => $colonia,
                    ClientesModel::MUNICIPIO => $municipio,
                    ClientesModel::ESTADO => $estado,
                    ClientesModel::PAIS => $pais,
                    ClientesModel::CODIGO_POSTAL => $codigo_postal,
                    ClientesModel::TELEFONO => $telefono,
                    ClientesModel::TELEFONO_2 => $telefono_secundario,
                    ClientesModel::TELEFONO_3 => $telefono_oficina,
                    ClientesModel::FLOTILLERO => $flotillero,
                    ClientesModel::CORREO_ELECTRONICO => $correo,
                    ClientesModel::CORREO_ELECTRONICO_2 => $correo_secundario,
                    ClientesModel::FECHA_NACIMIENTO => $fecha_nacimiento,
                    ClientesModel::CFDI_ID => $cfdi_id,
                    ClientesModel::METODO_PAGO_ID => $metodo_pago,
                    ClientesModel::FORMA_PAGO => $forma_pago,
                    ClientesModel::SALDO => $saldo,
                    ClientesModel::LIMITE_CREDITO => $limite_credito,
                    ClientesModel::NOTAS => $notas,
                    ClientesModel::ES_CLIENTE => $es_cliente
                ]);
            }
        }
    }
}
