<?php

use App\Models\CPVentas\CPVentasModel;
use Illuminate\Database\Seeder;

class cpVentasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filename = 'csv/cp_ventas.csv';
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            while (($data = fgetcsv($file)) !== false) {
                // dd();
                $no_cuenta = trim($data[1]);
                $vin = trim($data[2]);
                $apellido = trim($data[3]);
                $nombre = trim($data[4]);
                $tipo_cliente = trim($data[5]);
                $telefono_oficina = trim($data[6]);
                $telefono_casa = trim($data[7]);
                $enganche = trim($data[8]);
                $meses_contrato = trim($data[9]);
                $fecha_inicio_contrato = trim($data[10]);
                $fecha_termino = trim($data[11]);
                $tasa_interes = trim($data[12]);
                $mensualidad = trim($data[13]);
                $monto_financiado = trim($data[14]);
                $marca = trim($data[15]);
                $modelo = trim($data[16]);
                $linea = trim($data[17]);
                $tipo_vehiculo = trim($data[18]);
                $anio = trim($data[19]);
                $plan_financiamiento = trim($data[20]);
                $vendedor_asignado = trim($data[21]);
                $asignado = trim($data[22]);
                $email = trim($data[23]);
                $intentos = trim($data[24]);
                $estatus_id = trim($data[25]);
                try {
                    CPVentasModel::create([
                        CPVentasModel::NO_CUENTA => $no_cuenta,
                        CPVentasModel::VIN => $vin,
                        CPVentasModel::APELLIDO => $apellido,
                        CPVentasModel::NOMBRE => $nombre,
                        CPVentasModel::TIPO_CLIENTE => $tipo_cliente,
                        CPVentasModel::TELEFONO_OFICINA => $telefono_oficina,
                        CPVentasModel::TELEFONO_CASA => $telefono_casa,
                        CPVentasModel::ENGANCHE => $enganche,
                        CPVentasModel::MESES_CONTRATO => $meses_contrato,
                        CPVentasModel::FECHA_INICIO_CONTRATO => date('Y-M-d', strtotime($fecha_inicio_contrato)),
                        CPVentasModel::FECHA_TERMINO => date('Y-M-d', strtotime($fecha_termino)),
                        CPVentasModel::TASA_INTERES => $tasa_interes,
                        CPVentasModel::MENSUALIDAD => $mensualidad,
                        CPVentasModel::MONTO_FINANCIADO => $monto_financiado,
                        CPVentasModel::MARCA => $marca,
                        CPVentasModel::MODELO => $modelo,
                        CPVentasModel::LINEA => $linea,
                        CPVentasModel::TIPO_VEHICULO => $tipo_vehiculo,
                        CPVentasModel::ANIO => $anio,
                        CPVentasModel::PLAN_FINANCIAMIENTO => $plan_financiamiento,
                        CPVentasModel::VENDEDOR_ASIGNADO => $vendedor_asignado,
                        CPVentasModel::ASIGNADO => $asignado,
                        CPVentasModel::EMAIL => $email,
                        CPVentasModel::INTENTOS => $intentos,
                        CPVentasModel::ESTATUS_ID => !empty($estatus_id) ? $estatus_id : null
                    ]);
                } catch (\Throwable $th) {
                    // dd($th);
                }
            }
        }
    }
}
