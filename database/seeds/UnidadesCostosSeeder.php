<?php

use App\Models\Refacciones\RecepcionUnidadesCostosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnidadesCostosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_precios = [
            [
                RecepcionUnidadesCostosModel::VALOR_UNIDAD_COSTO => 1000,
                RecepcionUnidadesCostosModel::VALOR_UNIDAD_VENTA => 1200,
                RecepcionUnidadesCostosModel::UNIDAD_ID => 1
            ]
        ];

        foreach ($tipo_precios as $key => $value) {
            DB::table(RecepcionUnidadesCostosModel::getTableName())->insert([
                RecepcionUnidadesCostosModel::UNIDAD_ID => $value[RecepcionUnidadesCostosModel::UNIDAD_ID],
                RecepcionUnidadesCostosModel::VALOR_UNIDAD_VENTA => $value[RecepcionUnidadesCostosModel::VALOR_UNIDAD_VENTA],
                RecepcionUnidadesCostosModel::VALOR_UNIDAD_COSTO => $value[RecepcionUnidadesCostosModel::VALOR_UNIDAD_COSTO]
            ]);
        }
    }
}
