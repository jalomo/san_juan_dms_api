<?php

use App\Models\Usuarios\UsuariosModulosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuModulosUsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $filename = 'csv/det_usuarios_modulos.csv';
        $file_path = base_path('database/' . $filename);

        if (file_exists($file_path)) {
            $file = fopen($file_path, 'r');
            while (($data = fgetcsv($file)) !== false) {

                $modulo_id = trim($data[1]);
                $usuario_id = trim($data[2]);
                
                UsuariosModulosModel::create([
                    UsuariosModulosModel::MODULO_ID => $modulo_id,
                    UsuariosModulosModel::USUARIO_ID => $usuario_id
                ]);
            }
        }
    }
}
