<?php

use App\Models\Soporte\TelefonosSoporteModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class telefonos_usuarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                TelefonosSoporteModel::ID => 1,
                TelefonosSoporteModel::USUARIO => 'ANGEL SALAS',
                TelefonosSoporteModel::TELEFONO => '3318953592'
            ],
            [
                TelefonosSoporteModel::ID => 2,
                TelefonosSoporteModel::USUARIO => 'GREGORIO JALOMO',
                TelefonosSoporteModel::TELEFONO => '3121189964'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(TelefonosSoporteModel::getTableName())->insert($items);
        }
    }
}
