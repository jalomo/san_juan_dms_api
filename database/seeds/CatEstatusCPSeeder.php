<?php

use App\Models\CPVentas\CatStatusCPModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatEstatusCPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatStatusCPModel::ESTATUS => 'Interesado'
            ],
            [
                CatStatusCPModel::ESTATUS => 'Opciones'
            ],
            [
                CatStatusCPModel::ESTATUS => 'Ya compro'
            ],
            [
                CatStatusCPModel::ESTATUS => 'Renovado'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(CatStatusCPModel::getTableName())->insert($items);
        }
    }
}
