<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaTipoCatalogoModel as Model;
use Illuminate\Support\Facades\DB;

class CaTipoCatalogoSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID=>1,Model::Descripcion=>'Cálculo',Model::Campo_1=>'Límite superior',Model::Campo_2=>'Cuota fija',Model::Campo_3=>'Procentaje'],
            [Model::ID=>2,Model::Descripcion=>'Valoraciones',Model::Campo_1=>'Antigüedad',Model::Campo_2=>'Días disfrute',Model::Campo_3=>'Días prima v.'],
            [Model::ID=>3,Model::Descripcion=>'Salario',Model::Campo_1=>'Antigüedad',Model::Campo_2=>'Días prima v.',Model::Campo_3=>'Días aguinaldo'],
            [Model::ID=>4,Model::Descripcion=>'Faltas',Model::Campo_1=>'Días laborados por semana',Model::Campo_2=>'Días ausentismo mes',Model::Campo_3=>'Días a descontar']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
