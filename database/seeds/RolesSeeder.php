<?php

use App\Models\Usuarios\RolModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                RolModel::ID => 1,
                RolModel::ROL => 'Administrador',

            ],
            [
                RolModel::ID => 2,
                RolModel::ROL => 'Refacciones',
            ],
            [
                RolModel::ID => 3,
                RolModel::ROL => 'Contabilidad',
            ],
            [
                RolModel::ID => 4,
                RolModel::ROL => 'Caja',
            ],
            [
                RolModel::ID => 5,
                RolModel::ROL => 'Mostrador',
            ],
            [
                RolModel::ID => 6,
                RolModel::ROL => 'Autos nuevos',
            ],
            [
                RolModel::ID => 7,
                RolModel::ROL => 'Autos seminuevos',
            ],
            [
                RolModel::ID => 8,
                RolModel::ROL => 'Asesor prospectos ventas',
            ],
            [
                RolModel::ID => 9,
                RolModel::ROL => 'Asesor Telemarketing',
            ],
            [
                RolModel::ID => 10,
                RolModel::ROL => 'Asesor web ventas',
            ],
            [
                RolModel::ID => 11,
                RolModel::ROL => 'Vendedores',
            ]
        ];

        foreach ($roles as $key => $value) {
            DB::table(RolModel::getTableName())->insert([
                RolModel::ID => $value[RolModel::ID],
                RolModel::ROL => $value[RolModel::ROL],
            ]);
        }
    }
}
