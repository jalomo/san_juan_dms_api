<?php

use App\Models\Soporte\CatStatusTickets;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class addStatusTickets extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatStatusTickets::ID => 3,
                CatStatusTickets::ESTATUS => 'CERRADO'
            ],
           
        ];

        foreach ($data as $value) {
            DB::table(CatStatusTickets::getTableName())->insert($value);
        }
    }
}
