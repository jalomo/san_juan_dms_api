<?php

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\StockProductosModel;
use Illuminate\Database\Seeder;

class StockProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
                $productos = ProductosModel::get();
                foreach($productos as $val) {
                    StockProductosModel::create([
                        StockProductosModel::PRODUCTO_ID =>  $val->id,
                        StockProductosModel::CANTIDAD_ACTUAL =>  5,
                        StockProductosModel::CANTIDAD_ALMACEN_PRIMARIO =>  5,
                        StockProductosModel::CANTIDAD_ALMACEN_SECUNDARIO => 0,
                        StockProductosModel::CANTIDAD_COMPRAS => 0,
                        StockProductosModel::CANTIDAD_VENTAS => 0,
                        StockProductosModel::TOTAL_PRECIO_COMPRAS => 0,
                        StockProductosModel::TOTAL_PRECIO_VENTAS => 0,
                    ]);
                }
            
    
    }
}
