<?php

use App\Models\Usuarios\Menu\ModulosModel;
use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuVistasModel;
use App\Models\Usuarios\RolesModulosModel;
use App\Models\Usuarios\RolesSeccionesModel;
use App\Models\Usuarios\RolesSubmenuModel;
use App\Models\Usuarios\RolesVistasModel;
use App\Models\Usuarios\RolModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuModulosRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->modulosmodel = new ModulosModel();
        // $this->rolessmodel = new RolModel();
        $this->menuSeccionesModel = new MenuSeccionesModel();
        $this->menuSubmenuModel = new MenuSubmenuModel();
        $this->menuVistasModel = new MenuVistasModel();

        $total_modulos = $this->modulosmodel->get();

        foreach ($total_modulos as $key => $modulo) {
            DB::table(RolesModulosModel::getTableName())->insert([
                RolesModulosModel::ROL_ID => RolModel::ROL_ADMIN,
                RolesModulosModel::MODULO_ID => $modulo->id
            ]);
        }

        $total_secciones = $this->menuSeccionesModel->get();
        foreach ($total_secciones as $key => $seccion) {
            DB::table(RolesSeccionesModel::getTableName())->insert([
                RolesSeccionesModel::ROL_ID => RolModel::ROL_ADMIN,
                RolesSeccionesModel::SECCION_ID => $seccion->id
            ]);
        }

        $total_submenu = $this->menuSubmenuModel->get();
        foreach ($total_submenu as $key => $submenu) {
            DB::table(RolesSubmenuModel::getTableName())->insert([
                RolesSubmenuModel::ROL_ID => RolModel::ROL_ADMIN,
                RolesSubmenuModel::SUBMENU_ID => $submenu->id
            ]);
        }

        $total_vistas = $this->menuVistasModel->get();
        foreach ($total_vistas as $key => $vista) {
            DB::table(RolesVistasModel::getTableName())->insert([
                RolesVistasModel::ROL_ID => RolModel::ROL_ADMIN,
                RolesVistasModel::VISTA_ID => $vista->id
            ]);
        }
    }
}
