<?php

use App\Models\Autos\EstatusVentaAutosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusVentaAutosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                EstatusVentaAutosModel::ID => 1,
                EstatusVentaAutosModel::NOMBRE => 'PREVENTA GENERADA'
            ],
            [
                
                EstatusVentaAutosModel::ID => 2,
                EstatusVentaAutosModel::NOMBRE => 'CREDITO APROBADO'
            ],
            [
                EstatusVentaAutosModel::ID => 3,
                EstatusVentaAutosModel::NOMBRE => 'CREDITO RECHAZADO'
            ],
            [
                EstatusVentaAutosModel::ID => 4,
                EstatusVentaAutosModel::NOMBRE => 'ABONO PAGADO, AUTO PENDIENTE'
            ],
            [
                EstatusVentaAutosModel::ID => 5,
                EstatusVentaAutosModel::NOMBRE => 'ABONO PAGADO'
            ],
            [
                EstatusVentaAutosModel::ID => 6,
                EstatusVentaAutosModel::NOMBRE => 'ASESOR NO HA PROCESADO EL ABONO '
            ],
            [
                EstatusVentaAutosModel::ID => 7,
                EstatusVentaAutosModel::NOMBRE => 'PAGO PENDIENTE'
            ],
            [
                EstatusVentaAutosModel::ID => 8,
                EstatusVentaAutosModel::NOMBRE => 'PAGO LISTO, PENDIENTE EN CAJA'
            ],
            [
                EstatusVentaAutosModel::ID => 9,
                EstatusVentaAutosModel::NOMBRE => 'PAGO TOTAL PENDIENTE'
            ],
            [
                EstatusVentaAutosModel::ID => 10,
                EstatusVentaAutosModel::NOMBRE => 'PAGO TOTAL LISTO'
            ],
            [
                EstatusVentaAutosModel::ID => 11,
                EstatusVentaAutosModel::NOMBRE => 'DOCUMENTOS FACTURA PENDIENTE'
            ],
            [
                EstatusVentaAutosModel::ID => 12,
                EstatusVentaAutosModel::NOMBRE => 'CONTRATOS PENDIENTES'
            ],
            [
                EstatusVentaAutosModel::ID => 13,
                EstatusVentaAutosModel::NOMBRE => 'CONTRATOS LISTOS'
            ],
            [
                EstatusVentaAutosModel::ID => 14,
                EstatusVentaAutosModel::NOMBRE => 'PASE DE SALIDA PENDIENTE'
            ],
            [
                EstatusVentaAutosModel::ID => 15,
                EstatusVentaAutosModel::NOMBRE => 'PASE DE SALIDA LISTO'
            ],
            [
                EstatusVentaAutosModel::ID => 16,
                EstatusVentaAutosModel::NOMBRE => 'UNIDAD ENTREGADA'
            ],
            [
                EstatusVentaAutosModel::ID => 17,
                EstatusVentaAutosModel::NOMBRE => 'PREVENTA SIN UNIDAD EN INVENTARIO'
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(EstatusVentaAutosModel::getTableName())
                ->insert($items);
        }
    }
}
