<?php

use App\Models\Autos\UnidadesNuevas\ListPreciosUNModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Table_lista_precios_un extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lista_precios = [
            [
                ListPreciosUNModel::ID => 1,
                ListPreciosUNModel::CAT => '001',
                ListPreciosUNModel::CLAVE_VEHICULAR => '0001',
                ListPreciosUNModel::DESCRIPCION => 'AUTO LOBO LIMITED 2021 LOBO 4X4 LIMITED'
            ],
            [
                ListPreciosUNModel::ID => 2,
                ListPreciosUNModel::CAT => '002',
                ListPreciosUNModel::CLAVE_VEHICULAR => '002',
                ListPreciosUNModel::DESCRIPCION => 'AUTO RANGER SAMERICA 2022 RANGER SA CREW XLT 4X4'
            ],
            [
                ListPreciosUNModel::ID => 3,
                ListPreciosUNModel::CAT => '003',
                ListPreciosUNModel::CLAVE_VEHICULAR => '003',
                ListPreciosUNModel::DESCRIPCION => 'AUTO MUSTANG 2021 MACH 1 COUPE'
            ]
        ];
        foreach ($lista_precios as $value) {
            DB::table(ListPreciosUNModel::getTableName())->insert([
                ListPreciosUNModel::ID => $value[ListPreciosUNModel::ID],
                ListPreciosUNModel::CAT => $value[ListPreciosUNModel::CAT],
                ListPreciosUNModel::CLAVE_VEHICULAR => $value[ListPreciosUNModel::CLAVE_VEHICULAR],
                ListPreciosUNModel::DESCRIPCION => $value[ListPreciosUNModel::DESCRIPCION]
            ]);
        }
    }
}
