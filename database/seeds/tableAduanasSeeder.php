<?php

use App\Models\Autos\UnidadesNuevas\CatAduanasUNModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tableAduanasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aduanas = [
            [
                CatAduanasUNModel::ID => 1,
                CatAduanasUNModel::ADUANA => 'Aduana 1',
            ],
            [
                CatAduanasUNModel::ID => 2,
                CatAduanasUNModel::ADUANA => 'Aduana 2',
            ],
            [
                CatAduanasUNModel::ID => 3,
                CatAduanasUNModel::ADUANA => 'Aduana 3',
            ]
        ];

        foreach ($aduanas as $value) {
            DB::table(CatAduanasUNModel::getTableName())->insert([
                CatAduanasUNModel::ID => $value[CatAduanasUNModel::ID],
                CatAduanasUNModel::ADUANA => $value[CatAduanasUNModel::ADUANA]
            ]);
        }
    }
}
