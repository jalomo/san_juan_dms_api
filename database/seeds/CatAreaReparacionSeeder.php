<?php

use App\Models\Logistica\CatAreaReparacionModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatAreaReparacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatAreaReparacionModel::ID => 1,
                CatAreaReparacionModel::AREA_REPARACION => 'Bodyshop'
            ],
            [
                CatAreaReparacionModel::ID => 2,
                CatAreaReparacionModel::AREA_REPARACION => 'Bodega'
            ],
            [
                CatAreaReparacionModel::ID => 3,
                CatAreaReparacionModel::AREA_REPARACION => 'Otros'
            ],
        );

        foreach ($data as $key => $items) {
            DB::table(CatAreaReparacionModel::getTableName())->insert($items);
        }
    }
}
