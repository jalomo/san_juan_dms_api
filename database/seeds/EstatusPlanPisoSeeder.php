<?php

use App\Models\Financiamientos\CatEstatusPlanPisoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusPlanPisoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatEstatusPlanPisoModel::ID => 1,
                CatEstatusPlanPisoModel::ESTATUS => 'UNIDAD PROPIA'
            ],
            [
                CatEstatusPlanPisoModel::ID => 2,
                CatEstatusPlanPisoModel::ESTATUS => 'INTERCAMBIO'
            ],
            [
                CatEstatusPlanPisoModel::ID => 3,
                CatEstatusPlanPisoModel::ESTATUS => 'SE DEBE'
            ]
        ];
        foreach ($data as $key => $items) {
            DB::table(CatEstatusPlanPisoModel::getTableName())->insert($items);
        }
    }
}
