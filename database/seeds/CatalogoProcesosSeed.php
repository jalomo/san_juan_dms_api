<?php

use App\Models\Contabilidad\CatalogoProcesosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoProcesosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data =  [
            [CatalogoProcesosModel::DESCRIPCION => 'VENTAS MENUDEO REFACCIONES'],
            [CatalogoProcesosModel::DESCRIPCION => 'TRASPASO'],
            [CatalogoProcesosModel::DESCRIPCION => 'COMPRAS'],
            [CatalogoProcesosModel::DESCRIPCION => 'DEVOLUCIÓN PROVEEDOR'],
            [CatalogoProcesosModel::DESCRIPCION => 'DEVOLUCIÓN VENTAS'],
            [CatalogoProcesosModel::DESCRIPCION => 'VENTA DE AUTOS'],
            [CatalogoProcesosModel::DESCRIPCION => 'VENTA DE AUTOS SEMINUEVOS'],
            [CatalogoProcesosModel::DESCRIPCION => 'MANO DE OBRA'],
            [CatalogoProcesosModel::DESCRIPCION => 'BANORTE'],
            [CatalogoProcesosModel::DESCRIPCION => 'REDONDEO'],
            [CatalogoProcesosModel::DESCRIPCION => 'SERVICIOS']
        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoProcesosModel::getTableName())
                ->insert($items);
        }
    }
}
