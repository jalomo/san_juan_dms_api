<?php

use App\Models\Refacciones\EstatusCompra as EstatusCompraModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusCompraSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                EstatusCompraModel::ID => 1,
                EstatusCompraModel::NOMBRE => 'PROCESO'
            ],
            [
                EstatusCompraModel::ID => 2,
                EstatusCompraModel::NOMBRE => 'COMPRA REALIZADA'
            ],
            [
                EstatusCompraModel::ID => 3,
                EstatusCompraModel::NOMBRE => 'FACTURA PAGADA'
            ],
            [
                EstatusCompraModel::ID => 4,
                EstatusCompraModel::NOMBRE => 'CANCELADA'
            ],
            [
                EstatusCompraModel::ID => 5,
                EstatusCompraModel::NOMBRE => 'DEVOLUCION'
            ],
        );

        foreach ($data as $key => $items) {
            DB::table(EstatusCompraModel::getTableName())->insert($items);
        }
    }
}
