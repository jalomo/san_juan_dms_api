<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Facturas\Factura;
use App\Models\Refacciones\ReFacturaOrdenCompraModel;
use App\Models\Refacciones\OrdenCompraModel;
use App\Models\Refacciones\EstatusFacturaModel;

class CreateTableReFacturaOrdenCompra extends Migration
{
    public function up()
    {
        Schema::create(ReFacturaOrdenCompraModel::getTableName(), function (Blueprint $table) {
            $table->increments(ReFacturaOrdenCompraModel::ID);
            $table->unsignedInteger(ReFacturaOrdenCompraModel::FACTURA_ID);
            $table->unsignedInteger(ReFacturaOrdenCompraModel::ORDEN_COMPRA_ID);
            $table->unsignedInteger(ReFacturaOrdenCompraModel::ESTATUS_FACTURA_ID);
            $table->boolean(ReFacturaOrdenCompraModel::ACTIVO)->default(1);
            $table->unsignedInteger(ReFacturaOrdenCompraModel::USER_ID);
            $table->foreign(ReFacturaOrdenCompraModel::ESTATUS_FACTURA_ID)->references(EstatusFacturaModel::ID)->on(EstatusFacturaModel::getTableName());
            $table->foreign(ReFacturaOrdenCompraModel::FACTURA_ID)->references(Factura::ID)->on(Factura::getTableName());
            $table->foreign(ReFacturaOrdenCompraModel::ORDEN_COMPRA_ID)->references(OrdenCompraModel::ID)->on(OrdenCompraModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ReFacturaOrdenCompraModel::getTableName());
    }
}
