<?php

use App\Models\Autos\Inventario\CarroceriaModel;
use App\Models\Autos\Inventario\InventarioModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaInventarioCarroceria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
         Schema::create(CarroceriaModel::getTableName(), function (Blueprint $table) {
            $table->increments(CarroceriaModel::ID);
            $table->text(CarroceriaModel::CARROCERIA_IMG)->nullable();
            $table->unsignedInteger(CarroceriaModel::ID_INVENTARIO);
            $table->foreign(CarroceriaModel::ID_INVENTARIO)
                ->references(InventarioModel::ID)
                ->on(InventarioModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop(CarroceriaModel::getTableName());
    }
}
