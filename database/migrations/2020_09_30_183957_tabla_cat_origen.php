<?php

use App\Models\Telemarketing\CatalogoOrigenesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaCatOrigen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatalogoOrigenesModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoOrigenesModel::ID);
            $table->string(CatalogoOrigenesModel::ORIGEN);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoOrigenesModel::getTableName());
    }
}
