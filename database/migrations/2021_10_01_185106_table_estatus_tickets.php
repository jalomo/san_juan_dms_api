<?php

use App\Models\Soporte\CatStatusTickets;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableEstatusTickets extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatStatusTickets::getTableName(), function (Blueprint $table) {
            $table->increments(CatStatusTickets::ID);
            $table->string(CatStatusTickets::ESTATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatStatusTickets::getTableName());
    }
}
