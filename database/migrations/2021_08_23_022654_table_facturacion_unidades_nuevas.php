<?php

use App\Models\Autos\UnidadesNuevas\CatAduanasUNModel;
use App\Models\Autos\UnidadesNuevas\FacturacionModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableFacturacionUnidadesNuevas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FacturacionModel::getTableName(), function (Blueprint $table) {
            $table->increments(FacturacionModel::ID);
            $table->unsignedInteger(FacturacionModel::REMISION_ID);
            $table->foreign(FacturacionModel::REMISION_ID)
                ->references(RemisionModel::ID)
                ->on(RemisionModel::getTableName());
            $table->string(FacturacionModel::PROCEDENCIA);
            $table->string(FacturacionModel::VENDEDOR);
            $table->unsignedInteger(FacturacionModel::ADUANA_ID);
            $table->foreign(FacturacionModel::ADUANA_ID)
                ->references(CatAduanasUNModel::ID)
                ->on(CatAduanasUNModel::getTableName());
            $table->string(FacturacionModel::REPUVE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(FacturacionModel::getTableName());
    }
}
