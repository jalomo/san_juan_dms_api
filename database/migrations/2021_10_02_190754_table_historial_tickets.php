<?php

use App\Models\Soporte\CatStatusTickets;
use App\Models\Soporte\HistorialTicketsModel;
use App\Models\Soporte\TicketsModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableHistorialTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(HistorialTicketsModel::getTableName(), function (Blueprint $table) {
            $table->increments(HistorialTicketsModel::ID);
            $table->text(HistorialTicketsModel::COMENTARIO);
            $table->string(HistorialTicketsModel::USUARIO);
            $table->string(HistorialTicketsModel::EVIDENCIA);
            $table->unsignedInteger(HistorialTicketsModel::ESTATUS_ANTERIOR_ID)->nullable();
            $table->foreign(HistorialTicketsModel::ESTATUS_ANTERIOR_ID)
                ->references(CatStatusTickets::ID)
                ->on(CatStatusTickets::getTableName());
            
            $table->unsignedInteger(HistorialTicketsModel::ESTATUS_NUEVO_ID)->nullable();
            $table->foreign(HistorialTicketsModel::ESTATUS_NUEVO_ID)
                ->references(CatStatusTickets::ID)
                ->on(CatStatusTickets::getTableName());

            $table->unsignedInteger(HistorialTicketsModel::USER_ID)->nullable();
                $table->foreign(HistorialTicketsModel::USER_ID)
                    ->references(User::ID)
                    ->on(User::getTableName());

            $table->unsignedInteger(HistorialTicketsModel::TICKET_ID);
            $table->foreign(HistorialTicketsModel::TICKET_ID)
                        ->references(TicketsModel::ID)
                        ->on(TicketsModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
