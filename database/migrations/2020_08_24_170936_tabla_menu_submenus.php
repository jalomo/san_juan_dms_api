<?php

use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaMenuSubmenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MenuSubmenuModel::getTableName(), function (Blueprint $table) {
            $table->increments(MenuSubmenuModel::ID);
            $table->string(MenuSubmenuModel::NOMBRE);
            $table->unsignedInteger(MenuSubmenuModel::SECCION_ID);
            $table->foreign(MenuSubmenuModel::SECCION_ID)
                ->references(MenuSeccionesModel::ID)
                ->on(MenuSeccionesModel::getTableName());
            $table->integer(MenuSubmenuModel::VISIBLE)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MenuSubmenuModel::getTableName());
    }
}
