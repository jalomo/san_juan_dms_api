<?php

use App\Models\Refacciones\TipoPedidoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaCatTipoPedido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TipoPedidoModel::getTableName(), function (Blueprint $table) {
            $table->increments(TipoPedidoModel::ID);
            $table->string(TipoPedidoModel::NOMBRE);
            $table->string(TipoPedidoModel::CLAVE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TipoPedidoModel::getTableName());
    }
}
