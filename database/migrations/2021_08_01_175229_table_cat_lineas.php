<?php

use App\Models\Autos\UnidadesNuevas\CatLineas;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatLineas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatLineas::getTableName(), function (Blueprint $table) {
            $table->increments(CatLineas::ID);
            $table->integer(CatLineas::MODELO);
            $table->string(CatLineas::DESCRIPCION);
            $table->string(CatLineas::LINEA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatLineas::getTableName());
    }
}
