<?php

use App\Models\Soporte\CatStatusPrioridadesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatEstatusPrioridades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatStatusPrioridadesModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatStatusPrioridadesModel::ID);
            $table->string(CatStatusPrioridadesModel::PRIORIDAD);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatStatusPrioridadesModel::getTableName());
    }
}
