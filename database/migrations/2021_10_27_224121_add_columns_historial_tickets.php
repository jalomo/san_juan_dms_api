<?php

use App\Models\Soporte\HistorialTicketsModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsHistorialTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(HistorialTicketsModel::getTableName(), function (Blueprint $table) {
            $table->dateTime(HistorialTicketsModel::INICIO_ESTATUS)->default(null);
            $table->dateTime(HistorialTicketsModel::FIN_ESTATUS)->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(HistorialTicketsModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(HistorialTicketsModel::INICIO_ESTATUS);
            $table->dropColumn(HistorialTicketsModel::FIN_ESTATUS);
        });
    }
}
