<?php

use App\Models\Financiamientos\CatEstatusPlanPisoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatEstatusPiso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatEstatusPlanPisoModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatEstatusPlanPisoModel::ID);
            $table->string(CatEstatusPlanPisoModel::ESTATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatEstatusPlanPisoModel::getTableName());
    }
}
