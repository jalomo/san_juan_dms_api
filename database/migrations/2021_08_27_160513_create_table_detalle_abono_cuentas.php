<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\DetalleAsientoCuentasModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\TipoAsientoModel;
use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\CuentasPorCobrar\EstatusAsientosModel;

class CreateTableDetalleAbonoCuentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DetalleAsientoCuentasModel::getTableName(), function (Blueprint $table) {
            $table->increments(DetalleAsientoCuentasModel::ID);
            $table->unsignedInteger(DetalleAsientoCuentasModel::CUENTA_POR_COBRAR_ID);
            $table->foreign(DetalleAsientoCuentasModel::CUENTA_POR_COBRAR_ID)->references(CuentasPorCobrarModel::ID)->on(CuentasPorCobrarModel::getTableName());
            $table->integer(DetalleAsientoCuentasModel::ABONO_ID);
            $table->unsignedInteger(DetalleAsientoCuentasModel::TIPO_ASIENTO_ID);
            $table->foreign(DetalleAsientoCuentasModel::TIPO_ASIENTO_ID)->references(TipoAsientoModel::ID)->on(TipoAsientoModel::getTableName());
            $table->unsignedInteger(DetalleAsientoCuentasModel::CUENTA_ID);
            $table->foreign(DetalleAsientoCuentasModel::CUENTA_ID)->references(CatalogoCuentasModel::ID)->on(CatalogoCuentasModel::getTableName());
            $table->float(DetalleAsientoCuentasModel::TOTAL_PAGO);
            $table->date(DetalleAsientoCuentasModel::FECHA_PAGO);
            $table->string(DetalleAsientoCuentasModel::POLIZA_ID)->nullable();
            $table->text(DetalleAsientoCuentasModel::REFERENCIA)->nullable();
            $table->unsignedInteger(DetalleAsientoCuentasModel::ESTATUS_ID)->default(1);
            $table->foreign(DetalleAsientoCuentasModel::ESTATUS_ID)->references(EstatusAsientosModel::ID)->on(EstatusAsientosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DetalleAsientoCuentasModel::getTableName());
    }
}
