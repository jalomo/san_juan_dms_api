<?php

use App\Models\Autos\UnidadesNuevas\MemoCompraModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableMemoCompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MemoCompraModel::getTableName(), function (Blueprint $table) {
            $table->increments(MemoCompraModel::ID);
            $table->unsignedInteger(MemoCompraModel::REMISION_ID);
            $table->string(MemoCompraModel::CM);
            $table->foreign(MemoCompraModel::REMISION_ID)
                ->references(RemisionModel::ID)
                ->on(RemisionModel::getTableName());
            $table->string(MemoCompraModel::MES);
            $table->string(MemoCompraModel::NO_PRODUCTO);
            $table->string(MemoCompraModel::MEMO);
            $table->float(MemoCompraModel::COSTO_REMISION);
            $table->boolean(MemoCompraModel::COMPRAR);
            $table->integer(MemoCompraModel::MES_COMPRA);
            $table->string(MemoCompraModel::FP);
            $table->integer(MemoCompraModel::DIAS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MemoCompraModel::getTableName());
    }
}
