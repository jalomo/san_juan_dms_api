<?php

use App\Models\Autos\SalidaUnidades\DocumentosModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaDocumentos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DocumentosModel::getTableName(), function (Blueprint $table) {
            $table->increments(DocumentosModel::ID);
            $table->string(DocumentosModel::CARTA_FACTURA)->nullable();
            $table->string(DocumentosModel::POLIZA_GARANTIA)->nullable();
            $table->string(DocumentosModel::POLIZA_SEGURO)->nullable();
            $table->string(DocumentosModel::TENENCIA)->nullable();
            $table->string(DocumentosModel::HERRAMIENTAS)->nullable();
            $table->unsignedInteger(DocumentosModel::ID_VENTA_AUTO);
            $table->foreign(DocumentosModel::ID_VENTA_AUTO)->references(VentasAutosModel::ID)->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DocumentosModel::getTableName());
    }
}
