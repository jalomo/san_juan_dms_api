<?php

use App\Models\Autos\SalidaUnidades\AhorroCombustibleModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaAhorroCombustible extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AhorroCombustibleModel::getTableName(), function (Blueprint $table) {
            $table->increments(AhorroCombustibleModel::ID);
            $table->string(AhorroCombustibleModel::IVCT)->nullable();
            $table->string(AhorroCombustibleModel::TIVCT)->nullable();
            $table->unsignedInteger(AhorroCombustibleModel::ID_VENTA_AUTO);
            $table->foreign(AhorroCombustibleModel::ID_VENTA_AUTO)
                ->references(VentasAutosModel::ID)->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(AhorroCombustibleModel::getTableName());
    }
}
