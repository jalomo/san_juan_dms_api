<?php

use App\Models\Autos\UnidadesNuevas\CatProveedoresUN;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatProveedoresUnidadesNuevas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatProveedoresUN::getTableName(), function (Blueprint $table) {
            $table->increments(CatProveedoresUN::ID);
            $table->string(CatProveedoresUN::CLAVE);
            $table->string(CatProveedoresUN::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatProveedoresUN::getTableName());
    }
}
