<?php

use App\Models\Financiamientos\EstatusFinanciamientosModel;
use App\Models\Financiamientos\FinanciamientosModel;
use App\Models\Financiamientos\HistorialEstatusFinanciamientosModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableHistorialEstatusFinanciamientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(HistorialEstatusFinanciamientosModel::getTableName(), function (Blueprint $table) {
            $table->increments(HistorialEstatusFinanciamientosModel::ID);
            $table->unsignedInteger(HistorialEstatusFinanciamientosModel::ID_ESTATUS);
            $table->foreign(HistorialEstatusFinanciamientosModel::ID_ESTATUS)->references(EstatusFinanciamientosModel::ID)->on(EstatusFinanciamientosModel::getTableName());
            $table->text(HistorialEstatusFinanciamientosModel::COMENTARIO);
            $table->unsignedInteger(HistorialEstatusFinanciamientosModel::ID_USUARIO);
            $table->foreign(HistorialEstatusFinanciamientosModel::ID_USUARIO)->references(User::ID)->on(User::getTableName());
            $table->unsignedInteger(HistorialEstatusFinanciamientosModel::ID_FINANCIAMIENTO);
            $table->foreign(HistorialEstatusFinanciamientosModel::ID_FINANCIAMIENTO)->references(FinanciamientosModel::ID)->on(FinanciamientosModel::getTableName());
            $table->smallInteger(HistorialEstatusFinanciamientosModel::TIPO_COMENTARIO)->comment('1 estatus general, 2 estatus de medición de tiempo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(HistorialEstatusFinanciamientosModel::getTableName());
    }
}
