<?php

use App\Models\CuentasPorCobrar\DocumentosSolicitudModel;
use App\Models\CuentasPorCobrar\SolicitudCreditoModel;
use App\Models\CuentasPorCobrar\TipoDocumentoCreditoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaDocumentosSolicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DocumentosSolicitudModel::getTableName(), function (Blueprint $table) {
            $table->increments(DocumentosSolicitudModel::ID);
            
            $table->unsignedInteger(DocumentosSolicitudModel::ID_SOLICITUD);
            $table->foreign(DocumentosSolicitudModel::ID_SOLICITUD)->references(SolicitudCreditoModel::ID)->on(SolicitudCreditoModel::getTableName());

            $table->unsignedInteger(DocumentosSolicitudModel::ID_TIPO_DOCUMENTO);
            $table->foreign(DocumentosSolicitudModel::ID_TIPO_DOCUMENTO)->references(TipoDocumentoCreditoModel::ID)->on(TipoDocumentoCreditoModel::getTableName());
            $table->string(DocumentosSolicitudModel::RUTA_ARCHIVO);
            $table->string(DocumentosSolicitudModel::NOMBRE_ARCHIVO);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DocumentosSolicitudModel::getTableName());
    }
}
