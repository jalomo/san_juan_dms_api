<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Contabilidad\CatalogoProcesosModel;

class CreateTableCatalogoProcesos extends Migration
{

    public function up()
    {
        Schema::create(CatalogoProcesosModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoProcesosModel::ID);
            $table->string(CatalogoProcesosModel::DESCRIPCION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoProcesosModel::getTableName());
    }
}
