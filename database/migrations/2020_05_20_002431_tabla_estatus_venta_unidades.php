<?php

use App\Models\Autos\EstatusVentaAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaEstatusVentaUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EstatusVentaAutosModel::getTableName(), function (Blueprint $table) {
            $table->increments(EstatusVentaAutosModel::ID);
            $table->string(EstatusVentaAutosModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
