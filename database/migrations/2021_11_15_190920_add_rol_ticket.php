<?php

use App\Models\Soporte\CatRolesSoporteModel;
use App\Models\Soporte\TicketsModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRolTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(TicketsModel::getTableName(), function (Blueprint $table) {
            $table->unsignedInteger(TicketsModel::ROL_ID)->nullable();
            $table->foreign(TicketsModel::ROL_ID)
                ->references(CatRolesSoporteModel::ID)
                ->on(CatRolesSoporteModel::getTableName());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(TicketsModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(TicketsModel::ROL_ID);
        });
    }
}
