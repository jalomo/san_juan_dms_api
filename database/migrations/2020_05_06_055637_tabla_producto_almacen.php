<?php

use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\ProductoAlmacenModel;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaProductoAlmacen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ProductoAlmacenModel::getTableName(), function (Blueprint $table) {
            $table->increments(ProductoAlmacenModel::ID);
            $table->unsignedInteger(ProductoAlmacenModel::ALMACEN_ID);
            $table->foreign(ProductoAlmacenModel::ALMACEN_ID)->references(Almacenes::ID)->on(Almacenes::getTableName());

            $table->unsignedInteger(ProductoAlmacenModel::PRODUCTO_ID);
            $table->foreign(ProductoAlmacenModel::PRODUCTO_ID)->references(ProductosModel::ID)->on(ProductosModel::getTableName());
            $table->string(ProductoAlmacenModel::NO_IDENTIFICACION)->nullable();
            $table->float(ProductoAlmacenModel::CANTIDAD);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ProductoAlmacenModel::getTableName());
    }
}
