<?php

use App\Models\Autos\UnidadesModel;
use App\Models\Autos\UnidadesNuevas\RemisionModel;
use App\Models\Logistica\CatCarrilesModel;
use App\Models\Logistica\CatModosModel;
use App\Models\Logistica\LogisticaModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableLogistica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(LogisticaModel::getTableName(), function (Blueprint $table) {
            $table->increments(LogisticaModel::ID);
            $table->date(LogisticaModel::FECHA_ENTREGA_CLIENTE);
            $table->time(LogisticaModel::HORA_ENTREGA_CLIENTE);
            $table->string(LogisticaModel::ECO);
            $table->string(LogisticaModel::SERIE);
            $table->string(LogisticaModel::TIPO_OPERACION);
            $table->float(LogisticaModel::IVA_DESGLOSADO);
            $table->string(LogisticaModel::TOMA);
            $table->date(LogisticaModel::FECHA_PROMESA_VENDEDOR);
            $table->time(LogisticaModel::HORA_PROMESA_VENDEDOR);
            $table->string(LogisticaModel::UBICACION);
            $table->boolean(LogisticaModel::REPROGRAMACION);
            $table->boolean(LogisticaModel::SEGURO);
            $table->boolean(LogisticaModel::PERMISO_PLACAS)->comment('0 permiso 1 placas');
            $table->string(LogisticaModel::ACCESORIOS);
            $table->boolean(LogisticaModel::SERVICIO);
            $table->string(LogisticaModel::FECHA_PROGRAMACION);
            $table->string(LogisticaModel::HORA_PROGRAMACION);
            $table->unsignedInteger(LogisticaModel::ID_UNIDAD)->nullable();
            $table->foreign(LogisticaModel::ID_UNIDAD)->references(RemisionModel::ID)->on(RemisionModel::getTableName());
            $table->unsignedInteger(LogisticaModel::ID_COLOR)->nullable();
            $table->foreign(LogisticaModel::ID_COLOR)->references(CatalogoColoresModel::ID)->on(CatalogoColoresModel::getTableName());
            $table->unsignedInteger(LogisticaModel::ID_CLIENTE)->nullable();
            $table->foreign(LogisticaModel::ID_CLIENTE)->references(ClientesModel::ID)->on(ClientesModel::getTableName());
            $table->unsignedInteger(LogisticaModel::ID_VENDEDOR)->nullable();
            $table->foreign(LogisticaModel::ID_VENDEDOR)->references(User::ID)->on(User::getTableName());
            $table->unsignedInteger(LogisticaModel::ID_MODO)->nullable();
            $table->foreign(LogisticaModel::ID_MODO)->references(catModosModel::ID)->on(catModosModel::getTableName());
            $table->unsignedInteger(LogisticaModel::ID_CARRIL)->nullable();
            $table->foreign(LogisticaModel::ID_CARRIL)->references(catCarrilesModel::ID)->on(catCarrilesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(catModosModel::getTableName());
    }
}

