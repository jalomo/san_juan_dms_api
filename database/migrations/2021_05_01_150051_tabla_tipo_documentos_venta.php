<?php

use App\Models\Autos\TipoDocumentosVentaModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaTipoDocumentosVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TipoDocumentosVentaModel::getTableName(), function (Blueprint $table) {
            $table->increments(TipoDocumentosVentaModel::ID);
            $table->string(TipoDocumentosVentaModel::NOMBRE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TipoDocumentosVentaModel::getTableName());
    }
}
