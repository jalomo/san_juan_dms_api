<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\CantidadProductosInicialModel;
use App\Models\Refacciones\ProductosModel;


class CreateTableProductosInicial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CantidadProductosInicialModel::getTableName(), function (Blueprint $table) {
            $table->increments(CantidadProductosInicialModel::ID);
            $table->unsignedInteger(CantidadProductosInicialModel::PRODUCTO_ID);
            $table->foreign(ProductosModel::ID)->references(ProductosModel::ID)->on(CantidadProductosInicialModel::getTableName());
            $table->integer(CantidadProductosInicialModel::CANTIDAD);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CantidadProductosInicialModel::getTableName());
    }
}
