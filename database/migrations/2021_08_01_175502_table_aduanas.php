<?php

use App\Models\Autos\UnidadesNuevas\CatAduanasUNModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableAduanas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatAduanasUNModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatAduanasUNModel::ID);
            $table->string(CatAduanasUNModel::ADUANA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatAduanasUNModel::getTableName());
    }
}
