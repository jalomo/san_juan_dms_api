<?php

use App\Models\Autos\CatModelosModel;
use App\Models\Refacciones\CatalogoMarcasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaModelos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(CatModelosModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatModelosModel::ID);
            $table->string(CatModelosModel::NOMBRE);

            $table->string(CatModelosModel::CLAVE);
            
            $table->unsignedInteger(CatModelosModel::ID_MARCA);
            $table->foreign(CatModelosModel::ID_MARCA)
                ->references(CatalogoMarcasModel::ID)
                ->on(CatalogoMarcasModel::getTableName());
            $table->integer(CatModelosModel::TIEMPO_LAVADO)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatModelosModel::getTableName());
    }
}
