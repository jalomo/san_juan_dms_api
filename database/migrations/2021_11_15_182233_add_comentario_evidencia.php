<?php

use App\Models\Soporte\EvidenciaTicketModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddComentarioEvidencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(EvidenciaTicketModel::getTableName(), function (Blueprint $table) {
            $table->text(EvidenciaTicketModel::COMENTARIO)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(EvidenciaTicketModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(EvidenciaTicketModel::COMENTARIO);
        });
    }
}
