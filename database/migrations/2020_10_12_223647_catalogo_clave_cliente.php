<?php

use App\Models\Refacciones\CatalogoClaveClienteModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CatalogoClaveCliente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatalogoClaveClienteModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoClaveClienteModel::ID);
            $table->string(CatalogoClaveClienteModel::NOMBRE);
            $table->string(CatalogoClaveClienteModel::CLAVE);
            $table->timestamps();
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoClaveClienteModel::getTableName());
    }
}
