<?php

use App\Models\Autos\Inventario\GasolinaModel;
use App\Models\Autos\Inventario\InventarioModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaInventarioGasolina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create(GasolinaModel::getTableName(), function (Blueprint $table) {
            $table->increments(GasolinaModel::ID);
            $table->string(GasolinaModel::NIVEL_GASOLINA)->nullable();
            $table->string(GasolinaModel::ARTICULOS_PERSONALES)->nullable();
            $table->string(GasolinaModel::CUALES)->nullable();
            $table->string(GasolinaModel::REPORTE_ALGO_MAS)->nullable();
            $table->unsignedInteger(GasolinaModel::ID_INVENTARIO);
            $table->foreign(GasolinaModel::ID_INVENTARIO)
                ->references(InventarioModel::ID)
                ->on(InventarioModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop(GasolinaModel::getTableName());
    }
}
