<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Usuarios\User;

class CrearTablaCuentaPorCobrar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CuentasPorCobrarModel::getTableName(), function (Blueprint $table) {
            $table->increments(CuentasPorCobrarModel::ID);
            $table->unsignedInteger(CuentasPorCobrarModel::FOLIO_ID);
            $table->foreign(CuentasPorCobrarModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());
            $table->unsignedInteger(CuentasPorCobrarModel::CLIENTE_ID);
            $table->foreign(CuentasPorCobrarModel::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());
            $table->unsignedInteger(CuentasPorCobrarModel::ESTATUS_CUENTA_ID);
            $table->foreign(CuentasPorCobrarModel::ESTATUS_CUENTA_ID)->references(EstatusCuentaModel::ID)->on(EstatusCuentaModel::getTableName());
            $table->text(CuentasPorCobrarModel::CONCEPTO);
            $table->unsignedInteger(CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID)->nullable();
            $table->foreign(CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID)->references(TipoFormaPagoModel::ID)->on(TipoFormaPagoModel::getTableName());
            $table->unsignedInteger(CuentasPorCobrarModel::TIPO_PAGO_ID)->nullable();
            $table->foreign(CuentasPorCobrarModel::TIPO_PAGO_ID)->references(CatTipoPagoModel::ID)->on(CatTipoPagoModel::getTableName());
            $table->unsignedInteger(CuentasPorCobrarModel::PLAZO_CREDITO_ID)->nullable();
            $table->foreign(CuentasPorCobrarModel::PLAZO_CREDITO_ID)->references(PlazoCreditoModel::ID)->on(PlazoCreditoModel::getTableName());
            $table->float(CuentasPorCobrarModel::TOTAL);

            $table->float(CuentasPorCobrarModel::IMPORTE);
            $table->float(CuentasPorCobrarModel::ENGANCHE)->nullable();
            $table->float(CuentasPorCobrarModel::TASA_INTERES)->nullable();
            $table->float(CuentasPorCobrarModel::INTERESES)->nullable();
            $table->text(CuentasPorCobrarModel::COMENTARIOS)->nullable();
            $table->integer(CuentasPorCobrarModel::TRANSFERENCIA_AUTORIZADA)
                ->default(CuentasPorCobrarModel::ESTATUS_TRANSFERENCIA_NO_APLICA);

            $table->unsignedInteger(CuentasPorCobrarModel::USUARIO_GESTOR_ID)->nullable();
            $table->foreign(CuentasPorCobrarModel::USUARIO_GESTOR_ID)
                ->references(User::ID)
                ->on(User::getTableName());
            $table->date(CuentasPorCobrarModel::FECHA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CuentasPorCobrarModel::getTableName());
    }
}
