<?php

use App\Models\Refacciones\VentaServicioModel;
use App\Models\Refacciones\VentasRealizadasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaVentaServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(VentaServicioModel::getTableName(), function (Blueprint $table) {
            $table->increments(VentaServicioModel::ID);

            $table->float(VentaServicioModel::PRECIO);
            $table->float(VentaServicioModel::IVA)->nullable();
            $table->float(VentaServicioModel::COSTO_MO)->nullable();
            $table->string(VentaServicioModel::NOMBRE_SERVICIO);
            $table->string(VentaServicioModel::NO_IDENTIFICACION);
            $table->float(VentaServicioModel::CANTIDAD);

            $table->unsignedInteger(VentaServicioModel::VENTA_ID);
            $table->foreign(VentaServicioModel::VENTA_ID)
                ->references(VentasRealizadasModel::ID)
                ->on(VentasRealizadasModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(VentaServicioModel::getTableName());
    }
}
