<?php

use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsVentaRealizadas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(VentasRealizadasModel::getTableName(), function (Blueprint $table) {
            $table->unsignedInteger(VentasRealizadasModel::VENDEDOR_ID)->default(4);
            $table->foreign(VentasRealizadasModel::VENDEDOR_ID)
                ->references(User::ID)->on(User::getTableName());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(VentasRealizadasModel::getTableName(), function (Blueprint $table) {
            $table->dropColumn(VentasRealizadasModel::VENDEDOR_ID);
        });
    }
}
