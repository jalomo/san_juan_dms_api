<?php

use App\Models\Autos\EncuestaSatisfaccionModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaEncuestaSatisfaccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EncuestaSatisfaccionModel::getTableName(), function (Blueprint $table) {
            $table->increments(EncuestaSatisfaccionModel::ID);
            $table->integer(EncuestaSatisfaccionModel::EXPERIENCIA_VENTAS);
            $table->integer(EncuestaSatisfaccionModel::ATENCION_ASESOR);
            $table->integer(EncuestaSatisfaccionModel::EXPERIENCIA_FINANCIAMIENTO);
            $table->integer(EncuestaSatisfaccionModel::EXPERIENCIA_ENTREGA_VEHICULO);
            $table->integer(EncuestaSatisfaccionModel::SEGUIMIENTO_COMPROMISOS);
            $table->text(EncuestaSatisfaccionModel::COMENTARIOS_MEJORAS);
            $table->unsignedInteger(EncuestaSatisfaccionModel::ID_VENTA_UNIDAD);
            $table->foreign(EncuestaSatisfaccionModel::ID_VENTA_UNIDAD)
                ->references(VentasAutosModel::ID)
                ->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EncuestaSatisfaccionModel::getTableName());
    }
}
