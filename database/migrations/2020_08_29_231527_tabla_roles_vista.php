<?php

use App\Models\Usuarios\Menu\ModulosModel;
use App\Models\Usuarios\RolesVistasModel;
use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuVistasModel;
use App\Models\Usuarios\RolModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaRolesVista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // 
        Schema::create(RolesVistasModel::getTableName(), function (Blueprint $table) {
            $table->increments(RolesVistasModel::ID);
            $table->unsignedInteger(RolesVistasModel::ROL_ID);
            $table->foreign(RolesVistasModel::ROL_ID)
                ->references(RolModel::ID)
                ->on(RolModel::getTableName());

            $table->unsignedInteger(RolesVistasModel::VISTA_ID)->nullable();
            $table->foreign(RolesVistasModel::VISTA_ID)
                ->references(MenuVistasModel::ID)
                ->on(MenuVistasModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RolesVistasModel::getTableName());
    }
}
