<?php

use App\Models\Autos\SalidaUnidades\ComentariosModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaComentarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ComentariosModel::getTableName(), function (Blueprint $table) {
            $table->increments(ComentariosModel::ID);
            $table->string(ComentariosModel::EXPLICO_ASISTENCIA_FORD)->nullable();
            $table->string(ComentariosModel::EXPLICO_FORD_PROTECT)->nullable();
            $table->string(ComentariosModel::ACCESORIOS_PERSONALIZAR)->nullable();
            $table->string(ComentariosModel::PRUEBA_MANEJO)->nullable();
            $table->string(ComentariosModel::INFORMACION_MANTENIMIENTO)->nullable();
            $table->string(ComentariosModel::ASESOR_SERVICIO)->nullable();
            $table->unsignedInteger(ComentariosModel::ID_VENTA_AUTO);
            $table->foreign(ComentariosModel::ID_VENTA_AUTO)
            ->references(VentasAutosModel::ID)
            ->on(VentasAutosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ComentariosModel::getTableName());
    }
}
