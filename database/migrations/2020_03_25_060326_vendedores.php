<?php

use App\Models\Refacciones\VendedorModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Vendedores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(VendedorModel::getTableName(), function (Blueprint $table) {
            $table->increments(VendedorModel::ID);
            $table->string(VendedorModel::NOMBRE);
            $table->string(VendedorModel::RFC)->nullable();
            $table->string(VendedorModel::NUMERO_VENDEDOR)->nullable();
            $table->string(VendedorModel::APELLIDO_MATERNO)->nullable();
            $table->string(VendedorModel::APELLIDO_PATERNO)->nullable();
            $table->string(VendedorModel::DIRECCION)->nullable();
            $table->string(VendedorModel::ESTADO)->nullable();
            $table->string(VendedorModel::MUNICIPIO)->nullable();
            $table->string(VendedorModel::COLONIA)->nullable();
            $table->string(VendedorModel::CODIGO_POSTAL)->nullable();
            $table->string(VendedorModel::TELEFONO)->nullable();
            $table->string(VendedorModel::TELEFONO_2)->nullable();
            $table->string(VendedorModel::CORREO_ELECTRONICO)->nullable();
            $table->string(VendedorModel::CORREO_ELECTRONICO_2)->nullable();
            $table->text(VendedorModel::NOTAS)->nullable();
            $table->string(VendedorModel::CLAVE_VENDEDOR)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(VendedorModel::getTableName());
    }
}
