<?php
use App\Models\Contabilidad\Subcuenta;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaSubcuenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Subcuenta::getTableName(), function (Blueprint $table) {
            $table->increments(Subcuenta::ID);
            $table->string(Subcuenta::NOMBRE_CUENTA);
            $table->string(Subcuenta::NO_CUENTA);
            $table->unsignedInteger(Subcuenta::ID_CUENTA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
