<?php

use App\Models\Soporte\EvidenciaTicketModel;
use App\Models\Soporte\TicketsModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableEvidenciaTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EvidenciaTicketModel::getTableName(), function (Blueprint $table) {
            $table->increments(EvidenciaTicketModel::ID);
            $table->text(EvidenciaTicketModel::EVIDENCIA);
            $table->unsignedInteger(EvidenciaTicketModel::TICKET_ID);
            $table->foreign(EvidenciaTicketModel::TICKET_ID)
                        ->references(TicketsModel::ID)
                        ->on(TicketsModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EvidenciaTicketModel::getTableName());
    }
}
