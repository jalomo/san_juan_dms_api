<?php

use App\Models\Soporte\CatStatusPrioridadesModel;
use App\Models\Soporte\CatStatusTickets;
use App\Models\Soporte\TicketsModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TicketsModel::getTableName(), function (Blueprint $table) {
            $table->increments(TicketsModel::ID);
            $table->string(TicketsModel::ASUNTO);
            $table->text(TicketsModel::COMENTARIO);
            $table->string(TicketsModel::EVIDENCIA);
            $table->string(TicketsModel::FOLIO);
            $table->unsignedInteger(TicketsModel::USER_ID);
            $table->foreign(TicketsModel::USER_ID)
                ->references(User::ID)
                ->on(User::getTableName());

            $table->unsignedInteger(TicketsModel::PRIORIDAD_ID);
            $table->foreign(TicketsModel::PRIORIDAD_ID)
                ->references(CatStatusPrioridadesModel::ID)
                ->on(CatStatusPrioridadesModel::getTableName());

            $table->unsignedInteger(TicketsModel::ESTATUS_ID)->default(1);
            $table->foreign(TicketsModel::ESTATUS_ID)
                ->references(CatStatusTickets::ID)
                ->on(CatStatusTickets::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TicketsModel::getTableName());
    }
}
