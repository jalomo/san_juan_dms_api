<?php

// use App\Autos\AgendaSalidaModel;

use App\Models\Autos\AgendaSalidaModel;
use App\Models\Autos\VentasAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaAgendaSalida extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AgendaSalidaModel::getTableName(), function (Blueprint $table) {
            $table->increments(AgendaSalidaModel::ID);
            $table->unsignedInteger(AgendaSalidaModel::ID_VENTA_AUTO);
            $table->foreign(AgendaSalidaModel::ID_VENTA_AUTO)
                ->references(VentasAutosModel::ID)
                ->on(VentasAutosModel::getTableName());

            $table->text(AgendaSalidaModel::TITULO);
            $table->dateTime(AgendaSalidaModel::DATETIME_INICIO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(AgendaSalidaModel::getTableName());
    }
}
