<?php

use App\Models\CPVentas\CatStatusCPModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatEstatusContactoProactivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatStatusCPModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatStatusCPModel::ID);
            $table->string(CatStatusCPModel::ESTATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatStatusCPModel::getTableName());
    }
}
